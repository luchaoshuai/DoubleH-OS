﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace DoubleH.Utility.Configuration
{
    /// <summary>
    /// 软件配置实例
    /// </summary>
    public class DoubleHConfig
    {
        static AppConfig appConfig = (AppConfig)ConfigSerializer.LoadConfig(typeof(AppConfig), DbDefine.appConfigFile);
        /// <summary>
        /// 获取程序配置文件
        /// </summary>
        public static AppConfig AppConfig
        {
            get { return appConfig; }
        }
        /// <summary>
        /// 获取或设置用户配置
        /// </summary>
        public static UserConfig UserConfig
        {
            get;
            set;
        }
    }
}