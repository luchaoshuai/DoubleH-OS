﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Utility.UC.Password;
using System.ComponentModel;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// PasswordPanel.xaml 的交互逻辑
    /// </summary>
    public partial class AndroidPassword : UserControl
    {
        private string _password;

        private string _tmpPassword;

        private Point startPoint;

        private Point endPoint;

        private List<PasswordButton> PasswordButtons;

        public static readonly RoutedEvent InputCompletedEvent;

        [Category("Behavior")]
        public event RoutedEventHandler InputCompleted
        {
            add
            {
                base.AddHandler(InputCompletedEvent, value);
            }
            remove
            {
                base.RemoveHandler(InputCompletedEvent, value);
            }
        }

        public string Password
        {
            get { return _password; }
        }

        static AndroidPassword()
        {
            InputCompletedEvent = EventManager.RegisterRoutedEvent("InputCompleted", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AndroidPassword));
        }

        public AndroidPassword()
        {
            InitializeComponent();
            _password = string.Empty;
            PasswordButtons = new List<PasswordButton>();
            foreach (UIElement item in PasswordLayout.Children)
            {
                PasswordButton pb = item as PasswordButton;
                if (pb != null)
                {
                    pb.CheckIn += new RoutedEventHandler(pb_CheckIn);
                    PasswordButtons.Add(pb);
                }
            }
        }

        void pb_CheckIn(object sender, RoutedEventArgs e)
        {
            PasswordButton pb = sender as PasswordButton;
            if (pb != null)
            {
                if (string.IsNullOrWhiteSpace(_tmpPassword))
                {
                    startPoint = pb.TransformToAncestor(this).Transform(new Point(pb.ActualWidth / 2, pb.ActualHeight / 2));
                }
                else
                {
                    if (_tmpPassword.Substring(_tmpPassword.Length - 1, 1) == pb.Password)
                        return;
                    endPoint = endPoint = pb.TransformToAncestor(this).Transform(new Point(pb.ActualWidth / 2, pb.ActualHeight / 2));
                    Line line = new Line();
                    line.X1 = startPoint.X;
                    line.X2 = endPoint.X;
                    line.Y1 = startPoint.Y;
                    line.Y2 = endPoint.Y;
                    line.Stroke = Brushes.Aqua;
                    line.StrokeThickness = 4;
                    LinePanel.Children.Add(line);
                    startPoint = endPoint;
                }
                _tmpPassword += pb.Password;
            }
        }

        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PasswordButtons.ForEach((pb) => { pb.IsSwitchOn = true; });
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PasswordButtons.ForEach((pb) => { pb.IsSwitchOn = false; });
            LinePanel.Children.Clear();
            _password = _tmpPassword;
            _tmpPassword = string.Empty;
            this.OnInputCompleted(new RoutedEventArgs(InputCompletedEvent));
        }

        protected virtual void OnInputCompleted(RoutedEventArgs e)
        {
            base.RaiseEvent(e);
        }

        public Brush GridBackgroundBrush
        {
            set { PasswordLayout.Background = value; }
        }
    }
}
