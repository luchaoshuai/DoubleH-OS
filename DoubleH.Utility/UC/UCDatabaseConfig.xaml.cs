﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoubleH.Utility;
using FCNS.Data;
using DoubleH.Utility.Configuration;

namespace DoubleH.Utility.UC
{
    /// <summary>
    /// UCDatabaseConfig.xaml 的交互逻辑
    /// </summary>
    public partial class UCDatabaseConfig : UserControl
    {
        public UCDatabaseConfig()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }
        private void InitVar()
        {
            comboBoxDataType.ItemsSource = Enum.GetNames(typeof(DataType));

            listBoxFlag.ItemsSource = DoubleHConfig.AppConfig.DataConfigItems;
            listBoxFlag.DisplayMemberPath = "Flag";
            listBoxFlag.SelectedItem = DoubleHConfig.AppConfig.DataFlag;
        }

        private void InitEvent()
        {
            listBoxFlag.SelectionChanged += (ss, ee) => SelectedSql();
            buttonDelete.Click += (ss, ee) => DeleteSql();
            buttonNew.Click += (ss, ee) => NewSql();
            buttonSave.Click += (ss, ee) => SaveSql();
            passwordBoxPwd.MouseDoubleClick += (ss, ee) => ShowPassword();
        }

        private void ShowPassword()
        {
            MessageWindow.Show(passwordBoxPwd.Password);
        }

        private void DeleteSql()
        {
            if (listBoxFlag.SelectedItem == null)
                return;
            if (MessageWindow.Show("", "确定要移除选择的项目吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;
            DoubleHConfig.AppConfig.DataConfigItems.Remove((DataConfig)listBoxFlag.SelectedItem);
        }

        private void NewSql()
        {
            DoubleHConfig.AppConfig.DataConfigItems.Add(new DataConfig() { Flag = "未命名数据库连接" });
        }

        private void SaveSql()
        {
            if (listBoxFlag.SelectedItem == null)
                return;

            DataConfig dc = (DataConfig)listBoxFlag.SelectedItem;
            dc.Flag = textBoxFlag.Text;
            dc.DataType = comboBoxDataType.Text;
            dc.DataAddress = textBoxIpOrName.Text;
            dc.Port = integerUpDownPort.Value.Value ;
            dc.DataName = textBoxFile.Text;
            dc.DataUser = textBoxName.Text;
            dc.DataPassword = passwordBoxPwd.Password;
            dc.TimeOut = integerUpDownTimeOut.Value.Value;
        }

        private void SelectedSql()
        {
            if (listBoxFlag.SelectedItem == null)
                return;

            DataConfig dc = (DataConfig)listBoxFlag.SelectedItem;
            textBoxFlag.Text = dc.Flag;
            comboBoxDataType.Text = dc.DataType;
            textBoxIpOrName.Text = dc.DataAddress;
            integerUpDownPort.Value = Convert.ToInt32(dc.Port);
            textBoxFile.Text = dc.DataName;
            textBoxName.Text = dc.DataUser;
            passwordBoxPwd.Password = dc.DataPassword;
            integerUpDownTimeOut.Value = dc.TimeOut;
        }
    }
}
