﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DoubleH.Utility;
using System.Collections;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using Table = FCNS.Data.Table;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Markup;
using DoubleH.Utility.Configuration;
namespace DoubleH.Utility.UC
{
   class ListViewExt:ListView
    {
        public ListViewExt()
        {
            ResourceDictionary newDictionary = new ResourceDictionary();
            newDictionary.Source = new Uri("pack://application:,,,/DoubleH.Utility;component/themes/Generic.xaml");
            this.Resources.MergedDictionaries.Add(newDictionary);
        }

        //public enum EnumView
        //{
        //    gridView,
        //    iconView,
        //    tileView,
        //    oneButtonHeaderView
        //}

        public void ChangeView(string resourceId)
        {
        }

        public void ChangeView(string dataTempleteName,string resourceId,double itemSize)
        {
            //MessageBox.Show(dataTempleteName.ToString() + "  " + resourceId.ToString());
            if (string.IsNullOrEmpty(dataTempleteName))
                return;
            object res = TryFindResource(dataTempleteName);
            if(res==null)
                return;

            PlainView pv = new PlainView(resourceId);
            pv.ItemHeight = itemSize;
            pv.ItemWidth = itemSize;
            pv.ItemTemplate =res  as DataTemplate;
            this.View = pv;
        }
    }
}
