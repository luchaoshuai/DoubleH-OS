﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.ComponentModel;

namespace DoubleH.Utility.UC.PagePanel
{
    /// <summary>
    /// PageDataGrid.xaml 的交互逻辑
    /// </summary>
    public partial class PageDataGrid : Page,IPagePanel
    {
        public PageDataGrid()
        {
            InitializeComponent();

            InitEvent();
        }

        public event Utility.UC.UCPagePanel.dDoubleClickItem ItemDoubleClick;
        public event Utility.UC.UCPagePanel.dClickItem ItemClick;
        public event Utility.UC.UCPagePanel.dScrollMove ScrollMove;

        private void InitEvent()
        {
            dataGridExt1.MouseDoubleClick += (ss, ee) => DataGridExtMouseDoubleClick();
            dataGridExt1.CurrentCellChanged += (ss, ee) => DataGridExtMouseClick();
            CommandManager.AddPreviewExecutedHandler(dataGridExt1, new ExecutedRoutedEventHandler(OnScorllMove));
        }

        private void DataGridExtMouseDoubleClick()
        {
            if (dataGridExt1.SelectedItem == null || ItemDoubleClick == null)
                return;

            ItemDoubleClick(dataGridExt1.SelectedItem);
        }

        private void DataGridExtMouseClick()
        {
            if (ItemClick != null)
                ItemClick(dataGridExt1.CurrentCell);
        }

        private void OnScorllMove(object sender, ExecutedRoutedEventArgs e)
        {
            if (ScrollMove != null)
                ScrollMove(sender, e);
        }


        public void Init(UserUIparams userUIparams,params PagePanel.PageListView.EnumModeTemplete[] templeteName)
        {
            dataGridExt1.Init(userUIparams);
        }

        public void InitHomeData(IList obj)
        {
            dataGridExt1.ItemsSource = obj;
        }

        public void InitData(IList<FCNS.Data.Table.GroupS> groups, IList items)
        {
            dataGridExt1.ItemsSource = items;
        }

        public object SelectedItem
        {
            get
            {
                  return dataGridExt1.SelectedItem;
            }
        }

        public object SelectedGroupItem
        {
            get
            {
                    return null;
            }
        }

        public IEnumerable SelectedItems
        {
            get
            {
                return dataGridExt1.SelectedItems;
            }
        }

        public Brush BackgroundBrush
        {
            set
            {
                dataGridExt1.Background = value;
            }
        }

        public Brush ForegroundBrush
        {
            set
            {
                dataGridExt1.Foreground = value;
            }
        }
    }
}
