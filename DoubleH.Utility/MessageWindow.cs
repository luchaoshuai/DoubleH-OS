﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DoubleH.Utility
{
   public class MessageWindow
    {
       public static void Show(string text)
       {
           Show("", text);
       }

       public static void Show(string title, string text)
       {
           System.Windows.MessageBox.Show(text, title);
       }

       public static MessageBoxResult Show(string title, string text,MessageBoxButton button)
       {
           return System.Windows.MessageBox.Show(text, title, button);
       }

    }
}
