﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using System.Net;
using System.ComponentModel;
using DoubleH.Utility.Configuration;
using System.Data;

namespace DoubleH.Utility
{
    public class NormalUtility
    {
        /// <summary>
        /// 程序初始化是调用它
        /// </summary>
        public static void Init()
        {
        }
        /// <summary>
        /// 程序关闭时必须调用它
        /// </summary>
        public static void Dispose()
        {
        }

        /// <summary>
        /// 加载屏幕手写板
        /// </summary>
        /// <returns></returns>
        public static void LoadTabTip()
        {
            if (DoubleHConfig.AppConfig.AutoLoadTabTip)
                Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + @"\Microsoft Shared\Ink\TabTip.exe");
        }

        /// <summary>
        /// 显示登录窗体并且是否登录成功
        /// </summary>
        /// <returns></returns>
        public static bool ShowLoginWindowSuccess(Table.UserS.EnumFlag flag)
        {
            Table.UserS.LoginUser = null;
            DoubleH.Utility.Login.LoginWindow lw = new Utility.Login.LoginWindow();
            lw.Init(flag);
            lw.ShowDialog();
            return Table.UserS.LoginUser != null;
            //if (logined)
            //    InitNet();
        }

        /// <summary>
        /// 将 CheckComboBox 文本转换,必须为可转换的值.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Int64[] FormatStringToInt64(string text)
        {
            List<Int64> id = new List<long>();
            if (!string.IsNullOrEmpty(text))
                foreach (string str in text.Split(','))
                    id.Add(Convert.ToInt64(str));

            return id.ToArray();
        }
        ///// <summary>
        ///// 加载虚拟键盘
        ///// </summary>
        //public void LoadOSK()
        //{
        //    MessageWindow.Show(Environment.GetFolderPath(Environment.SpecialFolder.System));
        //    MessageWindow.Show(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86));

        //    //Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86) + "osk.exe");
        //}

        /// <summary>
        /// 根据用户配置文件中保存的字段进行排序
        /// </summary>
        /// <param name="items"></param>
        /// <param name="sortName"></param>
        /// <param name="sortDirection"></param>
        public static void SortColumn(IList items, string sortName, ListSortDirection sortDirection)
        {
            if (items==null||items.Count == 0 || string.IsNullOrEmpty(sortName))
                return;

            for (int i = 1; i < items.Count; i++)
            {
                object obj = items[i];
                int j = i;
                while ((j > 0) && CompareForDataGridItemSource(items[j - 1], obj, sortName, sortDirection == ListSortDirection.Ascending) < 0)
                {
                    items[j] = items[j - 1];
                    --j;
                }
                items[j] = obj;
            }
        }

        /// <summary>
        /// 比较大小 返回值 小于零则X小于Y，等于零则X等于Y，大于零则X大于Y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private static int CompareForDataGridItemSource(object x, object y, string sortName, bool sortBy)
        {
            PropertyInfo property = x.GetType().GetProperty(sortName);
            if (property == null)
                return 1;
                //throw new ArgumentNullException("在对象中没有找到指定属性!");

            switch (property.PropertyType.ToString())
            {
                case "System.Int32":
                case "System.Int64":
                    Int64 int1 = 0;
                    Int64 int2 = 0;
                    if (property.GetValue(x, null) != null)
                        int1 = Convert.ToInt64(property.GetValue(x, null));

                    if (property.GetValue(y, null) != null)
                        int2 = Convert.ToInt64(property.GetValue(y, null));

                    if (sortBy)
                        return int2.CompareTo(int1);
                    else
                        return int1.CompareTo(int2);

                case "System.Nullable`1[System.Double]":
                case "System.Double":
                    double double1 = 0;
                    double double2 = 0;
                    if (property.GetValue(x, null) != null)
                        double1 = Convert.ToDouble(property.GetValue(x, null));

                    if (property.GetValue(y, null) != null)
                        double2 = Convert.ToDouble(property.GetValue(y, null));

                    if (sortBy)
                        return double2.CompareTo(double1);
                    else
                        return double1.CompareTo(double2);

                case "System.String":
                    string string1 = string.Empty;
                    string string2 = string.Empty;
                    if (property.GetValue(x, null) != null)
                        string1 = property.GetValue(x, null).ToString();

                    if (property.GetValue(y, null) != null)
                        string2 = property.GetValue(y, null).ToString();

                    if (sortBy)
                        return string2.CompareTo(string1);
                    else
                        return string1.CompareTo(string2);


                case "System.DateTime":
                case "System.Nullable`1[System.DateTime]":
                    DateTime dt1 = DateTime.Now;
                    DateTime dt2 = DateTime.Now;
                    if (property.GetValue(x, null) != null)
                     DateTime.TryParse(property.GetValue(x, null).ToString(),null, System.Globalization.DateTimeStyles.None, out dt1);
                    
                     if (property.GetValue(y, null) != null)
                        DateTime.TryParse(property.GetValue(y, null).ToString(), null, System.Globalization.DateTimeStyles.None, out dt2);

                    if (sortBy)
                        return dt2.CompareTo(dt1);
                    else
                        return dt1.CompareTo(dt2);
            }
            return 0;
        }

        public delegate bool EqualsComparer<T>(T x, T y);
        public class Compare<T> : IEqualityComparer<T>
        {
            private EqualsComparer<T> _equalsComparer;

            public Compare(EqualsComparer<T> equalsComparer)
            {
                this._equalsComparer = equalsComparer;
            }

            public bool Equals(T x, T y)
            {
                if (null != this._equalsComparer)
                    return this._equalsComparer(x, y);
                else
                    return false;
            }
            public int GetHashCode(T obj)
            {
                return obj.ToString().GetHashCode();
            }
        }

    }
}
