﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using System.Reflection;
using DoubleH.Utility.Configuration;
using FCNS.Data;

namespace DoubleH.Utility.IO
{
    /// <summary>
    /// SqlBackup.xaml 的交互逻辑
    /// </summary>
    public partial class SqlBackup : Window
    {
        string[] tableNames = new string[]{"UserS","UniqueS","SysConfig","GroupS","CorS","PosS","SalesOrderS","AfterSaleServiceS",
        "ProductSInRepairS","ProductSInWeiBaoS","RepairS","WeiBaoS","CarDriveS","CarHealthS","CarOilS","CarS","StoreS","StoreOrderS",
        "ProductSIO","ProductS","ProductSInStoreS","AdjustAveragePriceS","PurchaseOrderS","PayS","ShopInfoS",
"ProductSInCheckStoreS","CheckStoreS","PosOrderS"};

        string folder = DbDefine.baseDir + "backup\\";
        
    public SqlBackup()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            textBox1.Text = folder;
            textBox1.IsReadOnly = true;
        }

        private void InitEvent()
        {
            buttonBrowser.Click += (ss, ee) =>
            {
                using (System.Windows.Forms.FolderBrowserDialog sfd = new System.Windows.Forms.FolderBrowserDialog())
                {
                    sfd.SelectedPath = folder;
                    sfd.ShowNewFolderButton = true;
                    if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        textBox1.Text = sfd.SelectedPath;
                }
            };

            buttonStart.Click += (ss, ee) =>
                {
                    string filePath = textBox1.Text;
                    if (!filePath.EndsWith("\\"))
                        filePath += "\\";

                    if (string.IsNullOrEmpty(filePath))
                        return;

                    DataSet dataset = new DataSet();
                    foreach (string str in tableNames)
                    {
                        DataTable dt = FCNS.Data.SQLdata.GetDataTable("select * from " + str);
                        dt.TableName = str;
                        dataset.Tables.Add(dt);
                    }
                    dataset.WriteXml(filePath + DateTime.Now.ToString(FCNS.Data.DbDefine.dateTimeFormat)+".xml", XmlWriteMode.WriteSchema);
                    MessageWindow.Show("备份成功");
                    this.Close();
                    //File.Copy(FCNS.Data.DbDefine.dbFile, filePath + DateTime.Now.ToString(FCNS.Data.DbDefine.dateFormat));
                };
        }
    }
}