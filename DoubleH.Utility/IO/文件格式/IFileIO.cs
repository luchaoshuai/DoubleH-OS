﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DoubleH.Utility.IO
{
    public interface IFileIO
    {
        string FileName { get; set; }
        DataTable ImportFileToDataTable( );
        void ExportData( DataTable dataTable);
        void ExportDataGridToFile(UC.DataGridExt grid );
    }
}
