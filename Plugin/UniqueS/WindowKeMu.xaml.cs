﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;
using System.Collections;
using DoubleH.Utility;
using System.Diagnostics;

namespace UniqueS
{
    /// <summary>
    /// WindowKeMu.xaml 的交互逻辑
    /// </summary>
    public partial class WindowKeMu : Window
    {
        public WindowKeMu()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        ObservableCollection<ItemBinding> allSortKeMu = new ObservableCollection<ItemBinding>();
        ObservableCollection<Table.UniqueS> allKeMu = Table.UniqueS.GetListForKeMu(true);
        ItemBinding selectItemBinding = null;

        Table.UniqueS order = null;
        public void Init(Table.UniqueS order)
        {
            this.order = order ?? new Table.UniqueS(Table.UniqueS.EnumFlag.会计科目) { StringValue = "10" };
            InitOrder();
            if (order == null)
                buttonSave.IsEnabled = true;

        }

        private void InitVar()
        {
            ItemBinding ib1 = new ItemBinding() { Name = "资产合计", KeMu = "10", CanEdit = false };
            InitKeMu(allKeMu, "10", ib1);
            ItemBinding ib2 = new ItemBinding() { Name = "资产负债", KeMu = "20", CanEdit = false };
            InitKeMu(allKeMu, "20", ib2);
            ItemBinding ib3 = new ItemBinding() { Name = "收入类", KeMu = "30", CanEdit = false };
            InitKeMu(allKeMu, "30", ib3);
            ItemBinding ib4 = new ItemBinding() { Name = "支出类", KeMu = "40", CanEdit = false };
            InitKeMu(allKeMu, "40", ib4);
            allSortKeMu.Add(ib1);
            allSortKeMu.Add(ib2);
            allSortKeMu.Add(ib3);
            allSortKeMu.Add(ib4);
            treeViewKeMu.ItemsSource = allSortKeMu;
        }

        private void InitEvent()
        {
            buttonNew.Click += (ss, ee) => NewOrder();
            buttonSave.Click += (ss, ee) => SaveOrder();
            treeViewKeMu.MouseUp += (ss, ee) => ChangedOrder();
        }

        private void ChangedOrder()
        {
            if (treeViewKeMu.SelectedItems.Count == 0)
                return;

            selectItemBinding = treeViewKeMu.SelectedItems[0] as ItemBinding;
            if (selectItemBinding == null)
                return;

            order = allKeMu.FirstOrDefault(f => f.StringValue == selectItemBinding.KeMu);
            InitOrder();
        }

        private void InitOrder()
        {
            Debug.Assert(order != null);

            int leg = order.StringValue.Length;
            Debug.Assert(leg > 1);

            string end = order.StringValue.Substring(leg - 2);
            textBoxName.Text = order.Name;
            integerUpDownIntegerValue.Text = end;
            treeViewKeMu.SelectedItems = new List<ItemBinding>(allSortKeMu.Where(f => f.KeMu == order.StringValue));
            buttonSave.IsEnabled = !order.BoolValue;
        }

        private void NewOrder()
        {
            if (selectItemBinding == null)
            {
                MessageWindow.Show("请在左边选择会计科目");
                return;
            }
            int count = selectItemBinding.SubItems.Count + 1;
            if (count > 99)
                count = 99;

            integerUpDownIntegerValue.Value = count;
            textBoxName.Clear();
            order = new Table.UniqueS(Table.UniqueS.EnumFlag.会计科目);
            buttonSave.IsEnabled = true;
        }

        private void SaveOrder()
        {
            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
            {
                order.BoolValue = false;
                order.StringValue = selectItemBinding.KeMu + integerUpDownIntegerValue.Text;
                order.Name = textBoxName.Text;
                result = order.Insert();
            }
            else
            {
                order.Name = textBoxName.Text;
                result = order.Update();
            }

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private void InitKeMu(ObservableCollection<Table.UniqueS> all, string parentKeMu, ItemBinding parentItem)
        {
            var vr = all.Where(f => (f.StringValue.StartsWith(parentKeMu) & (f.StringValue.Length == parentKeMu.Length + 2)));
            foreach (var v in vr)
            {
                ItemBinding ib = new ItemBinding() { Name = v.Name, KeMu = v.StringValue };
                ib.CanEdit = !v.BoolValue;
                InitKeMu(all, v.StringValue, ib);
                parentItem.SubItems.Add(ib);
            }
        }
    }

    public class ItemBinding
    {
        public override string ToString()
        {
            return KeMu + ":" + Name;
        }

        public string Name { get; set; }
        public string KeMu { get; set; }
        public bool CanEdit { get; set; }

        ObservableCollection<ItemBinding> tb = new ObservableCollection<ItemBinding>();
        public ObservableCollection<ItemBinding> SubItems
        {
            get { return tb; }
            set { tb = value; }
        }
    }
}
