﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace UniqueS.UC
{
    public class UCUniqueS : ComboBox
    {
        public UCUniqueS()
        {
            InitVar();
        }

        private void InitVar()
        {
            this.DisplayMemberPath = "Name";
            this.SelectedValuePath = "Id";
        }

        Table.UniqueS.EnumFlag flag;
        /// <summary>
        /// 必须调用me 初始化控件参数
        /// </summary>
        /// <param name="obj"></param>
        public void Init(Table.UniqueS.EnumFlag obj)
        {
            flag = obj;
            var vr = Table.UniqueS.GetList(obj, Table.UniqueS.EnumEnable.启用);
            switch (flag)
            {
                case Table.UniqueS.EnumFlag.产地:
                case Table.UniqueS.EnumFlag.计量单位:
                case Table.UniqueS.EnumFlag.品牌:
                case Table.UniqueS.EnumFlag.客户性质:
                case Table.UniqueS.EnumFlag.维修对象:
                case Table.UniqueS.EnumFlag.维修类型:
                    IsEditable = true;
                    break;

                case Table.UniqueS.EnumFlag.发票定义:
                    IsEditable = false;
                    vr.Insert(0, new Table.UniqueS(Table.UniqueS.EnumFlag.发票定义) { Id = -1, Name = "无" });
                   
                    break;

                default:
                    IsEditable = false;
                    break;
            }
            this.ItemsSource = vr;
        }
        /// <summary>
        /// 如果编辑框有内容且数据库中不存在此记录则自动添加到数据库
        /// </summary>
        public Int64 SelectedObjectId
        {
            get
            {
                if (SelectedItem == null)
                {
                    if (!string.IsNullOrEmpty(Text))
                    {
                        Table.UniqueS us = new Table.UniqueS(flag) ;
                        us.Name = Text;
                        if (us.Insert() == Table.DataTableS.EnumDatabaseStatus.操作成功)
                            return us.Id;
                        else
                            return -1;
                    }
                    return -1;
                }
                else
                    return (Int64)SelectedValue;
            }
            set
            {
                SelectedValue = value;
            }
        }
    }
}
