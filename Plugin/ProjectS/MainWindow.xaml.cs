﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace ProjectS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.ProjectS order = null;
        public void Init(Table.ProjectS obj)
        {
            order = obj ?? new Table.ProjectS();
            InitVar();
            InitOrder();
            InitEvent();
            textBoxName.Focus();
        }

        private void InitVar()
        {
            dataGridExt1.ShowBodyMenu = true;
            datePickerOrderDate.SelectedDate = DateTime.Now;
            corSelect1.Init(true);
        }

        private void InitOrder()
        {
            if (order.Id == -1)
                return;

            textBoxName.Text = order.Name;
            corSelect1.SelectedObjectId = order.CorSId;
            doubleUpDownMoney.Value = order.Money;
            textBoxOrderNO.Text = order.OrderNO;
            datePickerOrderDate.SelectedDate = order.OrderDateTime;
            dataGridExt1.ItemsSource = order.ProductSList;

            switch (order.Enable)
            {
                case Table.ProjectS.EnumEnable.评估: if (order.Id != -1) labelDataTime.Content = "实施日期:"; break;
                case Table.ProjectS.EnumEnable.实施: labelDataTime.Content = "完工日期:"; break;
            }
            buttonSave.IsEnabled = order.Enable == Table.ProjectS.EnumEnable.评估;
            buttonShiShi.IsEnabled = (order.Enable == Table.ProjectS.EnumEnable.评估 && order.Id != -1);
            buttonWanGong.IsEnabled = order.Enable == Table.ProjectS.EnumEnable.实施;
            buttonYanShou.IsEnabled = order.Enable == Table.ProjectS.EnumEnable.验收;
            buttonZuoFei.IsEnabled = (order.Enable == Table.ProjectS.EnumEnable.评估 || order.Enable == Table.ProjectS.EnumEnable.实施);
        }

        private void InitEvent()
        {
            dataGridExt1.MouseDoubleClick += (ss, ee) => SelectProductS();
            buttonSave.Click += (ss, ee) => Save();
            buttonShiShi.Click += (ss, ee) => UpdateOrderTo(Table.ProjectS.EnumEnable.实施);
            buttonWanGong.Click += (ss, ee) => WanGong();
            buttonYanShou.Click += (ss, ee) => UpdateOrderTo(Table.ProjectS.EnumEnable.入账);
            buttonZuoFei.Click += (ss, ee) => ZuoFei();
            buttonExport.Click += (ss, ee) => Export();
        }

        private void Export()
        {
            DoubleH.Utility.IO.FileTools ft = new DoubleH.Utility.IO.FileTools();
            ft.ExportDataGridToFile(dataGridExt1, string.Empty);
        }

        private void SelectProductS()
        {
            ProductS.GetProductS gps = new ProductS.GetProductS();
            gps.Init(ProductS.GetProductS.EnumProductS.询价单商品, null);
            gps.ShowDialog();
            foreach (Table.ProductS p in gps.Selected)
            {
                Table.ProductSInProjectS psps = new Table.ProductSInProjectS();
                psps.ProductSId = p.Id;
                psps._ProductSBrand = p._BrandName;
                psps._ProductSModel = p.Model;
                psps._ProductSName = p.Name;
                order.ProductSList.Add(psps);
            }
        }

        private void WanGong()
        {
            if (MessageWindow.Show("操作不可逆", "完工后不能再添加任务,确定继续?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                UpdateOrderTo(Table.ProjectS.EnumEnable.验收);
        }

        private void ZuoFei()
        {
            if (MessageWindow.Show("操作不可逆", "项目作废将导致其相关的任务和反馈被作废,确定继续?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                UpdateOrderTo(Table.ProjectS.EnumEnable.作废);
        }

        private void Save()
        {
            order.Money = doubleUpDownMoney.Value.Value;
            order.Name = textBoxName.Text;
            order.CorSId = corSelect1.SelectedObjectId;
            order.OrderNO = textBoxOrderNO.Text;
            order.OrderDateTime = datePickerOrderDate.SelectedDate.Value;

            Table.DataTableS.EnumDatabaseStatus result = order.Id == -1 ? order.Insert() : order.Update();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }

        private void UpdateOrderTo(Table.ProjectS.EnumEnable enable)
        {
            switch (order.Enable)
            {
                case Table.ProjectS.EnumEnable.评估: order.StartDateTime = datePickerOrderDate.SelectedDate.Value; break;
                case Table.ProjectS.EnumEnable.实施: order.EndDateTime = datePickerOrderDate.SelectedDate.Value; break;
            }
            Table.DataTableS.EnumDatabaseStatus result = order.UpdateTo(enable);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageBox.Show(result.ToString());
        }
    }
}