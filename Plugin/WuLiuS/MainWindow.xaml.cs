﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace WuLiuS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.WuLiuS order = null;
        public void Init(DataTableText tableText, Table.WuLiuS obj)
        {
            order = obj ?? new Table.WuLiuS();
            if (order.Id == -1)
                switch (tableText)
                {
                    case DataTableText.采购物流跟踪: comboBoxRelatedOrderNO.ItemsSource = Table.WuLiuS.GetListForRelatedOrderNO(Table.WuLiuS._EnumFlag.采购物流跟踪); break;
                    case DataTableText.批发物流跟踪: comboBoxRelatedOrderNO.ItemsSource = Table.WuLiuS.GetListForRelatedOrderNO(Table.WuLiuS._EnumFlag.批发物流跟踪); break;
                    case DataTableText.仓库物流跟踪: comboBoxRelatedOrderNO.ItemsSource = Table.WuLiuS.GetListForRelatedOrderNO(Table.WuLiuS._EnumFlag.仓库物流跟踪); break;
                }
            else
                comboBoxRelatedOrderNO.Items.Add(order.RelatedOrderNO);

            comboBoxWuLiu.DisplayMemberPath = "Name";
            comboBoxWuLiu.SelectedValuePath = "Id";
            comboBoxWuLiu.ItemsSource = Table.CorS.GetWuLiuList();

            InitOrder();
            InitEvent();
        }

        private void InitOrder()
        {
            datePickerDateTime.SelectedDate = order.OrderDateTime;
            comboBoxWuLiu.Text = order._Company;
            doubleUpDownMoney.Value = order.Money;
            textBoxNote.Text = order.Note;
            textBoxOrderNO.Text = order.OrderNO;
            checkBoxMoney.IsChecked = (order.Money == 0);
            comboBoxRelatedOrderNO.SelectedValue = order.RelatedOrderNO;
            buttonSave.IsEnabled = (order.Enable == Table.WuLiuS.EnumEnable.已发货);
        }

        private void InitEvent()
        {
            buttonSave.Click += (ss, ee) => Save();
            checkBoxMoney.Click += (ss, ee) => CheckMoney();
            doubleUpDownMoney.ValueChanged += (ss, ee) => CheckMoney();
        }

        private void CheckMoney()
        {
            if (checkBoxMoney.IsChecked.Value)
                doubleUpDownMoney.Value = 0;
        }

        private void Save()
        {
            if (!checkBoxMoney.IsChecked.Value && doubleUpDownMoney.Value == 0)
                if (MessageWindow.Show("", "确定运费金额为 0 吗?", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;

            order.OrderDateTime = datePickerDateTime.SelectedDate.Value;
                Table.CorS cs = comboBoxWuLiu.SelectedItem as Table.CorS;
                if (cs != null)
                {
                    order.CorSId = cs.Id;
                    order.WuLiuCompany = cs.WuLiuName;
                }

            order.OrderNO = textBoxOrderNO.Text;
            order.Money = checkBoxMoney.IsChecked.Value ? 0 : doubleUpDownMoney.Value.Value;
            order.Note = textBoxNote.Text;
            order.RelatedOrderNO = comboBoxRelatedOrderNO.Text;

            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}