﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility;

namespace MoneyS
{
    /// <summary>
    /// ShouKuan.xaml 的交互逻辑
    /// </summary>
    public partial class FuKuan : Window
    {
        public FuKuan()
        {
            InitializeComponent();

            InitEvent();
        }

        double selectMoney = 0;

        public void Init(Table.PayS obj)
        {
            ZhiFuFangShi.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.支付方式, Table.UniqueS.EnumEnable.启用);
            ZhiFuFangShi.SelectedValuePath = "Id";
            ZhiFuFangShi.SelectedValueBinding = new Binding("UniqueSId");
            ZhiFuFangShi.DisplayMemberPath = "Name";
            
            comboBoxBank.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.银行账户, Table.UniqueS.EnumEnable.启用);
            comboBoxBank.DisplayMemberPath = "Name";
            comboBoxBank.SelectedValuePath = "Id";
            if (obj != null)
                comboBoxBank.SelectedValue = obj.BankId;

            SelectedCustomerS(obj == null ? -1 : obj.CorSId);
        }

        private void GetBill()
        {
            List<Table.PayS> payS = Table.PayS.GetList("select * from PayS where Flag="+(Int64)Table.PayS.EnumFlag.付款单+" and Enable="+(Int64)Table.PayS.EnumEnable.评估+" and CorSId="+customerSinfo1.SelectedCorSId);
            dataGridObject.ItemsSource = payS;
            double d = 0;
            foreach (Table.PayS row in payS)
                d += row.Money;

            label1Due.Content = d;
            ckbSelectedAll_Checked(null, null);
        }

        private void SelectedCustomerS(Int64 corSId)
        {
                customerSinfo1.SelectedCorSId = corSId;
                GetBill();
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => { Save(); };
            customerSinfo1.CorSChanged += (ee) => { GetBill(); };
            dataGridObject.SelectionChanged += (ss, ee) => { SetMoney(); };
        }

        private void Save()
        {
            if (string.IsNullOrEmpty(comboBoxBank.Text))
            {
                MessageWindow.Show("请选择银行账户");
                return;
            }
            foreach (object item in dataGridObject.Items)
            {
                    var cntr = dataGridObject.ItemContainerGenerator.ContainerFromItem(item);
                    DataGridRow ObjROw = (DataGridRow)cntr;
                    if (ObjROw != null)
                    {
                        FrameworkElement objElement = dataGridObject.Columns[0].GetCellContent(ObjROw);
                        if (objElement != null)
                        {
                            System.Windows.Controls.CheckBox objChk = (System.Windows.Controls.CheckBox)objElement;
                            if (objChk.IsChecked != true)
                                continue;

                            Table.PayS ps = (Table.PayS)item;
                            ps.BankId = (Int64)comboBoxBank.SelectedValue;
                            if (!string.IsNullOrEmpty(ps.Invoice))
                                ps.UseInvoice = true;

                            ps.Update();
                        }
                    }
            }
            this.Close();
        }

        private void ckbSelectedAll_Checked(object sender, RoutedEventArgs e)
        {
            dataGridObject.SelectAll();
            selectMoney = (double)label1Due.Content;
            labelMoney.Content = selectMoney;
        }

        private void ckbSelectedAll_Unchecked(object sender, RoutedEventArgs e)
        {
            dataGridObject.UnselectAll();
            selectMoney = 0;
            labelMoney.Content = selectMoney;
        }

        private void SetMoney()
        {
            if (dataGridObject.SelectedItem == null)
                return;

            Table.PayS ps = (Table.PayS)dataGridObject.SelectedItem;
            var cntr = dataGridObject.ItemContainerGenerator.ContainerFromItem(dataGridObject.SelectedItem);
            DataGridRow ObjROw = (DataGridRow)cntr;
            if (ObjROw != null)
            {
                FrameworkElement objElement = dataGridObject.Columns[0].GetCellContent(ObjROw);
                if (objElement != null)
                {
                    System.Windows.Controls.CheckBox objChk = (System.Windows.Controls.CheckBox)objElement;
                    if (objChk.IsChecked != true)
                        selectMoney += ps.Money;
                    else
                        selectMoney -= ps.Money;
                }
            }
            labelMoney.Content = selectMoney;
        }
    }
}
