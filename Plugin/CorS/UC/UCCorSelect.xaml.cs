﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;

namespace CorS
{
    /// <summary>
    /// CustomerSelect.xaml 的交互逻辑
    /// </summary>
    public partial class UCCorSelect : UserControl
    {
        public UCCorSelect()
        {
            InitializeComponent();

            InitEvent();
        }

        bool isCustomer = true;
        public delegate void ChangedCorS(Table.CorS CorS);
        public event ChangedCorS CorSChanged;

        Table.CorS CorS = null;
        /// <summary>
        /// 已选择的客户,有可能为 null
        /// </summary>
        public Table.CorS SelectedObject
        {
            get { return CorS;}
            set
            {
                CorS = value;
                AfterSelectedCorS();
            }
        }
        /// <summary>
        /// 已选择的客商Id,如果为null则返回 -1;
        /// </summary>
        public Int64 SelectedObjectId
        {
            get
            {
                if (CorS == null)
                    return -1;
                else
                    return CorS.Id;
            }
            set
            {
                SelectedObject = Table.CorS.GetObject(value);
            }
        }

        public void Init(bool isCustomer)
        {
            this.isCustomer = isCustomer;
            textBoxExtText.Init(Table.CorS.tableName, new string[] { "Name", "Tel", "PinYinL", "PinYinS" }, new string[] { "Name", "Id"}, "Id");
            textBoxExtText.Focus();
        }

        private void InitEvent()
        {
            buttonBrowser.Click += (ss, ee) => ButtonClick(ee);
            textBoxExtText.SelectedRow += (ee) => TextBoxExtTextSelectedRow(ee);
            this.LostKeyboardFocus += (ss, ee) => TextBoxExtTextLostFocus();
        }

        private void ButtonClick(RoutedEventArgs ee)
        {
            CorS.GetCorS gcs = new CorS.GetCorS(isCustomer, false);
            Point p = PointToScreen(Mouse.GetPosition(ee.Source as FrameworkElement));
            gcs.Left = p.X;
            gcs.Top = p.Y;
            gcs.ShowDialog();
            if (gcs.Selected != null)
            {
                CorS = gcs.Selected;
                AfterSelectedCorS();
                if (CorSChanged != null)
                    CorSChanged(CorS);
            }
        }

        private void TextBoxExtTextSelectedRow(System.Data.DataRow ee)
        {
                CorS = Table.CorS.GetObject((Int64)ee["Id"]);
                if (CorSChanged != null)
                    CorSChanged(CorS);
        }

        private void TextBoxExtTextLostFocus()
        {
            if (CorS == null)
                textBoxExtText.Text = string.Empty;
            else if (string.IsNullOrEmpty(textBoxExtText.Text))
                CorS = null;
        }

        private void AfterSelectedCorS()
        {
            if (CorS == null)
                textBoxExtText.Text = string.Empty;
            else
            {
                textBoxExtText.Text = CorS.Name;
                textBoxExtText.Focus();
            }
        }
    }
}
