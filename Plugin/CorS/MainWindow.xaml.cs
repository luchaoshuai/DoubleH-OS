﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;
using DoubleH.Utility.IO;

namespace CorS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.CorS order;
        bool isCustomerS = true;

        public void Init(bool customerS, Table.CorS obj)
        {
            order = obj;
            isCustomerS = customerS;
            if (order == null)
                order = new Table.CorS(isCustomerS ? Table.CorS.EnumFlag.客户 : Table.CorS.EnumFlag.供应商);

            if (isCustomerS)
                InitCustomerS();
            else
                InitSupplierS();

            if (order.Flag == Table.CorS.EnumFlag.客户和供应商)
                checkBoxFlag.IsChecked = true;

            uCUniqueSVipType.Init(Table.UniqueS.EnumFlag.会员分类);
            uCUniqueSArea.Init(Table.UniqueS.EnumFlag.区域);

            checkComboBoxKeHuXingZhi.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.客户性质,Table.UniqueS.EnumEnable.启用);
            checkComboBoxKeHuXingZhi.DisplayMemberPath = "Name";
            checkComboBoxKeHuXingZhi.SelectedMemberPath = "Id";
            checkComboBoxKeHuXingZhi.ValueMemberPath = "Id";
            uCUniqueS1.Init(Table.UniqueS.EnumFlag.客户性质);
            InitVar();
            InitWuLiu();
            InitEvent();
            InitOrder();

            textBoxName.Focus();
        }

        public void Init(bool customerS,  Table.CorS obj,bool isWuLiu)
        {
            order = obj;
            if (order == null)
                order = new Table.CorS(isCustomerS ? Table.CorS.EnumFlag.客户 : Table.CorS.EnumFlag.供应商);

            if (customerS)
                order.Flag = Table.CorS.EnumFlag.客户和供应商;

            isCustomerS = false;
            order.IsWuLiu = isWuLiu;
            Init(false, order);
        }

        private void InitVar()
        {
            if (!Table.SysConfig.SysConfigParams.UseSalesOrderS)
                PiFaLingShou.Visibility = Visibility.Collapsed;

            dataGridExtContact.ShowHeaderMenu = false;
            dataGridExtContact.ShowBodyMenu = true;

            MenuItem mi = new MenuItem();
            mi.Header = "移除";
            mi.Click += (ss, ee) => RemoveContact();
            dataGridExtContact.BodyMenu.Items.Add(mi);
        }

        private void InitWuLiu()
        {
            if (!Table.SysConfig.SysConfigParams.UseWuLiuS)
            {
                checkBoxWuLiu.Visibility = Visibility.Collapsed;
                comboBoxWuLiu.Visibility = Visibility.Collapsed;
                return;
            }

            if (order.IsWuLiu)
            {
                checkBoxWuLiu.IsChecked = true;
                comboBoxWuLiu.Visibility = Visibility.Visible;
            }
            else
            {
                checkBoxWuLiu.IsChecked = false;
                comboBoxWuLiu.Visibility = Visibility.Collapsed;
            }
            comboBoxWuLiu.ItemsSource = Table.WuLiuS.WuLiuCompanyList;
            comboBoxWuLiu.DisplayMemberPath = "Key";
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>  Save();
            checkBoxWuLiu.Click += (ss, ee) => comboBoxWuLiu.Visibility = (checkBoxWuLiu.IsChecked.Value ? Visibility.Visible : Visibility.Collapsed);
        }

        private void RemoveContact()
        {
            if (dataGridExtContact.SelectedCells.Count == 0)
                return;

            Table.UserS ci = dataGridExtContact.SelectedCells[0].Item as Table.UserS;
            if (ci != null)
                order.AllContact.Remove(ci);
        }

        private void InitCustomerS()
        {
            this.Title = "客户资料";
            checkBoxFlag.Content = "也是供应商";
            uCGroupS1.Init(Table.GroupS.EnumFlag.客商分类);

        }

        private void InitSupplierS()
        {
            this.Title = "供应商资料";
            checkBoxFlag.Content = "也是客户";
            uCGroupS1.Init(Table.GroupS.EnumFlag.客商分类);

            PiFaLingShou.Visibility = Visibility.Hidden;
            tabItemVip.Visibility = Visibility.Hidden;
        }

        private void InitOrder()
        {
            comboBoxWholesalePrice.ItemsSource = Enum.GetNames(typeof(Table.CorS.EnumWholesalePrice));

            dataGridExtContact.ItemsSource = order.AllContact;
            textBoxEmail.Text = order.Email;
            textBoxName.Text = order.Name;
            textBoxAddress.Text = order.Address;
            textBoxCorporation.Text = order.Corporation;
            textBoxFax.Text = order.Fax;
            textBoxNote.Text = order.Note;
            textBoxPost.Text = order.Post;
            textBoxTel.Text = order.Tel;
            textBoxCode.Text = order.Code;
            textBoxIdentificationCard.Text = order.IdentificationCard;
            textBoxCode.IsEnabled = (order.GroupSId == -1);
            checkBoxEnable.IsChecked = order.Enable == Table.CorS.EnumEnable.停用 ? true : false;
            checkBoxSex.IsChecked = order.Sex;
            uCGroupS1.SelectedObjectId = order.GroupSId;
            checkComboBoxKeHuXingZhi.SelectedValue = order.KeHuXingZhi;
            uCUniqueSArea.SelectedObjectId = order.AreaSId;
            //批发销售
            comboBoxWholesalePrice.SelectedValue = order.WholesalePrice.ToString();
            //证件信息
            datePicker营业执照.SelectedDate = order.YingYeZhiZhao;
            integerUpDown营业执照.Value = (int)order.YingYeZhiZhaoMonth;
            textBox营业执照.Text = order.YingYeZhiZhaoNO;
            datePicker税务登记证.SelectedDate = order.ShuiWuDengJiZheng;
            integerUpDown税务登记证.Value = (int)order.ShuiWuDengJiZhengMonth;
            textBox税务登记证.Text = order.ShuiWuDengJiZhengNO;
            datePicker机构代码证.SelectedDate = order.ZuZhiJiGouDaiMaZheng;
            integerUpDown机构代码证.Value = (int)order.ZuZhiJiGouDaiMaZhengMonth;
            textBox机构代码证.Text = order.ZuZhiJiGouDaiMaZhengNO;
            //vip
            textBoxVipNO.IsEnabled = order.Id == -1;
            textBoxVipNO.Text = order.VipNO;
            uCUniqueSVipType.SelectedObjectId = order.VipUniqueSId;
            datePickerVipEnd.SelectedDate = order.VipDateEnd;
            datePickerVipStart.SelectedDate = order.VipDateStart;
            checkBoxVipEnable.IsChecked = order.VipUsed;
            gridVip.IsEnabled = !order.VipUsed;

            //物流
            comboBoxWuLiu.Text = order.WuLiuName;
        }

        private void Save()
        {
            if (checkBoxFlag.IsChecked == true)
                order.Flag = Table.CorS.EnumFlag.客户和供应商;
            else
                order.Flag = isCustomerS ? Table.CorS.EnumFlag.客户 : Table.CorS.EnumFlag.供应商;

            order.VipDateStart = datePickerVipStart.SelectedDate.Value;
            order.VipDateEnd = datePickerVipEnd.SelectedDate.Value;
            order.VipNO = textBoxVipNO.Text;
            order.VipUniqueSId = uCUniqueSVipType.SelectedObjectId;
            order.VipUsed = checkBoxVipEnable.IsChecked == true;
          
            order.YingYeZhiZhao = datePicker营业执照.SelectedDate.Value;
            order.YingYeZhiZhaoMonth = integerUpDown营业执照.Value.Value;
            order.YingYeZhiZhaoNO = textBox营业执照.Text;
            order.ShuiWuDengJiZheng = datePicker税务登记证.SelectedDate.Value;
            order.ShuiWuDengJiZhengMonth = integerUpDown税务登记证.Value.Value;
            order.ShuiWuDengJiZhengNO = textBox税务登记证.Text;
            order.ZuZhiJiGouDaiMaZheng = datePicker机构代码证.SelectedDate.Value;
            order.ZuZhiJiGouDaiMaZhengMonth = integerUpDown机构代码证.Value.Value;
            order.ZuZhiJiGouDaiMaZhengNO = textBox机构代码证.Text;

            order.IdentificationCard = textBoxIdentificationCard.Text;
            order.Sex = checkBoxSex.IsChecked.Value;
            order.Email = textBoxEmail.Text;
            order.Code = textBoxCode.Text;
            order.Name = textBoxName.Text;
            order.Address = textBoxAddress.Text;
            order.Corporation = textBoxCorporation.Text;
            order.Fax = textBoxFax.Text;
            order.Note = textBoxNote.Text;
            order.Post = textBoxPost.Text;
            order.Tel = textBoxTel.Text;
            order.GroupSId = uCGroupS1.SelectedObjectId;
            order.KeHuXingZhi = checkComboBoxKeHuXingZhi.SelectedValue;
            order.AreaSId = uCUniqueSArea.SelectedObjectId;
            order.WholesalePrice = (Table.CorS.EnumWholesalePrice)Enum.Parse(typeof(Table.CorS.EnumWholesalePrice), comboBoxWholesalePrice.Text);
            order.Enable = checkBoxEnable.IsChecked == true ? Table.CorS.EnumEnable.停用 : Table.CorS.EnumEnable.启用;
            order.AllContact = dataGridExtContact.ItemsSource as System.Collections.ObjectModel.ObservableCollection<Table.UserS>;

            order.IsWuLiu = checkBoxWuLiu.IsChecked.Value;
            order.WuLiuName = comboBoxWuLiu.Text;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
            {
                order.UpdateCorSInfo();
                this.Close();
            }
            else
                MessageWindow.Show(result.ToString());
        }
    }
}