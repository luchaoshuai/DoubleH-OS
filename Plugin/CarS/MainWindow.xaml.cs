﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace CarS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            buttonSave.Click += (ss, ee) => { Save(); };
        }

        Table.CarS orderS = null;
        public void Init(Table.CarS car)
        {
            doubleUpDownDay.FormatString = doubleUpDownHour.FormatString = doubleUpDownMonth.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();

            textBoxName.Focus();
            orderS = car ?? new Table.CarS();
            if (!Table.SysConfig.SysConfigParams.UseCarRentS)
            {
                groupBoxRent.IsEnabled = false;
                this.Height = groupBoxRent.Margin.Top+SystemParameters.CaptionHeight;
            }

            textBoxEngineNO.Text = orderS.EngineNO;
            textBoxModel.Text = orderS.Model;
            textBoxName.Text = orderS.Name;
            textBoxNote.Text = orderS.Note;
            textBoxOwner.Text = orderS.Owner;
            textBoxVin.Text = orderS.Vin;

            dateTimePickerExaminDate.Value = orderS.ExamineDate;
            dateTimePickerInsuranceDate.Value = orderS.InsuranceDate;
            dateTimePickerRegisterDate.Value = orderS.RegisterDate;

            checkBoxEnable.IsChecked = orderS.Enable == Table.CarS.EnumEnable.停用;

            doubleUpDownHour.Value = orderS.RentHour;
            doubleUpDownDay.Value = orderS.RentDay;
            doubleUpDownMonth.Value = orderS.RentMonth;
        }

        private void Save()
        {
            if (!Verify())
                return;

            orderS.EngineNO = textBoxEngineNO.Text;
            orderS.Model = textBoxModel.Text;
            orderS.Name = textBoxName.Text;
            orderS.Note = textBoxNote.Text;
            orderS.Owner = textBoxOwner.Text;
            orderS.Vin = textBoxVin.Text;

            orderS.ExamineDate = dateTimePickerExaminDate.Value.Value;
            orderS.InsuranceDate = dateTimePickerInsuranceDate.Value.Value;
            orderS.RegisterDate = dateTimePickerRegisterDate.Value.Value;

            if (checkBoxEnable.IsChecked == true)
                orderS.Enable = Table.CarS.EnumEnable.停用;

            orderS.RentHour = doubleUpDownHour.Value.Value;
            orderS.RentDay = doubleUpDownDay.Value.Value;
            orderS.RentMonth = doubleUpDownMonth.Value.Value;

            Table.DataTableS.EnumDatabaseStatus result = Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (orderS.Id == -1)
                result = orderS.Insert();
            else
                result = orderS.Update();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }

        private bool Verify()
        {
            bool b = true;
            if (string.IsNullOrWhiteSpace(textBoxName.Text))
            {
                b = false;
                MessageBox.Show("车牌不能为空");
                textBoxName.Focus();
            }
            return b;
        }
    }
}
