﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Diagnostics;

namespace PromotionS
{
    /// <summary>
    /// WindowCoupons.xaml 的交互逻辑
    /// </summary>
    public partial class WindowCoupons : Window
    {
        public WindowCoupons()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.CouponsS order = null;
        public void Init(Table.CouponsS order)
        {
            this.order = order ?? new Table.CouponsS();
            InitOrder();
        }

        private void InitOrder()
        {
            textBoxName.Text = order.Name;
            doubleUpDownMoney.Value = order.Money;
            dateTimePickerStart.Value = order.StartDateTime;
            dateTimePickerEnd.Value = order.EndDateTime;
            textBoxNOStart.Text = order.OrderNO;
            textBoxNOEnd.Text = order.OrderNO;
            textBoxNote.Text = order.Note;
            checkBoxJiFen.IsChecked = order.IsScore;

            if (order.Id == -1)
                buttonZuoFei.IsEnabled = false;
            else
                buttonSave.IsEnabled = false;
        }

        private void InitVar()
        {
            listBoxProductSIdList.DisplayMemberPath = "Name";
        }

        private void InitEvent()
        {
            buttonProduct.Click += (ss, ee) => SelectProductS();
            buttonSave.Click += (ss, ee) => SaveOrder();
            buttonZuoFei.Click += (ss, ee) => ZuoFeiOrder();
        }

        private void SelectProductS()
        {
            ProductS.GetProductS pgs = new ProductS.GetProductS();
            pgs.Init(ProductS.GetProductS.EnumProductS.询价单商品, null);
            pgs.ShowDialog();
            listBoxProductSIdList.ItemsSource = pgs.Selected;
        }

        private void SaveOrder()
        {
            Debug.Assert(order != null);

            order.Name = textBoxName.Text;
            order.Money = doubleUpDownMoney.Value.Value;
            order.Note = textBoxNote.Text;
            order.StartDateTime = dateTimePickerStart.Value.Value;
            order.EndDateTime = dateTimePickerEnd.Value.Value;
            if (listBoxProductSIdList.HasItems)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Table.ProductS ps in listBoxProductSIdList.ItemsSource)
                    sb.Append(ps.Id + ",");

                order.ProductSIdList = sb.Remove(sb.Length - 1, 1).ToString();
            }
            Table.DataTableS.EnumDatabaseStatus result = order.Insert(textBoxNOStart.Text, textBoxNOEnd.Text);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                DoubleH.Utility.MessageWindow.Show(result.ToString());
        }

        private void ZuoFeiOrder()
        {
            Debug.Assert(order != null);
            Table.DataTableS.EnumDatabaseStatus result = order.ZuoFei(textBoxNOStart.Text, textBoxNOEnd.Text);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                DoubleH.Utility.MessageWindow.Show(result.ToString());
        }
    }
}
