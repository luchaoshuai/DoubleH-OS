﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace PromotionS
{
    /// <summary>
    /// WindowPromotion.xaml 的交互逻辑
    /// </summary>
    public partial class WindowPromotion : Window
    {
        public WindowPromotion()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Table.PromotionS order = null;
        public void Init(Table.PromotionS order)
        {
            this.order = order ?? new Table.PromotionS();
            InitOrder();
        }

        private void InitOrder()
        {
            textBoxName.Text = order.Name;
            dateTimePickerStart.Value = order.StartDateTime;
            dateTimePickerEnd.Value = order.EndDateTime;
            doubleUpDownMoneyMin.Value = order.MoneyMin;
            doubleUpDownMoneyMax.Value = order.MoneyMax;

            comboBoxFlag.Text = order.Flag.ToString();
            //ChangeFlag();
            if (order.Flag!=Table.PromotionS.EnumFlag.赠品)
            {
                double d = 0;
                double.TryParse(order.FlagValue, out d);
                doubleUpDownZengSong.Value = d;
            }
            else
                listBoxZengSong.ItemsSource = GetList(order.FlagValue);

            checkListBoxVip.SelectedValue = order.VipList;
            textBoxNote.Text = order.Note;
            listBoxProductSIdList.ItemsSource = GetList(order.ProductSIdList);

            if (order.Id == -1)
                buttonZuoFei.IsEnabled = false;
            else
                buttonSave.IsEnabled = false;
        }

        private void InitVar()
        {
            checkListBoxVip.DisplayMemberPath = "Name";
            checkListBoxVip.ValueMemberPath= "Id";
            checkListBoxVip.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.会员分类, Table.UniqueS.EnumEnable.启用);

            listBoxZengSong.DisplayMemberPath = "Name";
            listBoxProductSIdList.DisplayMemberPath = "Name";
            comboBoxFlag.ItemsSource = Enum.GetNames(typeof(Table.PromotionS.EnumFlag));
        }

        private void InitEvent()
        {
            buttonZengSong.Click += (ss, ee) => SelectZengSong();
            buttonProduct.Click += (ss, ee) => SelectProductS();
            buttonSave.Click += (ss, ee) => SaveOrder();
            buttonZuoFei.Click += (ss, ee) => ZuoFeiOrder();
            doubleUpDownZengSong.ValueChanged += (ss, ee) => ZengSongValueProperty();
        }

        private void ZengSongValueProperty()
        {
            if (comboBoxFlag.Text== Table.PromotionS.EnumFlag.价格折扣.ToString())
            {
                doubleUpDownZengSong.Minimum = 0.01;
                doubleUpDownZengSong.Increment = 0.01;
                doubleUpDownZengSong.Maximum = 1;
            }
            else
            {
                doubleUpDownZengSong.Minimum =0;
                doubleUpDownZengSong.Increment = 1;
                doubleUpDownZengSong.Maximum = double.MaxValue;
            }
        }

        //private void ChangeFlag()
        //{
        //    bool zengPin = false;
        //    if (comboBoxFlag.Text == Table.PromotionS.EnumFlag.赠品.ToString())
        //        zengPin = true;

        //    buttonZengSong.IsEnabled = zengPin;
        //    doubleUpDownZengSong.IsEnabled = !zengPin;
        //}

        private void SelectZengSong()
        {
            ProductS.GetProductS pgs = new ProductS.GetProductS();
            pgs.Init(ProductS.GetProductS.EnumProductS.询价单商品, null);
            pgs.ShowDialog();
            listBoxZengSong.ItemsSource = pgs.Selected;
        }

        private void SelectProductS()
        {
            ProductS.GetProductS pgs = new ProductS.GetProductS();
            pgs.Init(ProductS.GetProductS.EnumProductS.询价单商品, null);
            pgs.ShowDialog();
            listBoxProductSIdList.ItemsSource = pgs.Selected;
        }

        private void SaveOrder()
        {
            Debug.Assert(order != null);

            order.Name = textBoxName.Text;
            order.StartDateTime = dateTimePickerStart.Value.Value;
            order.EndDateTime = dateTimePickerEnd.Value.Value;
            order.MoneyMin = doubleUpDownMoneyMin.Value.Value;
            order.MoneyMax = doubleUpDownMoneyMax.Value.Value;

            order.Flag=(Table.PromotionS.EnumFlag)Enum.Parse(typeof(Table.PromotionS.EnumFlag),comboBoxFlag.Text);
            if (comboBoxFlag.Text == Table.PromotionS.EnumFlag.赠品.ToString())
            {
                if (listBoxZengSong.HasItems)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Table.ProductS ps in listBoxZengSong.ItemsSource)
                        sb.Append(ps.Id + ",");

                    order.FlagValue = sb.Remove(sb.Length - 1, 1).ToString();
                }
            }
            else
                order.FlagValue = doubleUpDownZengSong.Value.Value.ToString();

            order.VipList = checkListBoxVip.SelectedValue;
            order.Note = textBoxNote.Text;

            if (listBoxProductSIdList.HasItems)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Table.ProductS ps in listBoxProductSIdList.ItemsSource)
                    sb.Append(ps.Id + ",");

                order.ProductSIdList = sb.Remove(sb.Length - 1, 1).ToString();
            }

            Table.DataTableS.EnumDatabaseStatus result = order.Insert();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                DoubleH.Utility.MessageWindow.Show(result.ToString());
        }

        private void ZuoFeiOrder()
        {
            Debug.Assert(order != null);
            Table.DataTableS.EnumDatabaseStatus result = order.ZuoFei();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                DoubleH.Utility.MessageWindow.Show(result.ToString());
        }

        private ObservableCollection<Table.ProductS> GetList(string idString)
        {
            if (string.IsNullOrEmpty(idString))
                return null;

            List<Int64> ll = new List<long>();
            foreach (string str in idString.Split(','))
            {
                Int64 i = 0;
                if (Int64.TryParse(str, out i))
                    ll.Add(i);
            }
            return Table.ProductS.GetList(ll.ToArray());
        }
    }
}
