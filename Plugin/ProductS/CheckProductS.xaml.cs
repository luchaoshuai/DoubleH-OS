﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table=FCNS.Data.Table;
using System.Collections.ObjectModel;

namespace ProductS
{
    /// <summary>
    /// CheckProductS.xaml 的交互逻辑
    /// </summary>
    public partial class CheckProductS : Window
    {
        public CheckProductS()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        ObservableCollection<Table.ProductS> productSinObject = null;
        private void InitVar()
        {
            UserUIparams binding = new UserUIparams() { TableText = DataTableText.仓库商品 };
          foreach (DataGridColumnHeaderBinding item in binding.Items)
          {
              DataGridTextColumn gc = new DataGridTextColumn();
              gc.Width = item.Width;
              gc.Header = item.Header;
              gc.Binding = new Binding(item.BindingName);
              dataGridObject.Columns.Add(gc);
              item.Index = dataGridObject.Columns.Count;
          }
        }

        private void InitEvent()
        {
            dataGridObject.LoadingRow += (ss, ee) => { ee.Row.Header = ee.Row.GetIndex() + 1; };
            dataGridObject.MouseDoubleClick += (ss, ee) =>
            {
                if (dataGridObject.SelectedItem == null)
                    return;

                MainWindow mw = new MainWindow();
                mw.Init((Table.ProductS)dataGridObject.SelectedItem);
                mw.ShowDialog();
            };

            buttonAll.Click += (ss, ee) =>
                {
                    if (buttonAll.Content.ToString() == "全选")
                    {
                        buttonAll.Content = "全不选";
                        Grid grid = (Grid)groupBox1.Content;
                        foreach (UIElement u in grid.Children)
                            if(u is CheckBox)
                            ((CheckBox)u).IsChecked = true;
                    }
                    else
                    {
                        buttonAll.Content = "全选";
                        Grid grid = (Grid)groupBox1.Content;
                        foreach (UIElement u in grid.Children)
                            if(u is CheckBox)
                            ((CheckBox)u).IsChecked = false;
                    }
                };

            buttonSearch.Click += (ss, ee) =>
                {
                    if (productSinObject == null)
                        productSinObject = Table.ProductS.GetListBySearch(null, null);

                    var v = from f in productSinObject where (checkBoxEnable.IsChecked.HasValue?( checkBoxEnable.IsChecked == true ? f.Enable == Table.ProductS.EnumEnable.停用 : f.Enable == Table.ProductS.EnumEnable.启用):true) && 
                                (checkBoxScore.IsChecked.HasValue?(f.IsScore = checkBoxScore.IsChecked.Value):true) && (checkBoxDiscount.IsChecked.HasValue?(f.IsDiscount=checkBoxDiscount.IsChecked.Value):true) select f;

                    dataGridObject.ItemsSource = v;
                };
        }
    }
}
