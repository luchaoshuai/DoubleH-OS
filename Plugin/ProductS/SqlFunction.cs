﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FCNS.Data;
using System.Data;
using System.ComponentModel;
using Table = FCNS.Data.Table;

namespace ProductS
{
    class SqlFunction
    {
        public IEnumerable<StoreSProperty> GetStoreSProperty(Int64 productSId)
        {
            if (productSId == -1)
                return null;

            StringBuilder sb = new StringBuilder("select ProductSIO_1.Quantity as QuantityOut,ProductSIO.Quantity as QuantityIn,StoreS.Id,StoreS.Name,ProductSInStoreS.AveragePrice,ProductSInStoreS.Quantity,ProductSInStoreS.QuantityMax,ProductSInStoreS.QuantityMin ");
            sb.Append(" from StoreS  left join ProductSInStoreS on ProductSInStoreS.StoreSId=StoreS.Id and ProductSInStoreS.ProductSId=" + productSId);
            sb.Append(" left join ProductSIO on ProductSIO.ProductSId=" + productSId + " and ProductSIO.StoreSId=StoreS.Id and ProductSIO.Enable=0 and (ProductSIO.OrderNO like 'PF%TH%' or ProductSIO.OrderNO like 'CG%DD%' or ProductSIO.OrderNO like 'CG%ZP%')");
            sb.Append(" left join ProductSIO as ProductSIO_1 on ProductSIO_1.ProductSId=" + productSId + " and ProductSIO_1.StoreSId=StoreS.Id and ProductSIO_1.Enable=0 and (ProductSIO_1.OrderNO like 'PF%DD%' or ProductSIO_1.OrderNO like 'PF%ZP%' or ProductSIO_1.OrderNO like 'CG%TH%')");
            sb.Append(" where StoreS.Id in (select Id from StoreS where Enable=0)");
            DataRowCollection drc = SQLdata.GetDataRows(sb.ToString());
           
            List<StoreSProperty> sp = new List<StoreSProperty>();
            foreach (DataRow row in SQLdata.GetDataRows("select Id from StoreS where Enable=0"))
            {
                StoreSProperty ssp = new StoreSProperty();
                ssp.Id = (Int64)row["Id"];
                foreach (DataRow row2 in drc)
                {
                    if ((Int64)row2["Id"] != (Int64)row["Id"])
                        continue;

                    ssp.QuantityIn += (row2["QuantityIn"] is DBNull ? 0 : (double)row2["QuantityIn"]);
                    ssp.QuantityOut += (row2["QuantityOut"] is DBNull ? 0 : (double)row2["QuantityOut"]);

                    ssp.StoreSName = row2["Name"] as string;
                    ssp.AveragePrice = row2["AveragePrice"] is DBNull ? 0 : (double)row2["AveragePrice"];
                    ssp.Quantity = row2["Quantity"] is DBNull ? 0 : (double)row2["Quantity"];
                    ssp.QuantityMax = row2["QuantityMax"] is DBNull ? 0 : (double)row2["QuantityMax"];
                    ssp.QuantityMin = row2["QuantityMin"] is DBNull ? 0 : (double)row2["QuantityMin"];
                }
                sp.Add(ssp);
            }
            return sp;
        }
    }

    internal class StoreSProperty : IEditableObject ,INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        internal void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public Int64 Id { get; set; }
        public string StoreSName { get; set; }
        public double QuantityIn { get; set; }
        public double QuantityOut { get; set; }
        public double Quantity { get; set; }
        public double QuantityMin { get; set; }
        public double QuantityMax { get; set; }
        public double AveragePrice { get; set; }

        void IEditableObject.BeginEdit()
        {
        }

        void IEditableObject.CancelEdit()
        {
        }

        void IEditableObject.EndEdit()
        {
            if(QuantityMin>QuantityMax)
            {
                DoubleH.Utility.MessageWindow.Show("库存上限必须大于库存下限");
                QuantityMin=0;
                NotifyPropertyChanged("QuantityMin");
            }
        }
    }

}
