﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Table = FCNS.Data.Table;
using System.Data;

namespace ProductS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class GetWholesalePrice : Window
    {
        Table.ProductS product = null;
        public GetWholesalePrice(Table.ProductS obj)
        {
            InitializeComponent();

            if (obj == null)
                return;

            product = obj;
            ObservableCollection<SalesOrderUtility.PriceUtility> price = new ObservableCollection<SalesOrderUtility.PriceUtility>();
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "零售价", Money = obj.PosPrice });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "一级批发价", Money = obj.WholesalePrice1 });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "二级批发价", Money = obj.WholesalePrice2 });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "三级批发价", Money = obj.WholesalePrice3 });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "四级批发价", Money = obj.WholesalePrice4 });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "五级批发价", Money = obj.WholesalePrice5 });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "会员价", Money = obj.VipPrice });
            price.Add(new SalesOrderUtility.PriceUtility() { Name = "折扣价", Money = obj.PosPrice * obj._DiscountPrice.Value });
            //price.Add(new SalesOrderUtility.PriceUtility() { Name = "折扣价", Money = obj.IsDiscount ? obj.PosPrice * obj._DiscountPrice : obj.PosPrice });

            listView1.ItemsSource = price;
            listView1.SelectedValuePath = "Money";
            listView1.SelectedIndex = 0;

            InitEvent();
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => { SelectedPrice(); };
            listView1.MouseDoubleClick += (ss, ee) => { SelectedPrice(); };
        }

        private void SelectedPrice()
        {
            product._TempPrice = (double)listView1.SelectedValue;
            this.Close();
        }
    }
}
