﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;
using DoubleH.Utility;
using System.Collections;
using System.Data;
using System.IO;
using System.Windows.Threading;
using System.Diagnostics;
using System.Collections.Specialized;
using DoubleH.Utility.Configuration;

namespace ProductS.UC
{
    /// <summary>
    /// 商品编辑控件，自动根据配置是否允许负库存销售并计算所有相关值
    /// </summary>
    public partial class ProductSListInfo : UserControl
    {
        public ProductSListInfo()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitMenu();
            InitEvent();
        }

        ObservableCollection<Table.ProductS> productSinObject = null;
        TextBox editTextBox = null;
        ProductS.GetProductS getProductSWindow = null;
        public delegate void dTempValueEdited(DataGridCellEditEndingEventArgs ee);
        public event dTempValueEdited TempPriceEdited;

        #region 属性
        ///<summary>
        ///获取数据行数量
        ///</summary>
        public int Count
        {
            get { return productSinObject.Count; }
        }

        /// <summary>
        /// 获取或设置商品选择窗体
        /// </summary>
        public GetProductS.EnumProductS ProductSType
        {
            get;
            set;
        }

        public ObservableCollection<Table.ProductS> ItemsSource
        {
            get { return productSinObject; }
            set
            {
                InitDataGridObjectItemSources(value);
                //CalcQuantityAndMoneyOnBottom();
            }
        }
        /// <summary>
        /// 不提供底部统计功能
        /// </summary>
        public IEnumerable ItemsSourceNoCalc
        {
            get { return productSinObject; }
            set
            {
                //InitDataGridObjectItemSources(value);
                dataGridObject.ItemsSource = value;
                //CalcQuantityAndMoneyOnBottom();
            }
        }

        bool showAverage = false;
        /// <summary>
        /// 是否显示成本(默认:false)
        /// </summary>
        public bool ShowAverage
        {
            get
            {
                return showAverage;
            }
            set
            {
                showAverage = value;
                if (showAverage)
                {
                    SumAverageMoney.Visibility = Visibility.Visible;
                    SumAverageMoneyText.Visibility = Visibility.Visible;
                }
                else
                {
                    SumAverageMoney.Visibility = Visibility.Hidden;
                    SumAverageMoneyText.Visibility = Visibility.Hidden;
                }
            }
        }

        bool canSelectProduct = true;
        /// <summary>
        /// 能否双击控件选择商品(默认:true)
        /// </summary>
        public bool CanSelectProduct
        {
            get { return canSelectProduct; }
            set { canSelectProduct = value; }
        }

        bool canNegativeSales = false;
        /// <summary>
        /// 是否允许负销售(默认:调用数据库中的配置)
        /// </summary>
        public bool CanNegativeSales
        {
            get { return canNegativeSales; }
            set { canNegativeSales = value; }
        }

        public DoubleH.Utility.UC.DataGridExt DataGrid
        {
            get { return dataGridObject; }
        }

        /// <summary>
        /// 是否显示底部合计金额(默认:true)
        /// </summary>
        public bool ShowSumMoney
        {
            get { return SumMoney.Visibility == Visibility.Visible; }
            set
            {
                SumMoney.Visibility = value ? Visibility.Visible : Visibility.Hidden;
                SumMoneyText.Visibility = value ? Visibility.Visible : Visibility.Hidden;
            }
        }

        bool showBottom = true;
        /// <summary>
        /// 是否是否显示底部(默认:true)
        /// </summary>
        public bool ShowBottom
        {
            get { return showBottom; }
            set
            {
                showBottom = value;
                ShowSumMoney = value;
                SumQuantity.Visibility = value ? Visibility.Visible : Visibility.Hidden;
                SumQuantityText.Visibility = value ? Visibility.Visible : Visibility.Hidden;
                while (gridAll.RowDefinitions.Count > 0)
                    gridAll.RowDefinitions.RemoveAt(0);
                while (gridAll.ColumnDefinitions.Count > 0)
                    gridAll.ColumnDefinitions.RemoveAt(0);
            }
        }

        /// <summary>
        /// 是否显示编辑栏右键菜单
        /// </summary>
        public bool ShowBodyContexMenu
        {
            set { dataGridObject.ShowBodyMenu = value; }
        }

        //Table.CorS corS = null;
        ///// <summary>
        ///// 停用这个 根据客户Id计算对应的会员价格
        ///// </summary>
        //public Table.CorS CorS
        //{
        //    set
        //    {
        //        corS = value;
        //        if(value!=null)
        //        if ((ProductSType == GetProductS.EnumProductS.可采购商品 && DoubleHConfig.UserConfig.ProductPrice == 2) || ProductSType == GetProductS.EnumProductS.可销售商品)
        //            productSinObject.ToList<Table.ProductS>().ForEach(ps => CalcTempPrice(ps));
        //    }
        //}

        bool mustHasStoreS = true;
        /// <summary>
        /// 是否必须赋值 StoreS 属性,用于验证产品是否属于此仓库或者库存量是否足够
        /// </summary>
        public bool MustHasStoreS
        {
            get { return mustHasStoreS; }
            set { mustHasStoreS = value; }
        }

        Table.StoreS storeS = null;
        /// <summary>
        /// 设置仓库Id刷新相对应的库存和库存价,如果仓库不存在列表中的商品则自动移除商品
        /// </summary>
        public Table.StoreS StoreS
        {
            set
            {
                storeS = value;
                ChangedStoreS(value);
            }
        }

        //bool checkStoreS = false;
        ///// <summary>
        ///// 当仓库变更时是否检查商品列表(默认:false)
        ///// </summary>
        //public bool CheckStoreS
        //{
        //    get { return checkStoreS; }
        //    set { checkStoreS = value; }
        //}

        public string EnableText
        {
            set
            {
                labelEnable.Content = value;
            }
        }

        /// <summary>
        /// 获取右键菜单
        /// </summary>
        public ContextMenu BodyMenu
        {
            get { return dataGridObject.BodyMenu; }
        }

        public bool HasItems
        {
            get
            {
                if (productSinObject.Count == 1 && productSinObject[0].Id == -1)
                    return false;

                return productSinObject.Count != 0;
            }
        }

        public bool IsReadOnly
        {
            get { return dataGridObject.IsReadOnly; }
            set
            {
                dataGridObject.IsReadOnly = value;

                if (!BodyMenu.HasItems)
                    return;

                foreach (Control con in BodyMenu.Items)
                    if (con is MenuItem)
                        con.IsEnabled = !value;

                ((MenuItem)BodyMenu.Items[BodyMenu.Items.Count - 1]).IsEnabled = true;
            }
        }

        /// <summary>
        /// 仅在 fullrow 模式下生效
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return dataGridObject.SelectedIndex;
            }
            set
            {
                if (SelectionUnit == DataGridSelectionUnit.FullRow)
                    dataGridObject.SelectedIndex = value;
            }
        }

        /// <summary>
        /// 能否自动增加新行
        /// </summary>
        public bool CanUserAddRows
        {
            get { return dataGridObject.CanUserAddRows; }
            set { dataGridObject.CanUserAddRows = value; }
        }

        public DataGridSelectionUnit SelectionUnit
        {
            get { return dataGridObject.SelectionUnit; }
            set { dataGridObject.SelectionUnit = value; }
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 获取已选择的行或者单元格的所属对象
        /// </summary>
        /// <returns></returns>
        public Table.ProductS GetSelectedObject()
        {
            if (!HasItems)
                return null;

            return dataGridObject.SelectedObject() as Table.ProductS;
        }

        /// <summary>
        /// 如果不存在项，则新增一项
        /// 否则数量自动递增1
        /// </summary>
        /// <param name="index"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public void InsertItem(int index, Table.ProductS obj)
        {
            Debug.Assert(obj != null);
            Debug.Assert(obj._TempQuantity != 0);

            //Table.ProductS ps = productSinObject.FirstOrDefault<Table.ProductS>(f => f.Id == obj.Id);
            //if (ps == null || Table.SysConfig.SysConfigParams.ProductRow)
            //{
            //    obj._TempQuantity = obj._TempQuantity;
            //    obj.InitPrice(corS);
            productSinObject.Insert(index, obj);
            //    CalcQuantityAndMoneyOnBottom();
            //}
            //else
            //    ps._TempQuantity += 1;
        }

        /// <summary>
        /// 初始化 DataGrid 的Columns
        /// </summary>
        /// <param name="lvb"></param>
        public void Init(UserUIparams userParams)
        {
            dataGridObject.Init(userParams);
        }

        public void ResetAllProductQuantity(double newQuantity)
        {
            Debug.Assert(productSinObject != null);

            foreach (Table.ProductS ps in productSinObject)
                ps._TempQuantity = newQuantity;

            CalcQuantityAndMoneyOnBottom();
        }

        /// <summary>
        /// 检查库存是否足够
        /// </summary>
        /// <param name="ps"></param>
        /// <param name="newValue">待出库数量</param>
        /// <param name="append">true 附加出库数量到 _TempQuantity 值</param>
        /// <returns></returns>
        //private bool CalcQuantity(Table.ProductS obj, double quantity)
        // {
        //     if (ProductSType != GetProductS.EnumProductS.可销售商品 || obj.Flag == Table.ProductS.EnumFlag.服务)
        //         return true;

        //    Table.ProductS.GetNegativeQuantity(storeS.Id,
        //     if (!canNegativeSales &&( quantity + obj._Quantity)<0)
        //     {
        //         MessageWindow.Show(obj.Name + " - 库存不足");
        //         return false;
        //     }
        //     else if (append)
        //     {
        //         obj._TempQuantity += quantity;
        //         CalcQuantityAndMoneyOnBottom();
        //     }

        //     return true;
        // }
        #endregion

        #region 私有方法
        private void InitVar()
        {
            canNegativeSales = Table.SysConfig.SysConfigParams.NegativeSales;

            labelEnable.Content = string.Empty;

            dataGridObject.ShowBodyMenu = true;
            InitDataGridObjectItemSources(new ObservableCollection<Table.ProductS>());
        }

        private void InitEvent()
        {
            dataGridObject.MouseDoubleClick += (ss, ee) => DataGridMouseDoubleClick(ee);
            dataGridObject.PreparingCellForEdit += (ss, ee) => CellEditBegin(ee);
            dataGridObject.CellEditEnding += (ss, ee) => CellEditEnding(ee);
            dataGridObject.InitializingNewItem += new InitializingNewItemEventHandler(DataGrid_InitializingNewItem);
            dataGridObject.PreviewKeyDown += (ss, ee) => MoveSelectedObject(ee.Key);
            //先别删除,可能有用
            //dataGridObject.CurrentCellChanged += (ss, ee) =>
            //{
            //    if (isEditCell)
            //    {
            //        //CalcQuantityAndMoney();
            //        isEditCell = false;
            //    }
            //};
        }

        private void MoveSelectedObject(Key key)
        {
            switch (key)
            {
                case Key.Up: UpSelectedRow(); break;
                case Key.Down: DownSelectedRow(); break;
            }
        }

        private void UpSelectedRow()
        {
            Table.ProductS ps = GetSelectedObject();
            if (ps == null)
                return;

            int i = productSinObject.IndexOf(ps);
            if (i == 0)
                return;

            productSinObject.Move(i, i - 1);
            dataGridObject.CurrentCell = new DataGridCellInfo(dataGridObject.GetCell(i, 0));
        }

        private void DownSelectedRow()
        {
            Table.ProductS ps = GetSelectedObject();
            if (ps == null)
                return;

            int i = productSinObject.IndexOf(ps);
            if (i == productSinObject.Count - 1)
                return;

            productSinObject.Move(i, i + 1);
            dataGridObject.CurrentCell = new DataGridCellInfo(dataGridObject.GetCell(i, 0));
        }

        private void DataGrid_InitializingNewItem(object sender, InitializingNewItemEventArgs e)
        {
            if (productSinObject.Where(f => f.Id == -1).Count() > 1)
                productSinObject.RemoveAt(productSinObject.Count - 1);
        }

        private void InitDataGridObjectItemSources(ObservableCollection<Table.ProductS> obj)
        {
            productSinObject = obj;
            if (productSinObject != null)
                productSinObject.CollectionChanged += (ss, ee) => ItemsChanged(ss, ee);

            dataGridObject.ItemsSource = productSinObject;
            CalcQuantityAndMoneyOnBottom();
        }

        private void ItemsChanged(object ss, NotifyCollectionChangedEventArgs ee)
        {
            CalcQuantityAndMoneyOnBottom();
        }

        private void DataGridMouseDoubleClick(MouseButtonEventArgs ee)
        {
            if (IsReadOnly || !canSelectProduct)
                return;

            if (!(ee.OriginalSource is TextBlock))
                ShowGetProductS(false);
        }

        private void CellEditBegin(DataGridPreparingCellForEditEventArgs ee)
        {
            DataGridTextColumn textColumn = ee.Column as DataGridTextColumn;
            if (textColumn == null)
                return;

            string binding = ((Binding)textColumn.Binding).Path.Path;
            editTextBox = ee.EditingElement as TextBox;
            if (editTextBox == null)
                return;

            switch (binding)
            {
                case "Name":
                case "Code":
                case "Barcode":
                    editTextBox.UndoLimit = 0;//.net4.0 bug 没有它会导致TextChanged中输入中文出错
                    editTextBox.TextChanged += (s, e) =>
                    {
                        if (editTextBox == null)
                            return;

                        if (string.IsNullOrEmpty(editTextBox.Text))
                        {
                            if (getProductSWindow != null)
                            {
                                getProductSWindow.Close();
                                getProductSWindow = null;
                            }
                            return;
                        }
                        if (!ShowGetProductS(true))
                            return;

                        getProductSWindow.SearchText(editTextBox.Text, null);
                        editTextBox.Focus();
                    };
                    break;
            }
        }

        private void CellEditEnding(DataGridCellEditEndingEventArgs ee)
        {
            DataGridTextColumn textColumn = ee.Column as DataGridTextColumn;
            if (textColumn == null)
                return;

            string binding = ((Binding)textColumn.Binding).Path.Path;
            double d = 0;
            if (editTextBox == null)
                editTextBox = ee.EditingElement as TextBox;

            switch (binding)
            {
                case "_TempPrice":
                case "_AveragePrice":
                    double.TryParse(editTextBox.Text, out d);
                    editTextBox.Text = d.ToString();
                    CalcQuantityAndMoneyOnBottom();
                    break;

                case "_TempQuantity":
                    if (!double.TryParse(editTextBox.Text, out d))
                        editTextBox.Text = "0";
                    else
                    {
                        if (d == 0)
                            editTextBox.Text = "0";
                        else
                        {
                            if (d < 0 && (d + GetSelectedObject()._Quantity) < 0)
                                MessageWindow.Show("库存不足");

                            editTextBox.Text = d.ToString();
                        }
                    }
                    break;

                case "Name":
                case "Code":
                case "Barcode":
                    if (getProductSWindow != null)
                    {
                        getProductSWindow.Close();
                        getProductSWindow = null;
                    }
                    break;
            }

            var ps = from f in productSinObject where f.Id == -1 select f;
            for (int i = 1; i < ps.Count(); i++)
                productSinObject.Remove(ps.First());

            editTextBox = null;

            CheckEvent(binding, ee);
        }

        private void CheckEvent(string bindingName, DataGridCellEditEndingEventArgs ee)
        {
            switch (bindingName)
            {
                case "_TempPrice":
                    if (TempPriceEdited != null)
                        TempPriceEdited(ee);
                    break;
            }
        }

        private bool ShowGetProductS(bool mini)
        {
            if (getProductSWindow != null)
                return true;

            if (mustHasStoreS && storeS == null)
            {
                MessageWindow.Show(Table.DataTableS.EnumDatabaseStatus.仓库验证失败.ToString());
                return false;
            }

            getProductSWindow = new GetProductS();
            getProductSWindow.Closed += (s, e) =>
            {
                if (getProductSWindow.Selected != null)
                {
                    if (mini)
                        dataGridObject.RemoveSelectedItem();

                    foreach (Table.ProductS ps in getProductSWindow.Selected)
                        InsertItem(0, ps);
                }

                getProductSWindow = null;
                editTextBox = null;
                dataGridObject.CancelEdit();
            };
            getProductSWindow.Init(ProductSType, storeS);
            getProductSWindow.IsMini = mini;

            Point p = Mouse.GetPosition(this);
            Point pt = PointToScreen(Mouse.GetPosition(dataGridObject));
            getProductSWindow.Left = pt.X - p.X + 300;
            getProductSWindow.Top = pt.Y - p.Y;

            if (mini)
                getProductSWindow.Show();
            else
                getProductSWindow.ShowDialog();

            return true;
        }

        /// <summary>
        /// 计算合计数量与合计金额
        /// </summary>
        public void CalcQuantityAndMoneyOnBottom()
        {
            if (!showBottom)
                return;

            double sumQ = 0, sumM = 0;
            foreach (Table.ProductS ps in productSinObject)
            {
                sumQ += ps._TempQuantity;
                sumM += ps._TempSum;
            }
            SumQuantity.Text = sumQ.ToString();
            SumMoney.Text = sumM.ToString();

            //计算成本
            if (!showAverage)
                return;

            double sumA = 0;
            foreach (Table.ProductS ps in productSinObject)
                sumA += ps._TempQuantity * ps._AveragePrice;

            SumAverageMoney.Text = sumA.ToString();
        }

        private void ChangedStoreS(Table.StoreS obj)
        {
            if (!mustHasStoreS || canNegativeSales)
                return;

            if (obj == null || obj.Id == -1)
            {
                productSinObject.Clear();
                return;
            }

            Int64[] ids = Table.ProductSInStoreS.CheckQuantity(obj.Id, productSinObject.ToArray());
            if (ids != null)
            {
                //var vr = productSinObject.Where(f => !ids.Contains(f.Id));
                foreach (Int64 id in ids)
                {
                    Table.ProductS ps = productSinObject.FirstOrDefault(f => f.Flag != Table.ProductS.EnumFlag.免库存 && !ids.Contains(f.Id));
                    if (ps != null)
                        productSinObject.Remove(ps);
                }
                //foreach (Table.ProductS ps in vr)
                //    productSinObject.Remove(ps);
            }
            //productSinObject = new ObservableCollection<Table.ProductS>);

            dataGridObject.ItemsSource = productSinObject;
            Table.ProductS.InitQuantity(productSinObject, obj.Id);
        }
        #endregion

        #region 菜单
        private void InitMenu()
        {
            dataGridObject.BodyMenu.Items.Add(NewRow());
            //dataGridObject.BodyMenu.Items.Add(RemoveRow());
            dataGridObject.BodyMenu.Items.Add(RemoveAllRow());
            dataGridObject.BodyMenu.Items.Add(new Separator());
            dataGridObject.BodyMenu.Items.Add(UpRow());
            dataGridObject.BodyMenu.Items.Add(DownRow());
            dataGridObject.BodyMenu.Items.Add(new Separator());
            dataGridObject.BodyMenu.Items.Add(DaoRu());
            dataGridObject.BodyMenu.Items.Add(DaoChu());
            //dataGridObject.BodyMenu.Items.Add(new Separator());
            //MenuItem mi2 = new MenuItem();
            //mi2.Header = "查找单据";
            //mi2.Click += (ss, ee) =>
            //{
            //    FCNS.Data.Interface.DataIO io = new Table.ProductS();
            //    DoubleH.Utility.OpenFileWindow ofw = new OpenFileWindow();
            //    ofw.ShowDialog();
            //    foreach (Table.ProductS ps in io.Import(ofw.FileName))
            //        productSinObject.Add(ps);
            //};
            //dataGridObject.BodyMenu.Items.Add(mi1);
        }

        private MenuItem UpRow()
        {
            MenuItem mi0 = new MenuItem();
            mi0.Header = "向上移↑";
            mi0.Click += (ss, ee) => UpSelectedRow();
            return mi0;
        }

        private MenuItem DownRow()
        {
            MenuItem mi0 = new MenuItem();
            mi0.Header = "向下移↓";
            mi0.Click += (ss, ee) => DownSelectedRow();
            return mi0;
        }

        private MenuItem NewRow()
        {
            MenuItem mi0 = new MenuItem();
            mi0.Header = "新增行";
            mi0.Click += (ss, ee) =>
            {

            };
            return mi0;
        }

        //private MenuItem RemoveRow()
        //{
        //    MenuItem mi1 = new MenuItem();
        //    mi1.Header = "移除选中行";
        //    mi1.Click += (ss, ee) => RemoveSelectedItem();
        //    return mi1;
        //}

        //private void RemoveSelectedItem()
        //{
        //    Table.ProductS ps = GetSelectedObject();
        //    if (ps != null)
        //        productSinObject.Remove(ps);
        //    //CalcQuantityAndMoneyOnBottom();
        //}

        private MenuItem RemoveAllRow()
        {
            MenuItem mi3 = new MenuItem();
            mi3.Header = "清空";
            mi3.Click += (ss, ee) => productSinObject.Clear();
            return mi3;
        }

        private MenuItem DaoRu()
        {
            MenuItem mi1 = new MenuItem();
            mi1.Header = "导入数据";
            mi1.Click += (ss, ee) =>
            {
                DoubleH.Utility.IO.FileTools ft = new DoubleH.Utility.IO.FileTools();
                DataTable table = ft.ImportFileToDataTable(Table.ProductS.tableName, string.Empty);
                if (table == null)
                    return;

                if (!table.Columns.Contains("Id") || !table.Columns.Contains("Code") || !table.Columns.Contains("Barcode"))
                {
                    MessageWindow.Show("数据表列名不正确，请检查文件首行是否包含 Id/Code/Barcode 列头");
                    return;
                }

                foreach (DataRow row in table.Rows)
                {
                    Table.ProductS ps = GetProductSbyDaoRu(row["Id"] as string, row["Code"] as string, row["Barcode"] as string);
                    if (ps == null)
                        continue;

                    double temp = ps._TempPrice;
                    if (table.Columns.Contains("_TempPrice") && double.TryParse(row["_TempPrice"].ToString(), out temp))
                        ps._TempPrice = temp;

                    temp = 0;
                    if (table.Columns.Contains("_TempQuantity") && double.TryParse(row["_TempQuantity"].ToString(), out temp))
                        ps._TempQuantity = temp;

                    if (table.Columns.Contains("Note"))
                        ps.Note = row["Note"] as string;

                    productSinObject.Add(ps);
                }
            };

            return mi1;
        }

        private Table.ProductS GetProductSbyDaoRu(string id, string code, string barCode)
        {
            Table.ProductS ps = null;
            Int64 i = -1;
            if (!string.IsNullOrEmpty(id)&&Int64.TryParse(id, out i)&&i != -1)
                ps = Table.ProductS.GetObject(i);
            else if (!string.IsNullOrEmpty(code))
                ps = Table.ProductS.GetObject(code);
            else if (!string.IsNullOrEmpty(barCode))
                ps = Table.ProductS.GetObject(barCode);

            return ps;
        }

        private MenuItem DaoChu()
        {
            MenuItem mi1 = new MenuItem();
            mi1.Header = "导出数据";
            mi1.Click += (ss, ee) =>
            {
                DoubleH.Utility.IO.FileTools ft = new DoubleH.Utility.IO.FileTools();
                ft.ExportDataGridToFile(dataGridObject, string.Empty);
                //DoubleH.Utility.FileWindow ofw = new FileWindow();
                //ofw.IsOpen = false;
                //ofw.FileType = DoubleH.Utility.FileWindow.EnumFileType.商品控件的导入导出;
                //ofw.ShowDialog();
                //if (ofw.Canel)
                //    return;

                //StringBuilder sb = new StringBuilder();
                //foreach (Table.ProductS ps in productSinObject)
                //    sb.Append(ps.Id + "," + ps.Code + "," + ps.Barcode + "," + ps._TempPrice + "," + ps._TempQuantity + "," + ps.Note + "\r\n");

                //File.WriteAllText(ofw.FileName, sb.ToString());
                //MessageWindow.Show("导出成功");
            };

            return mi1;
        }
        #endregion
    }
}