﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using DoubleH.Utility;
using Table = FCNS.Data.Table;

namespace MessageS
{
    public class MessageSExt : Plugin
    {
        MainWindow window = null;
        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            window = new MainWindow();
            window.Owner = host.WorkAreaWindow;
            window.Init(DataTable as Table.ScheduleS);
            window.Show();
            //window.Closed+=(ss,ee)=>host.WorkAreaWindow.RefreshDataGrid();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();
        }
    }
}
