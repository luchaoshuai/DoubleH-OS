﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoubleH.Plugins;
using System.Diagnostics;
using System.Windows;
using Table = FCNS.Data.Table;
using DoubleH.Utility;

namespace GroupS
{
    public class GroupSExt : Plugin
    {
        private IPluginHost m_host = null;
        MainWindow form = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            form = new MainWindow();
            form.Init((Table.GroupS.EnumFlag)Enum.Parse(typeof(Table.GroupS.EnumFlag), TableText.ToString()), DataTable as Table.GroupS);
            form.ShowDialog();

            Terminate();
            return true;
        }
        public override void Terminate()
        {
            if (form != null)
                form.Close();
        }
    }
}