﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Collections.ObjectModel;
using DoubleH.Utility;

namespace HouseS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        ObservableCollection<Table.HouseS> allHouseS = null;
        Table.HouseS orderHouseS = new Table.HouseS();
        ObservableCollection<Table.RoomS> allRoomS = new ObservableCollection<Table.RoomS>();
        Table.RoomS orderRoomS = new Table.RoomS();

        public void Init()
        {
            InitVar();
            InitEvent();
            InitHouse();//默认是打开这个窗体
        }

        private void InitVar()
        {
            //house
            allHouseS = Table.HouseS.GetList(Table.HouseS.EnumEnable.启用);
            dataGridHouse.ItemsSource = allHouseS;
            //room
            comboBoxHouse.ItemsSource = allHouseS;
            comboBoxHouse.DisplayMemberPath = "Name";
            comboBoxHouse.SelectedValuePath = "Id";

            uCUniqueSRoomType.Init(Table.UniqueS.EnumFlag.房间类型);

            checkListBoxRoomFixedCost.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.公寓固定费用, Table.UniqueS.EnumEnable.启用);
            checkListBoxRoomFixedCost.DisplayMemberPath = "Name";
            checkListBoxRoomFixedCost.SelectedMemberPath = "Id";
            checkListBoxRoomFixedCost.ValueMemberPath = "Id";

            checkListBoxRoomOtherExpenses.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.公寓仪表费用, Table.UniqueS.EnumEnable.启用);
            checkListBoxRoomOtherExpenses.DisplayMemberPath = "Name";
            checkListBoxRoomOtherExpenses.SelectedMemberPath = "Id";
            checkListBoxRoomOtherExpenses.ValueMemberPath = "Id";
        }

        private void InitEvent()
        {
            buttonHouseNew.Click += (ss, ee) => NewHouse();
            buttonHouseSave.Click += (ss, ee) => SaveHouse();
            buttonHouseZuoFei.Click += (ss, ee) => ZuoFeiHouse();
            dataGridHouse.MouseDoubleClick += (ss, ee) => EditHouse();

            tabControl1.SelectionChanged += (ss, ee) => ChangedTabItem();
            treeViewLeftMenu.MouseLeftButtonUp+= (ss, ee) => ChangedFloor();
            comboBoxHouse.SelectionChanged += (ss, ee) => ChangedHouse();
            buttonRoomNew.Click += (ss, ee) => NewRoom();
            buttonRoomSave.Click += (ss, ee) => SaveRoom();
            buttonRoomZuoFei.Click += (ss, ee) => ZuoFeiRoom();
            dataGridRoom.MouseDoubleClick += (ss, ee) => EditRoom();
        }

        #region house
        private void InitHouse()
        {
            textBoxHouseName.Text = orderHouseS.Name;
            textBoxHouseNote.Text = orderHouseS.Note;
            textBoxHouseAddress.Text = orderHouseS.Address;
            textBoxHouseContact.Text = orderHouseS.Contact;
            textBoxHouseTel.Text = orderHouseS.Tel;
            integerUpDownHouseFloor.Value = (int)orderHouseS.Floor;
            checkBoxLift.IsChecked = orderHouseS.Lift;
            datePickerHouseDateTime.SelectedDate = orderHouseS.OrderDateTime;
        }

        private void EditHouse()
        {
            Table.HouseS hs = dataGridHouse.SelectedItem as Table.HouseS;
            if (hs == null)
                return;

            orderHouseS = hs;
            InitHouse();
        }

        private void NewHouse()
        {
            orderHouseS = new Table.HouseS();
            InitHouse();
        }

        private void SaveHouse()
        { 
            if (orderHouseS == null)
                return; 

            orderHouseS.Name = textBoxHouseName.Text;
            orderHouseS.Note = textBoxHouseNote.Text;
            orderHouseS.Address = textBoxHouseAddress.Text;
            orderHouseS.Contact = textBoxHouseContact.Text;
            orderHouseS.Tel = textBoxHouseTel.Text;
            orderHouseS.Floor = integerUpDownHouseFloor.Value.Value;
            orderHouseS.Lift = checkBoxLift.IsChecked.Value;
            orderHouseS.OrderDateTime = datePickerHouseDateTime.SelectedDate.Value;

            Table.DataTableS.EnumDatabaseStatus result;
            if (orderHouseS.Id == -1)
            {
                result = orderHouseS.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    allHouseS.Add(orderHouseS);
            }
            else
                result = orderHouseS.Update();

            MessageWindow.Show(result.ToString());
        }

        private void ZuoFeiHouse()
        {
            MessageWindow.Show(orderHouseS.ZuoFei().ToString());
            orderHouseS = new Table.HouseS();
            InitHouse();
        }
        #endregion

        #region room
        private void ChangedTabItem()
        {
            if (tabControl1.SelectedItem==tabItemRoom)
            {
                List<TempClass> tc = new List<TempClass>();
                foreach (Table.HouseS hs in allHouseS)
                {
                    TempClass t1 = new TempClass();
                    t1.Name = hs.Name;
                    t1.HouseSId = hs.Id;
                    for (int i = 1; i <= hs.Floor; i++)
                    {
                        TempClass t2 = new TempClass();
                        t2.Name = "第" + i + "层";
                        t2.HouseSId = hs.Id;
                        t2.RoomSId = i;
                        t1.SubItems.Add(t2);
                    }
                    tc.Add(t1);
                } 

                treeViewLeftMenu.ItemsSource = tc;
            }
        }

        private void ChangedFloor()
        {
            if (treeViewLeftMenu.SelectedItems.Count == 0)
                return;

            TempClass tc = treeViewLeftMenu.SelectedItems[0] as TempClass;
            allRoomS = Table.RoomS.GetList(tc.HouseSId, tc.RoomSId, Table.RoomS.EnumEnable.空置);
            if (allRoomS == null)
                allRoomS = new ObservableCollection<Table.RoomS>();

            dataGridRoom.ItemsSource = allRoomS;
        }

        private void ChangedHouse()
        {
            Table.HouseS hs = comboBoxHouse.SelectedItem as Table.HouseS;
            if (hs == null)
                return;

            comboBoxFloor.Items.Clear();
            for (int i = 1; i <= hs.Floor; i++)
                comboBoxFloor.Items.Add("第" + i + "层");
        }

        private void InitRoom()
        {
            comboBoxHouse.SelectedValue = orderRoomS.HouseSId;
            if (comboBoxFloor.Items.Count >= orderRoomS.FloorId)
            {
                if ((int)orderRoomS.FloorId > 0)
                    comboBoxFloor.SelectedIndex = (int)orderRoomS.FloorId - 1;
            }
            else
                MessageWindow.Show("楼层数量异常");

            textBoxRoomName.Text = orderRoomS.Name;
            uCUniqueSRoomType.SelectedObjectId = orderRoomS.RoomTypeId;
            comboBoxRoomDirection.Text = orderRoomS.Direction;
            doubleUpDownRoomArea.Value = orderRoomS.RoomArea;
            textBoxRoomNote.Text = orderRoomS.Note;
            checkBoxRoomBalcony.IsChecked = orderRoomS.HaveBalcony;
            checkBoxRoomKitchen.IsChecked = orderRoomS.HaveKitchen;
            checkBoxRoomToilet.IsChecked = orderRoomS.HaveToilet;
            checkListBoxRoomFixedCost.SelectedValue = orderRoomS.FixedCost;
            checkListBoxRoomOtherExpenses.SelectedValue = orderRoomS.OtherExpenses;
            textBoxOtherDevice.Text = orderRoomS.OtherDevice;
        }

        private void EditRoom()
        {
            Table.RoomS hs = dataGridRoom.SelectedItem as Table.RoomS;
            if (hs == null)
                return;

            orderRoomS = hs;
            InitRoom();
        }

        private void NewRoom()
        {
            orderRoomS = new Table.RoomS();
            InitRoom();
        }

        private void SaveRoom()
        {
            if (orderRoomS == null)
                return;

            if(comboBoxHouse.SelectedItem!=null)
            orderRoomS.HouseSId = (Int64)comboBoxHouse.SelectedValue;

            if (comboBoxFloor.SelectedItem != null)
                orderRoomS.FloorId = comboBoxFloor.SelectedIndex + 1;

            orderRoomS.Name = textBoxRoomName.Text;
            orderRoomS.RoomTypeId = uCUniqueSRoomType.SelectedObjectId;
            orderRoomS.Direction = comboBoxRoomDirection.Text;
            orderRoomS.RoomArea = doubleUpDownRoomArea.Value.Value;
            orderRoomS.Note = textBoxRoomNote.Text;
            orderRoomS.HaveBalcony = checkBoxRoomBalcony.IsChecked.Value;
            orderRoomS.HaveKitchen = checkBoxRoomKitchen.IsChecked.Value;
            orderRoomS.HaveToilet = checkBoxRoomToilet.IsChecked.Value;
            orderRoomS.FixedCost = checkListBoxRoomFixedCost.SelectedValue;
            orderRoomS.OtherExpenses = checkListBoxRoomOtherExpenses.SelectedValue;
            orderRoomS.OtherDevice = textBoxOtherDevice.Text;

            Table.DataTableS.EnumDatabaseStatus result;
            if (orderRoomS.Id == -1)
            {
                result = orderRoomS.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    allRoomS.Add(orderRoomS);
            }
            else
                result = orderRoomS.Update();

            MessageWindow.Show(result.ToString());
        }

        private void ZuoFeiRoom()
        {
            MessageWindow.Show(orderRoomS.ZuoFei().ToString());
            orderRoomS = new Table.RoomS();
            InitRoom();
        }
        #endregion

    }

    public class TempClass
    {
        public string Name { get; set; }

        Int64 houseSId = -1;
        public Int64 HouseSId { get { return houseSId; } set { houseSId = value; } }

        Int64? roomSId = null;
        public Int64? RoomSId { get { return roomSId; } set { roomSId = value; } }

        List<TempClass> tc = new List<TempClass>();
        public List<TempClass> SubItems
        {
            get { return tc; }
            set { tc = value; }
        }

        public override string ToString()
        {
            return Name  ;
        }
    }

}
