﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using System.Diagnostics;

namespace HouseS
{
    /// <summary>
    /// RentWindow.xaml 的交互逻辑
    /// </summary>
    public partial class RentWindow : Window
    {
        public RentWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.RoomIO order = null;
        public void Init(Table.RoomS order)
        {
            InitOrder(order);
        }

        private void InitEvent()
        {
            buttonHuan.Click += (ss, ee) => HuanFang();
            buttonTui.Click += (ss, ee) => TuiFang();
            buttonZu.Click += (ss, ee) => ZuFang();
            buttonShouKuan.Click += (ss, ee) => ShouKuan();
        }

        private void ShouKuan()
        {
            Table.DataTableS.EnumDatabaseStatus result=Table.DataTableS.EnumDatabaseStatus.操作成功;
            if (order == null)
                result = Table.RoomIO.EnumDatabaseStatus.操作失败;
            else
            {
                if (order.Id == -1)//如果是新租户，先添加租赁记录
                {
                    result = order.Insert();

                    if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                    {
                        Table.RoomMoneyIO money = new Table.RoomMoneyIO();
                        money.OrderNO = order.OrderNO;
                        money.OrderDateTime = DateTime.Now;
                        money.Money = order.Rent + order.Deposits;
                        money.MoneyDetail = "押金:" + order.Deposits + ";租金:" + order.Rent;
                        result = money.Insert();
                    }
                }
                else
                {
                }
            }
        }

        private void ZuFang()
        {
            CorSWindow cs = new CorSWindow();
            cs.ShowDialog();
            if (cs.SelectedObject == null)
                return;

                InitCorS(cs.SelectedObject);
                InitRoomIO(cs.SelectedObject);
        }

        private void TuiFang()
        {
        }

        private void HuanFang()
        {
        }

        private void InitOrder(Table.RoomS order)
        {
            InitRoom(order);


            if (order.Enable != Table.RoomS.EnumEnable.已租)
                return;

            Table.RoomIO io = Table.RoomIO.GetObject(order.Id);
            Debug.Assert(io != null);
            InitCorS(io);
            InitRoomIO(io);
        }

        private void InitRoom(Table.RoomS order)
        {
            switch (order.Enable)
            {
                case Table.RoomS.EnumEnable.空置: buttonZu.IsEnabled = true; break;
                case Table.RoomS.EnumEnable.已租:
                    buttonTui.IsEnabled = true;
                    buttonHuan.IsEnabled = true;
                    break;
            }

            labelHouseSName.Content = order._HouseSName;
            labelRoomSName.Content = order.Name;
            labelRoomType.Content = order._RoomTypeName;
            labelRoomEnable.Content = order.Enable;
        }

        private void InitCorS(Table.RoomIO io)
        {
            Table.CorS cs = Table.CorS.GetObject(io.CorSId);
            Debug.Assert(cs != null);
            labelCorSAddress.Content = cs.Address;
            labelCorSName.Content = cs.Name;
            labelCorSPassPort.Content = cs.IdentificationCard;
            labelCorSSex.Content =cs._Sex;
            labelCorSTel.Content = cs.Tel;
        }

        private void InitRoomIO(Table.RoomIO io)
        {
            labelOrderNO.Content = io.OrderNO;
            doubleUpDownRent.Value = io.Rent;
            doubleUpDownDeposits.Value = io.Deposits;
            datePickerStart.SelectedDate = io.OrderDateTimeStart;
            datePickerEnd.SelectedDate = io.OrderDateTimeStart.AddMonths((int)io.Month);
            integerUpDownPayCycle.Value = (int)io.PayCycle;
            textBoxNote.Text = io.Note;

            dataGrid1.ItemsSource = Table.RoomMoneyIO.GetList(io.OrderNO);
        }
    }
}
