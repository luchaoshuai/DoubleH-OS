﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;

namespace HouseS
{
    /// <summary>
    /// CorSWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CorSWindow : Window
    {
        public CorSWindow()
        {
            InitializeComponent();
        }

        Table.RoomIO selectedObject = null;
        public Table.RoomIO SelectedObject
        {
            get { return selectedObject; }
        }

        private void InitVar()
        {
            for (int i = 1; i < 13; i++)
                comboBoxZhouQi.Items.Add(i + " 个月");
        }
    }
}
