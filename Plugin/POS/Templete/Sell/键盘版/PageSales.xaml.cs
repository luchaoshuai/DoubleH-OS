﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Windows.Interop;
using System.Collections;
using DoubleH.Utility.Configuration;
using System.Collections.ObjectModel;

namespace POS.Templete.StoreTemplete
{
    /// <summary>
    /// StoreSales.xaml 的交互逻辑
    /// </summary>
    public partial class PageSale : Page, ISaleTemplete
    {
        public PageSale()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        public event dSaleToPayEvent Saled;
        public event dAddProductS AddProductS;

        private void InitVar()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as Control;
                if (c != null)
                {
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }

            if (!DoubleHConfig.AppConfig.ShowCashboxMoney)
            {
                labelCashboxMoney.Visibility = Visibility.Hidden;
                labelCashboxMoneyTitle.Visibility = Visibility.Hidden;
            }

            productSListInfo1.AbsQuantity = false;
            productSListInfo1.ShowBodyContexMenu = false;
            productSListInfo1.ShowAverage = false;
            productSListInfo1.EnableText = string.Empty;
            productSListInfo1.CanUserAddRows = false;
            productSListInfo1.CanSelectProduct = false;
            productSListInfo1.SelectionUnit = DataGridSelectionUnit.FullRow;
            productSListInfo1.ProductSType = ProductS.GetProductS.EnumProductS.可销售商品;

            textBoxInput.Focus();
        }

        private void InitEvent()
        {
            textBoxInput.KeyDown += (ss, ee) => PressEnter(ee.Key); 
            textBoxInput.PreviewKeyDown += (ss, ee) => TextBoxKeyPreviewDown(ee.Key); 
        }

        private void TextBoxKeyPreviewDown(Key key)
        {
            switch (key)
            {
                case Key.Delete:
                    productSListInfo1.RemoveSelectedItem();
                    break;

                case Key.Down:
                    if (productSListInfo1.SelectedIndex + 1 < productSListInfo1.Count)
                        productSListInfo1.SelectedIndex += 1;
                    break;

                case Key.Up:
                    if (productSListInfo1.SelectedIndex > 0)
                        productSListInfo1.SelectedIndex -= 1;
                    break;
            }
        }

        private void PressEnter(Key key)
        {
            if (key != Key.Enter)
                return;

            if (string.IsNullOrEmpty(textBoxInput.Text))
            {
                if (!productSListInfo1.HasItems)
                    return;

                if (Saled != null)
                    Saled();

                return;
            }

            if (textBoxInput.Text.StartsWith("++"))
            {
                ChangedQuantity();
                textBoxInput.Clear();
                return;
            }
            if (textBoxInput.Text.StartsWith("--"))
            {
                ChangedPrice();
                textBoxInput.Clear();
                return;
            }
            if (textBoxInput.Text.StartsWith(".."))
            {
                ChangeVipNO();
                textBoxInput.Clear();
                return;
            }
            if (textBoxInput.Text.StartsWith("//"))
                if (GetGuaDan())
                {
                    textBoxInput.Clear();
                    return;
                }

            Table.ProductS product = MainWindow.GetProductSBySearch(textBoxInput.Text.Trim());
            if (product == null)
                MessageWindow.Show("商品不存在");
            else
            {
                productSListInfo1.InsertItem(0, product);
                productSListInfo1.SelectedIndex = 0;

                if (AddProductS != null)
                    AddProductS(product);
            }
            if (productSListInfo1.Count > 0)
                productSListInfo1.SelectedIndex = 0;

            textBoxInput.Clear();
            textBoxInput.Focus();
        }

        private void ChangedQuantity()
        {
            if (productSListInfo1.SelectedItem == null)
                return;
            
            double d = 0; string str = string.Empty;
            if (!textBoxInput.Text.StartsWith("++"))
                return;

            if (!double.TryParse(textBoxInput.Text.Remove(0, 2), out d))
                return;

            if (d == 0)
                return;

            productSListInfo1.SelectedItem._TempQuantity -= Math.Abs(d);
        }

        private void ChangedPrice()
        {
            if (productSListInfo1.SelectedItem == null)
                return;

            if (Table.UserS.LoginUser.GetAuthority(Table.PosS.EnumAuthority.更改零售价) > Table.UserS.EnumAuthority.编辑)
            {
                MessageWindow.Show("权限不足");
                return;
            }

            double d = 0; string str = string.Empty;
            if (!textBoxInput.Text.StartsWith("--"))
                return;

            if (!double.TryParse(textBoxInput.Text.Remove(0, 2), out d))
                return;

            ((Table.ProductS)productSListInfo1.SelectedItem)._TempPrice = Math.Abs(d);
        }

        private void ChangeVipNO()
        {
            if (!textBoxInput.Text.StartsWith(".."))
                return;
            if (textBoxInput.Text == "..")
            {
                productSListInfo1.CorS = null;
                labelRemainIntegral.Content = null;
                labelVipNO.Content = null;
                return;
            }

            Table.CorS vip = Table.CorS.GetVipObject(textBoxInput.Text.Remove(0, 2));
            textBoxInput.Clear();
            if (vip == null || vip.Id == -1)
            {
                MessageWindow.Show("会员不存在");
                return;
            }
            if (vip.VipDateEnd < DateTime.Now)
            {
                MessageWindow.Show("会员已过期");
                return;
            }
            //如果此会员已经有挂单了，就直接调取出来
            ClassGuaDan gd = App.GuaDanList.FirstOrDefault<ClassGuaDan>(c => c.CorSId == vip.Id);
            if (gd != null)
                ReadGuaDan(gd);
            else
            {
                labelVipNO.Content = vip.VipNO;
                labelRemainIntegral.Content = vip.VipRemainIntegral;
                App.Order.CorSId = vip.Id;
                productSListInfo1.CorS = vip;
            }
        }

        private bool GetGuaDan()
        {
            string str = textBoxInput.Text.Remove(0, 2).Trim();
            if (string.IsNullOrEmpty(str))
                return false;

            int it = -1;
            if (int.TryParse(str, out it))
            {
                if (it < 1)
                    return false;

                if (it >App.GuaDanList.Count)
                    return false;

                ReadGuaDan(App.GuaDanList[it - 1]);
                return true;
            }
            else
                return false;
        }


        
        public void Init()
        {
            productSListInfo1.Init(DoubleHConfig.UserConfig.GetUserUIparams(DataTableText.Pos零售商品编辑));
            labelCashboxMoney.Content = DoubleHConfig.AppConfig.PosCashboxMoney;
        }

        public Int64 CorSId { get { return App.Order.CorSId; } }

        public string VipNO { get { return labelVipNO.Content.ToString(); } }

        public string RemainIntegral { get { return labelRemainIntegral.Content.ToString(); } }

        public void NewOrder(Table.PosOrderS.EnumFlag flag)
        {
            if (App.Order != null)
            {
                labelPreOrderNO.Content = App.Order.OrderNO;
                labelPreMoney.Content = App.Order.Money;
                labelVipNO.Content = "无";
                labelRemainIntegral.Content = string.Empty;
                labelCashboxMoney.Content = DoubleHConfig.AppConfig.PosCashboxMoney;
            }
            App.Order = new Table.PosOrderS(flag);
            productSListInfo1.ItemsSource = App.Order.ProductSList;
            productSListInfo1.CorS = null;
            if (flag == Table.PosOrderS.EnumFlag.POS退货单)
                productSListInfo1.AbsQuantity = true;
            else
                productSListInfo1.MinusQuantity = true;
        }

        public void GuaDan()
        {
            //商品都没有，有必要挂单吗？
            if (!productSListInfo1.HasItems)
                return ;

            App.GuaDanList.Add(new ClassGuaDan());
            NewOrder(Table.PosOrderS.EnumFlag.POS零售单);
            labelGuaDan.Content = App.GuaDanList.Count;
        }

        public void ReadGuaDan(ClassGuaDan gd)
        {
            App.Order = gd.ToOrder();
            productSListInfo1.ItemsSource = App.Order.ProductSList;
            labelVipNO.Content = gd.VipNO;
            labelRemainIntegral.Content = gd.RemainIntegral;
            App.GuaDanList.Remove(gd);
            labelGuaDan.Content = App.GuaDanList.Count;
        }
    }
}