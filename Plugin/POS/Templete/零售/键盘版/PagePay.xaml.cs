﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.IO;
using DoubleH.Utility.Configuration;

namespace POS.Templete.StoreTemplete
{
    /// <summary>
    /// PagePay.xaml 的交互逻辑
    /// </summary>
    public partial class PagePay : Page, IPayTemplete
    {
        public PagePay()
        {
            InitializeComponent();

            InitUI();
            InitEvent();
        }

        /// <summary>
        /// 因为 fastreport.net 在不显示打印框的情况下,会自动发送一次 Enter 输入
        /// 如果不这样做，下面的 Payed 事件就会触发两次了
        /// </summary>
        bool isPrinted = false;

        public event dPayToSaleEvent Payed;

        private void InitUI()
        {
            this.Background = App.BackgroundBrush;
            this.Foreground = App.ForegroundBrush;
            foreach (UIElement ui in mainGrid.Children)
            {
                Control c = ui as Control;
                if (c != null)
                {
                    c.Background = App.BackgroundBrush;
                    c.Foreground = App.ForegroundBrush;
                }
            }
        }

        public void InitEvent()
        {
            doubleUpDownPay.ValueChanged += (ss, ee) => ChangeMoney();
            this.Loaded += (ss, ee) => ThisOnLoad();
            doubleUpDownPay.KeyDown += (ss, ee) => DoubleUpDownPayKeyDown(ee);
        }

        private void DoubleUpDownPayKeyDown(KeyEventArgs ee)
        {
            switch (ee.Key)
            {
                case Key.Escape: PayCanel(); break;
                case Key.Enter: Pay(); break;
            }
        }

        private void ThisOnLoad()
        {
            isPrinted = false;

            if (App.Mode == POS.EnumMode.零售模式)
            {
                labelSum.Content =App.Order.Money;
                doubleUpDownPay.Value = App.Order.Money;
            }
            else
            {
                label1.Content = "应退款";
                label2.Content = "已退款";
                labelSum.Content = App.Order.Money;
                doubleUpDownPay.Value = App.Order.Money;
            }

            doubleUpDownPay.Focus();
            App.OpenLed();
        }

        private void ChangeMoney()
        {
            double d = doubleUpDownPay.Value.HasValue ? doubleUpDownPay.Value.Value : 0;
            d =Math.Round( d - App.Order.Money,Table.SysConfig.SysConfigParams.DecimalPlaces);
            labelBack.Content = (d > 0 ? d : 0);
        }

        public void PayCanel()
        {
            if (Payed != null)
                Payed(0, false);
        }

        int i = 0;
        public void Pay()
        {
            if (isPrinted)
                return;

            isPrinted = true;

            if (doubleUpDownPay.Value.HasValue)
            {
                if (doubleUpDownPay.Value.Value < App.Order.Money)
                {
                    MessageWindow.Show("注意", "收款金额低于账单金额.");
                    return;
                }

                if (Payed == null)
                    return;

                App.ShowLed(doubleUpDownPay.Value.ToString(), labelBack.Content.ToString());
                Payed(doubleUpDownPay.Value.Value, true);
            }
            else
                MessageWindow.Show("请输入收款金额");
        }
    }
}