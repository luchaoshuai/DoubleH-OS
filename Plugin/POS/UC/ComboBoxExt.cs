﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using DoubleH.Utility.Configuration;
using System.Xml.Linq;
using System.Xml;
using System.Windows;
using System.Windows.Markup;

namespace POS.UC
{
    public class ComboBoxExt :ComboBox
    {
        public ComboBoxExt()
        {
            InitVar();
            LoadTemplate();
            LoadData();
        }

        private void InitVar()
        {
            this.MinHeight = 48;
            this.MinWidth = 48;
            this.MaxHeight = 52;
            this.MaxWidth = 52;
        }

        private void LoadTemplate()
        {
            XNamespace ns = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";
            XElement xDataTemplate =
                new XElement(ns + "DataTemplate", new XAttribute("xmlns", "http://schemas.microsoft.com/winfx/2006/xaml/presentation"),
                    new XElement(ns + "StackPanel", new XAttribute("Orientation", "Horizontal"),
new XElement(ns + "Image", new XAttribute("Source", "{Binding _Img}"), new XAttribute("Stretch", "Fill"), new XAttribute("Width", 48), new XAttribute("Height", 48)),
new XElement(ns + "TextBlock", new XAttribute("Text", "{Binding  _Name}"))
));
         
            XmlReader xr = xDataTemplate.CreateReader();
            DataTemplate dataTemplate = XamlReader.Load(xr) as DataTemplate;
            this.ItemTemplate = dataTemplate;
        }

        private void LoadData()
        {
            List<TempClass> tc = new List<TempClass>();
            tc.Add(new TempClass() { _Name = "现金", _Img = "Pack://application:,,,/POS;component/Images/payMode_64.png" });
            tc.Add(new TempClass() { _Name = "积分", _Img = "Pack://application:,,,/POS;component/Images/vip_64.png" });
            tc.Add(new TempClass() { _Name = "银行卡", _Img = "Pack://application:,,,/POS;component/Images/card_64.png" });
           this.ItemsSource = tc;
           this.SelectedIndex = 0;
        }
    }

    public class TempClass
    {
        public string _Img { get; set; }
        public string _Name { get; set; }
    }
}