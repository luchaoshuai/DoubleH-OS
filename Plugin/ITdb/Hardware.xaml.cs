﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Diagnostics;

namespace ItDbS
{
    /// <summary>
    /// Hardware.xaml 的交互逻辑
    /// </summary>
    public partial class Hardware : Window
    {
        public Hardware()
        {
            InitializeComponent();
        }

        Table.ItDbS order = null;
        public void Init(Table.ItDbS obj)
        {
            this.order = obj ?? new Table.ItDbS() { Flag = Table.ItDbS.EnumFlag.硬件 };

            InitVar();
            InitOrder();
            InitEvent();
        }

        private void InitVar()
        {
            uCGroupS1.Init(Table.GroupS.EnumFlag.设备分类);
            uCUniqueS1.Init(Table.UniqueS.EnumFlag.品牌);

            checkComboBoxSoftware.ItemsSource = Table.ItDbS.GetList(Table.ItDbS.EnumFlag.软件, Table.ItDbS.EnumEnable.激活, Table.ItDbS.EnumEnable.维修);
            checkComboBoxSoftware.DisplayMemberPath = "Name";
            checkComboBoxSoftware.SelectedMemberPath = "Id";
            checkComboBoxSoftware.ValueMemberPath = "Id";

            checkListBoxSign.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.标记, Table.UniqueS.EnumEnable.启用);
            checkListBoxSign.DisplayMemberPath = "Name";
            checkListBoxSign.ValueMemberPath = "Id";

            comboBoxEnable.ItemsSource = Enum.GetNames(typeof(Table.ItDbS.EnumEnable));
            uCUserSelect1.Init(Table.UserS.EnumFlag.经办人);
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
            {
                order.GroupSId = uCGroupS1.SelectedObjectId;
                order.BrandId = uCUniqueS1.SelectedObjectId;
                order.Model = textBoxModel.Text;
                order.Name = textBoxName.Text;
                order.Standard = textBoxParams.Text;
                order.RelatedSoftware = checkComboBoxSoftware.SelectedValue;
                order.Code = textBoxCode.Text;

                order.IpV4 = textBoxIpv4.Text;
                order.IpV6 = textBoxIpv6.Text;
                order.Mac = textBoxMac.Text;
                order.Dns = textBoxDns.Text;
                order.Netmask = textBoxNetmask.Text;
                order.AdminPort = integerUpDownPort.Value.Value;
                order.Sign = checkListBoxSign.SelectedValue;

                order.PurchaseDate = datePicker1.SelectedDate;
                order.Quantity = integerUpDownBaoxiu.Value.Value;
                order.PurchaseOrderNO = textBoxRelatedOrderNO.Text;
                order.PurchasePrice = doubleUpDownMoney.Value.Value;
       
                order.Enable = (Table.ItDbS.EnumEnable)Enum.Parse(typeof(Table.ItDbS.EnumEnable), comboBoxEnable.Text);
                order.OperatorId = uCUserSelect1.SelectedObjectId;

                order.Note = textBoxNote.Text;
                order.UploadFile = uCImage1.UploadFileName;

                Table.DataTableS.EnumDatabaseStatus result;
                if (order.Id == -1)
                    result = order.Insert();
                else
                    result = order.Update();

                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                {
                    uCImage1.UpdateImage(Table.ItDbS.tableName);
                    this.Close();
                }
                else
                    MessageWindow.Show(result.ToString());
            };
        }

        private void InitOrder()
        {
            Debug.Assert(order != null);

            uCGroupS1.SelectedObjectId = order.GroupSId;
            uCUniqueS1.SelectedObjectId = order.BrandId;
            textBoxModel.Text = order.Model;
            textBoxName.Text = order.Name;
            textBoxCode.Text = order.Code;
            textBoxParams.Text = order.Standard;
            checkComboBoxSoftware.SelectedValue = order.RelatedSoftware;

            textBoxIpv4.Text = order.IpV4;
            textBoxIpv6.Text = order.IpV6;
            textBoxMac.Text = order.Mac;
            textBoxDns.Text = order.Dns;
            textBoxNetmask.Text = order.Netmask;
            integerUpDownPort.Value = (int)order.AdminPort;
            checkListBoxSign.SelectedValue = order.Sign;
            doubleUpDownMoney.Value = order.PurchasePrice;

            datePicker1.SelectedDate = order.PurchaseDate;
            integerUpDownBaoxiu.Value = (int)order.Quantity;
            textBoxRelatedOrderNO.Text = order.PurchaseOrderNO;
         
            comboBoxEnable.Text = order.Enable.ToString();
            uCUserSelect1.SelectedObjectId = order.OperatorId;

            uCImage1.ImageFile = order._ImageFile;

            textBoxNote.Text = order.Note;
        }
    }
}
