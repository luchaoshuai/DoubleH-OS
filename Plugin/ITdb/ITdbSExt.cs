﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;


namespace ItDbS
{
   public class ItDbSExt : Plugin
    {
        private IPluginHost m_host = null;
        Hardware hardware = null;
        Software software = null;
        SearchWindow search = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            m_host = host;
            switch (TableText)
            {
                case DataTableText.硬件:
                    hardware = new Hardware();
                    hardware.Init(DataTable as Table.ItDbS);
                    hardware.Owner = host.WorkAreaWindow;
                    hardware.Show();
                    break;

                case DataTableText.软件:
                    software = new Software();
                    software.Init(DataTable as Table.ItDbS);
                    software.Owner = host.WorkAreaWindow;
                    software.Show();
                    break;

                default:
                    search = new SearchWindow();
                    search.Owner = host.WorkAreaWindow;
                    search.Show();
                    break;
            }

            return true;
        }

        public override void Terminate()
        {
            if (hardware != null)
                hardware.Close();
            if (software != null)
                software.Close();
            if (search != null)
                search.Close();

            hardware = null;
            software = null;
            search = null;
        }
        /// <summary>
        /// 从销售单、项目管理单中导入数据
        /// </summary>
        public void ImportByOrder(FCNS.Data.Interface.IDataTable table)
        {
            ImportOrder io = new ImportOrder();
            io.Init(table);
            io.ShowDialog();
        }
    }
}
