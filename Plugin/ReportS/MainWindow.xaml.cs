﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH.Utility.Configuration;
using System.Data;
using FCNS.Data;
using System.Diagnostics;

namespace ReportS
{
    public class IdAndName
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        Table.SysConfig sysConfig = null;
        public static List<IdAndName> allProductS = new List<IdAndName>();
        public static List<IdAndName> allCorS = new List<IdAndName>();
        public static List<IdAndName> allStoreS = new List<IdAndName>();
        //public static List<IdAndName> allGroupS = new List<IdAndName>();
        List<Window> allOpenedWindow = new List<Window>();

        public MainWindow()
        {
            InitializeComponent();

            InitEvent();
        }

        private void InitEvent()
        {
            this.Closing += (ss, ee) => ThisClose();
        }

        private void ThisClose()
        {
            if (sysConfig != null)
                ConfigSerializer.SaveConfig(DoubleHConfig.UserConfig, Table.UserS.LoginUser.Id);

            allOpenedWindow.ForEach(f => { f.Closing -= window_Closing; f.Close(); });
        }

        public void Init(string[] Args)
        {
            switch (Args.Length)
            {
                case 0:
                    if (!NormalUtility.ShowLoginWindowSuccess(Table.UserS.EnumFlag.操作员))
                    {
                        Application.Current.Shutdown();
                        return;
                    }
                    break;

                default: UserLogin(Args[0], Args.Length > 1 ? Args[1] : string.Empty); break;
            }

            if (Table.UserS.LoginUser == null)
                return;

            sysConfig = Table.SysConfig.SysConfigParams;

            InitMenu();
            InitData();
        }
        //软件启动后加载需要筛选的数据,更加高效
        private void InitData()
        {
            foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows("select Id,Name from StoreS"))
                allStoreS.Add(new IdAndName() { Id = (Int64)row[0], Name = row[1] as string });
            foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows("select Id,Name from CorS "))
                allCorS.Add(new IdAndName() { Id = (Int64)row["Id"], Name = row["Name"] as string });
            //foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows("select Id,Name from GroupS "))
            //    allGroupS.Add(new IdAndName() { Id = (Int64)row["Id"], Name = row["Name"] as string });
            //foreach (DataRow row in FCNS.Data.SQLdata.GetDataRows("select Id,Name from ProductS where Enable=" + (int)Table.ProductS.EnumEnable.启用))
            //    allProductS.Add(new IdAndName() { Id = (Int64)row["Id"], Name = row["Name"] as string });
        }

        private void UserLogin(string name, string pwd)
        {
            AppConfig appConfig = (AppConfig)ConfigSerializer.LoadConfig(typeof(AppConfig), DbDefine.appConfigFile);
            DataConfig dc = appConfig.DataConfigItems.FirstOrDefault(f => { return f.Flag == appConfig.DataFlag; });
            Debug.Assert(dc != null);

            FCNS.Data.SQLdata.SqlConfig = new FCNS.Data.SQLdata.SqlString((FCNS.Data.DataType)Enum.Parse(typeof(FCNS.Data.DataType), dc.DataType), dc.DataAddress, dc.DataName)
            {
                Password = dc.DataPassword,
                Port = dc.Port,
                User = dc.DataUser
            };
            this.Title = FCNS.Data.DbDefine.SystemName;
            Table.UserS.LoginUser = Table.UserS.GetObject(name, pwd);
            DoubleHConfig.UserConfig = ConfigSerializer.LoadUserConfig(Table.UserS.LoginUser.Id);
        }

        private void InitMenu()
        {
            AddCorS();

            if (sysConfig.UsePurchaseOrderS)
                AddPurchaseOrderS();
            if (sysConfig.UseStoreS)
                AddStoreS();
            if (sysConfig.UseSalesOrderS)
                AddSalesOrderS();
            if (sysConfig.UsePos)
                AddPos();
            if (sysConfig.UseAfterSaleServiceS)
                AddAfterSaleServiceS();
            if (sysConfig.UsePayS)
                AddMoneyS();
            if (sysConfig.UseCarS)
                AddCarS();
            if (sysConfig.UseItDbS)
                AddItDbS();

            AddBoss();
        }


        private void MenuClick(MenuItem menuItem)
        {
            Window window = AppDomain.CurrentDomain.CreateInstanceAndUnwrap("ReportS", "ReportS." + menuItem.Header.ToString()) as Window;
            Debug.Assert(window != null);

            window.Icon = this.Icon;
            window.Title = menuItem.Header.ToString();
            window.ShowInTaskbar = false;
            window.Owner = this;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Width = 900;
            window.Height = 600;
            window.MinWidth = 900;
            window.MinHeight = 600;
            window.Show();
            window.Closing += new System.ComponentModel.CancelEventHandler(window_Closing);
            allOpenedWindow.Add(window);
        }

        private void window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            allOpenedWindow.Remove(sender as Window);
        }

        private void AddCorS()
        {
            MenuItem all = new MenuItem();
            all.Header = "客商报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.维修记录表);
        }

        private void AddItDbS()
        {
            MenuItem all = new MenuItem();
            all.Header = "IT设备报表";
            menu1.Items.Add(all);

            //AddMenuItem(all, DataTableText.会员积分排行表);
            //AddMenuItem(all, DataTableText.积分规则表);
            //AddMenuItem(all, DataTableText.积分消费明细表);
        }

        private void AddPurchaseOrderS()
        {
            MenuItem all = new MenuItem();
            all.Header = "采购报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.采购统计表);
            AddMenuItem(all, DataTableText.采购明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.采购价格变动情况表);
        }

        private void AddStoreS()
        {
            MenuItem all = new MenuItem();
            all.Header = "仓库报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.商品出入库明细表);
            AddMenuItem(all, DataTableText.库存单价变动情况表);
            AddMenuItem(all, DataTableText.商品实时库存表);
            AddMenuItem(all, DataTableText.库存成本);
        }

        private void AddSalesOrderS()
        {
            MenuItem all = new MenuItem();
            all.Header = "销售报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.销售统计表);
            AddMenuItem(all, DataTableText.销售明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.销售价格变动情况表);
        }

        private void AddPos()
        {
            MenuItem all = new MenuItem();
            all.Header = "POS报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.交班明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.零售统计表);
            AddMenuItem(all, DataTableText.零售明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.POS记账明细表);
            AddMenuItem(all, DataTableText.商品赠送明细表);
        }

        private void AddAfterSaleServiceS()
        {
            MenuItem all = new MenuItem();
            all.Header = "售后维护";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.售后统计表);
            AddMenuItem(all, DataTableText.售后区域补贴统计表);
            AddMenuItem(all, DataTableText.商品质保期查询);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.故障库);
        }

        private void AddMoneyS()
        {
            MenuItem all = new MenuItem();
            all.Header = "财务报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.客商余额表);
            AddMenuItem(all, DataTableText.应收支明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.账户余额表);
            AddMenuItem(all, DataTableText.收支明细表);
            AddMenuItem(all, DataTableText.发票明细表);
            all.Items.Add(new Separator());
            AddMenuItem(all, DataTableText.损益表);
            if (sysConfig.UseWuLiuS)
            {
                all.Items.Add(new Separator());
                AddMenuItem(all, DataTableText.物流明细表);
            }

        }

        private void AddCarS()
        {
            MenuItem all = new MenuItem();
            all.Header = "车辆报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.车辆成本);
        }

        private void AddBoss()
        {
            MenuItem all = new MenuItem();
            all.Header = "Boss报表";
            menu1.Items.Add(all);

            AddMenuItem(all, DataTableText.商品毛利表);
            AddMenuItem(all, DataTableText.营业额);

            //foreach (MenuItem mi in all.Items)
            //    mi.IsEnabled = false;
        }

        private void AddMenuItem(MenuItem father, DataTableText newItemHeader)
        {
            if (Table.UserS.LoginUser.GetAuthority(newItemHeader) < Table.UserS.EnumAuthority.查看)
                return;

            MenuItem item = new MenuItem() { Header = newItemHeader };
            item.Click += (ss, ee) => MenuClick(item);
            father.Items.Add(item);
        }
    }
}