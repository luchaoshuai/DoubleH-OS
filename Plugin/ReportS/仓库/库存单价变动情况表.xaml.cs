﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 库存单价变动情况表.xaml 的交互逻辑
    /// </summary>
    public partial class 库存单价变动情况表 : Window
    {
        public 库存单价变动情况表()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        public void InitVar()
        { 
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId();
            buttonOK.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            List<TempClass> temp = new List<TempClass>();
            temp.AddRange(GetProductSIO());
            temp.AddRange(GetStoreSTiaoJia());

            uCdataGrid1.Init(DataTableText.库存单价变动情况表,temp, false);
        }

        private List<TempClass> GetProductSIO()
        {
            var psio = from f in Table.ProductSIO.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, null,
                      NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue), productS, Table.ProductSIO.EnumEnable.入账)
                       orderby f.ProductSId ascending, f.Id ascending
                       select f;

            List<TempClass> temp = new List<TempClass>();
            for (int i = 0; i < psio.Count(); i++)
            {
                Table.ProductSIO p = psio.ElementAt(i);
                TempClass tc = temp.FirstOrDefault(f => f.AveragePrice == p.AveragePrice && f.OrderDateTime == p.OrderDateTime
                       && f.OrderNO == p.OrderNO && p.ProductSId == f.ProductSId && p.StoreSId == f.StoreSId);

                if (tc != null)
                    continue;

                tc = new TempClass();
                tc.AveragePrice = p.AveragePrice;
                tc.OrderDateTime = p.OrderDateTime;
                tc.OrderNO = p.OrderNO;
                tc.ProductSId = p.ProductSId;
                tc.StoreSId = p.StoreSId;
                tc.ProductSName = p._ProductSName;
                tc.StoreSName = p._StoreSName;
                temp.Add(tc);
            }
            return temp;
        }

        private List<TempClass> GetStoreSTiaoJia()
        {
            List<TempClass> temp = new List<TempClass>();
            foreach (Table.AdjustAveragePriceS aap in Table.AdjustAveragePriceS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,
                           NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue)))
            {
                foreach (Table.ProductS p in aap.ProductSList)
                {
                    TempClass tc = new TempClass();
                    tc.StoreSName = aap._StoreSName;
                    tc.ProductSName = p.Name;
                    tc.StoreSId = aap.StoreSId;
                    tc.ProductSId = p.Id;
                    tc.OrderNO = aap.OrderNO;
                    tc.AveragePrice = p._TempPrice;//注意这里用的是TempPrice为了和库存调价单中的一致性.
                    tc.OrderDateTime = aap.OrderDateTime;
                    
                    temp.Add(tc);
                }
            }


            return temp;
        }

        private class TempClass
        {
            public string StoreSName { get; set; }
            public string ProductSName { get; set; }
            public Int64 StoreSId { get; set; }
            public Int64 ProductSId { get; set; }
            public string OrderNO { get; set; }
            public double AveragePrice { get; set; }
            public DateTime OrderDateTime { get; set; }
        }
    }
}
