﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using System.Collections;
using Table = FCNS.Data.Table;

namespace ReportS
{
    /// <summary>
    /// 商品实时库存表.xaml 的交互逻辑
    /// </summary>
    public partial class 商品实时库存表 : Window
    {
        public 商品实时库存表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        private void InitVar()
        {
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId();
            buttonOK.Click += (ss, ee) => Search();
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(checkComboBoxStoreS.SelectedValue))
            {
                MessageWindow.Show("请选择仓库");
                return;
            }

            uCdataGrid1.Init(DataTableText.商品实时库存表, Table.ProductSInStoreS.GetList(NormalUtility.FormatStringToInt64(checkComboBoxStoreS.SelectedValue), productS).Where(f => (f.Quantity != 0)), false,
                new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "数量", ValueName = "Quantity" });
        }
    }
}