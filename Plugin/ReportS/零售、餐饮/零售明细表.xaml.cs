﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table=FCNS.Data.Table;
using DoubleH.Utility;

namespace ReportS
{
    /// <summary>
    /// 零售明细表.xaml 的交互逻辑
    /// </summary>
    public partial class 零售明细表 : Window
    {
        public 零售明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxPosNO.ItemsSource = Table.PosS.GetList(Table.PosS.EnumEnable.启用, Table.PosS.EnumEnable.停用);
            checkComboBoxPosNO.DisplayMemberPath = "PosNO";
            checkComboBoxPosNO.ValueMemberPath = "PosNO";

            checkComboBoxStore.ItemsSource = MainWindow.allStoreS;
            checkComboBoxStore.DisplayMemberPath = "Name";
            checkComboBoxStore.ValueMemberPath = "Id";

            checkComboBoxUser.ItemsSource = Table.UserS.GetList(null, Table.UserS.EnumFlag.POS操作员);
            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.ValueMemberPath = "Id";

            checkComboBoxType.ItemsSource = Enum.GetNames(typeof(Table.PosOrderS.EnumFlag));
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) =>
                {
                    string[] posNO = null;
                    if (!string.IsNullOrEmpty(checkComboBoxPosNO.SelectedValue))
                        posNO = checkComboBoxPosNO.SelectedValue.Split(',');

                    List<Table.PosOrderS.EnumFlag> flag = new List<Table.PosOrderS.EnumFlag>();
                    if (!string.IsNullOrEmpty(checkComboBoxType.SelectedValue))
                        foreach (string str in checkComboBoxType.SelectedValue.Split(','))
                            flag.Add((Table.PosOrderS.EnumFlag)Enum.Parse(typeof(Table.PosOrderS.EnumFlag), str));

                    uCdataGrid1.Init(DataTableText.零售明细表,Table.PosOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,
                        NormalUtility.StringToInt64(checkComboBoxStore.SelectedValue),
                        posNO,
                        NormalUtility.StringToInt64(checkComboBoxUser.SelectedValue),
                        flag.ToArray()), false,
                 new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "金额", ValueName = "Money" });
                };
        }
    }
}