﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 客户记账明细表.xaml 的交互逻辑
    /// </summary>
    public partial class POS记账明细表 : Window
    {
        public POS记账明细表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;

            checkComboBoxUser.DisplayMemberPath = "Name";
            checkComboBoxUser.ValueMemberPath = "Id";
            checkComboBoxUser.ItemsSource = Table.UserS.GetList(Table.UserS.EnumEnable.启用, Table.UserS.EnumFlag.POS操作员);
        }

        private void InitEvent()
        {
                  buttonOK.Click += (ss, ee) =>
                  {
                      uCdataGrid1.Init(DataTableText.POS记账明细表, Table.PosOrderS.GetListForNoPay(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,
                          NormalUtility.FormatStringToInt64(checkComboBoxCorS.SelectedValue), NormalUtility.FormatStringToInt64(checkComboBoxUser.SelectedValue)), false,
                   new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueTitle = "金额", ValueName = "Money" });
                  };
        }
    }
}
