﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 采购价格变动情况表.xaml 的交互逻辑
    /// </summary>
    public partial class 采购价格变动情况表 : Window
    {
        public 采购价格变动情况表()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

       Int64[] productS = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId(ProductS.GetProductS.EnumProductS.可采购商品);
            buttonSearch.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            List<int> store = new List<int>();
            List<int> cors = new List<int>();

            if (!string.IsNullOrEmpty(checkComboBoxStoreS.Text))
                foreach (string str in checkComboBoxStoreS.SelectedValue.Split(','))
                    store.Add(int.Parse(str));

            if (!string.IsNullOrEmpty(checkComboBoxCorS.Text))
                foreach (string str in checkComboBoxCorS.SelectedValue.Split(','))
                    cors.Add(int.Parse(str));

            List<TempClass> items = new List<TempClass>();
            foreach (Table.PurchaseOrderS pos in Table.PurchaseOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, new int[] { 0 }, new int[] { 2 }, store.ToArray(), cors.ToArray()))
            {
                foreach (Table.ProductS pr in pos.ProductSList)
                {
                    TempClass cl = new TempClass();
                    cl._ProductSId=pr.Id;
                    cl._CorSName = pos._CorSName;
                    cl._Money = pr._TempPrice;
                    cl._OrderDateTime = pos.OrderDateTime;
                    cl._ProductSName = pr.Name;
                    cl._OperatorerSName=pos._OperatorerSName;
                    items.Add(cl);
                }
            }

            if (productS != null)
                items=items.FindAll(f => productS.Contains(f._ProductSId));

            List<TempClass> result = new List<TempClass>();
            if (items.Count > 0)
            {
                //排序
                items.Sort((f1, f2) =>
                    {
                        if (f1.Id < f2.Id)
                            return 1;
                        else if (f1.Id == f2.Id)
                            return 0;
                        else
                            return -1;
                    });
                //移除相同项
                result.Add(items[0]);
                int all = items.Count - 1;
                for (int i = 0; i < all; i++)
                {
                    TempClass t1 = items[i];
                    TempClass t2 = items[i + 1];
                    if (t1.Id == t2.Id && t1._Money == t2._Money)
                        continue;

                    result.Add(t2);
                }
            }
            uCdataGrid1.Init(DataTableText.采购价格变动情况表,result ,false);
        }



       private class TempClass
        {
            public Int64 Id { get; set; }
            public Int64 _ProductSId { get; set; }
            public string _ProductSName { get; set; }
            public double _Money { get; set; }
            public string _CorSName { get; set; }
            public DateTime _OrderDateTime { get; set; }
            public string _OperatorerSName { get; set; }
        }
    }
}
