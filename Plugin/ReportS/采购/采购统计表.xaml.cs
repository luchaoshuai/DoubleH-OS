﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;

namespace ReportS
{
    /// <summary>
    /// CgTongJi.xaml 的交互逻辑
    /// </summary>
    public partial class 采购统计表 : Window
    {
        public 采购统计表()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        DataGridColumn columnStore = null;
        DataGridColumn columnCorS = null;
        DataGridColumn columnFlag = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            checkComboBoxFlag.Items.Add(Table.PurchaseOrderS.EnumFlag.采购订单);
            checkComboBoxFlag.Items.Add(Table.PurchaseOrderS.EnumFlag.采购退货单);
            checkComboBoxFlag.Items.Add(Table.PurchaseOrderS.EnumFlag.赠品入库单);

            checkComboBoxEnable.ItemsSource = Enum.GetNames(typeof(Table.PurchaseOrderS.EnumEnable));

            uCdataGrid1.Init(DataTableText.采购统计表, null, new UC.CharProperty
            {
                Series = new System.Windows.Controls.DataVisualization.Charting.ColumnSeries(),
                ValueTitle = "金额",
                ValueName = "Money",
            });
            columnCorS = uCdataGrid1.dataGridExt1.GetColumn("CorSName");
            columnStore = uCdataGrid1.dataGridExt1.GetColumn("StoreSName");
            columnFlag = uCdataGrid1.dataGridExt1.GetColumn("Flag");
            Debug.Assert(columnFlag != null);
            Debug.Assert(columnStore != null);
            Debug.Assert(columnCorS != null);
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            List<TempFunction> obj = new List<TempFunction>();
            StringBuilder sb = new StringBuilder("select ss.Flag,ss.Money,ss.CorSId,ss.StoreSId,CorS.Name,StoreS.Name as StoreSName");
            sb.Append(" from PurchaseOrderS ss INNER JOIN  CorS ON ss.CorSId = CorS.Id INNER JOIN StoreS ON ss.StoreSId = StoreS.Id");
            sb.Append(" where cast(ss.OrderDateTime as varchar)>='" + uCDateTime1.StartDateTime.ToString(FCNS.Data.DbDefine.dateTimeFormat) + "'");
            sb.Append(" and cast(ss.OrderDateTime as varchar)<='" + uCDateTime1.EndDateTime.ToString(FCNS.Data.DbDefine.dateTimeFormat) + "'");

            if (!string.IsNullOrEmpty(checkComboBoxStoreS.Text))
            {
                sb.Append(" and (");
                foreach (string str in checkComboBoxStoreS.SelectedValue.Split(','))
                    sb.Append("ss.StoreSId=" + str + " or ");

                sb.Remove(sb.Length - 3, 3);
                sb.Append(")");
            }
            if (!string.IsNullOrEmpty(checkComboBoxFlag.Text))
            {
                sb.Append(" and (");
                foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                    sb.Append("ss.Flag=" + (int)Enum.Parse(typeof(Table.PurchaseOrderS.EnumFlag), str) + " or ");

                sb.Remove(sb.Length - 3, 3);
                sb.Append(")");
            }

            if (!string.IsNullOrEmpty(checkComboBoxEnable.Text))
            {
                sb.Append(" and (");
                foreach (string str in checkComboBoxEnable.SelectedValue.Split(','))
                    sb.Append("ss.Enable=" + (int)Enum.Parse(typeof(Table.PurchaseOrderS.EnumEnable), str) + " or ");

                sb.Remove(sb.Length - 3, 3);
                sb.Append(")");
            }

            foreach (DataRow row in SQLdata.GetDataRows(sb.ToString()))
            {
                if (row == null)
                    continue;

                obj.Add(new TempFunction()
                {
                    Flag = ((Table.PurchaseOrderS.EnumFlag)(Int64)row["Flag"]).ToString(),
                    CorSId = (Int64)row["CorSId"],
                    CorSName = row["Name"] as string,
                    StoreSId = (Int64)row["StoreSId"],
                    StoreSName = row["StoreSName"] as string,
                    Money = (double)row["Money"]
                });
            }

            if (radioButtonCorS.IsChecked.Value)
                FilterByCorS(obj);
            if (radioButtonStoreS.IsChecked.Value)
                FilterByStoreS(obj);
        }

        private void FilterByStoreS(List<TempFunction> obj)
        {
            columnCorS.Visibility = Visibility.Hidden;
            columnStore.Visibility = Visibility.Visible;
            List<TempFunction> ltf = new List<TempFunction>();

            if (checkBoxFlag.IsChecked.Value)
            {
                columnFlag.Visibility = Visibility.Visible;
                foreach (string str in Enum.GetNames(typeof(Table.PurchaseOrderS.EnumFlag)))//订单类型
                {
                    var vr = from f in obj where f.Flag == str select f;
                    if (vr.Count() == 0)
                        continue;

                    foreach (IdAndName ian in MainWindow.allStoreS)
                    {
                        var v2 = from f in vr where f.StoreSId == ian.Id select f;
                        int count = v2.Count();
                        if (count == 0)
                            continue;

                        TempFunction tf = new TempFunction();
                        tf.StoreSName = ian.Name;
                        tf.Flag = str;
                        tf.Quantity = count;
                        tf.Money = v2.Sum(f => { return f.Money; });
                        ltf.Add(tf);
                    }
                }
            }
            else
            {
                columnFlag.Visibility = Visibility.Hidden;
                foreach (IdAndName ian in MainWindow.allStoreS)
                {
                    var v2 = from f in obj where f.StoreSId == ian.Id select f;
                    int count = v2.Count();
                    if (count == 0)
                        continue;

                    TempFunction tf = new TempFunction();
                    tf.StoreSName = ian.Name;
                    tf.Quantity = count;
                    tf.Money = v2.Sum(f => { return f.Money; });
                    ltf.Add(tf);
                }
            }

            uCdataGrid1.Init(DataTableText.采购统计表,ltf);
        }

        private void FilterByCorS(List<TempFunction> obj)
        {
            columnStore.Visibility = Visibility.Hidden;
            columnCorS.Visibility = Visibility.Visible;
            List<TempFunction> ltf = new List<TempFunction>();

            if (checkBoxFlag.IsChecked.Value)
            {
                columnFlag.Visibility = Visibility.Visible;
                foreach (string str in Enum.GetNames(typeof(Table.PurchaseOrderS.EnumFlag)))//订单类型
                {
                    var vr = from f in obj where f.Flag == str select f;
                    if (vr.Count() == 0)
                        continue;

                    foreach (IdAndName ian in MainWindow.allCorS)
                    {
                        var v2 = from f in vr where f.CorSId == ian.Id select f;
                        int count = v2.Count();
                        if (count == 0)
                            continue;

                        TempFunction tf = new TempFunction();
                        tf.CorSName = ian.Name;
                        tf.Flag = str;
                        tf.Quantity = count;
                        tf.Money = v2.Sum(f => { return f.Money; });
                        ltf.Add(tf);
                    }
                }
            }
            else
            {
                columnFlag.Visibility = Visibility.Hidden;
                foreach (IdAndName ian in MainWindow.allCorS)
                {
                    var v2 = from f in obj where f.CorSId == ian.Id select f;
                    int count = v2.Count();
                    if (count == 0)
                        continue;

                    TempFunction tf = new TempFunction();
                    tf.CorSName = ian.Name;
                    tf.Quantity = count;
                    tf.Money = v2.Sum(f => { return f.Money; });
                    ltf.Add(tf);
                }
            }

            uCdataGrid1.Init(DataTableText.采购统计表,ltf);
        }


        private class TempFunction
        {
            public string Flag { get; set; }
            public Int64 CorSId { get; set; }
            public string CorSName { get; set; }
            public Int64 StoreSId { get; set; }
            public string StoreSName { get; set; }
            public double Money { get; set; }
            public int Quantity { get; set; }
        }
    }
}
