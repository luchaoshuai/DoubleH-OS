﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using System.Data;
using FCNS.Data;
using System.Diagnostics;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 商品质保期查询.xaml 的交互逻辑
    /// </summary>
    public partial class 商品质保期查询 : Window
    {
        public 商品质保期查询()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        Int64[] productS = null;
        public void InitVar()
        {

            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;
        }

        private void InitEvent()
        {
            buttonProductS.Click += (ss, ee) => productS = MainWindow.GetProductSId(ProductS.GetProductS.EnumProductS.可销售商品);
            buttonSearch.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
            var vr = Table.SalesOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime,
                new Table.SalesOrderS.EnumFlag[] { Table.SalesOrderS.EnumFlag.销售订单, Table.SalesOrderS.EnumFlag.赠品出库单 },
              new Table.SalesOrderS.EnumEnable[]{ Table.SalesOrderS.EnumEnable.入账}).Where(f=>f.BaoXiu>0);

            List<TempClass> result=new List<TempClass>();
            foreach (Table.SalesOrderS sos in vr)
            {
                int day=(DateTime.Now.Date - sos.OrderDateTime.AddDays(sos.BaoXiu).Date).Days;
                foreach (Table.ProductS ps in sos.ProductSList)
                {
                    TempClass tc = new TempClass();
                    tc._ProductSName = ps.Name;
                    tc._CorSName = sos._CorSName;
                    tc._CorSTel = sos._CorSTel;
                    tc._OrderDateTime = sos.OrderDateTime;
                    tc._OrderNO = sos.OrderNO;
                    tc._OperatorerSName = sos._OperatorerSName;
                    tc._UserSName = sos._UserSName;
                    tc._Quantity = ps._TempQuantity;
                    tc._TempPrice = ps._TempPrice;
                    tc._BaoXiu = sos.BaoXiu;
                    if (day < 0)
                    {
                        tc._BaoXiuBe = -day;
                        tc._BaoXiuOver = 0;
                    }
                    else
                    {
                        tc._BaoXiuBe = 0;
                        tc._BaoXiuOver =day;

                    }
                    result.Add(tc);
                }
            }
            uCdataGrid1.Init(DataTableText.商品质保期查询, result, false);
                 //new UC.CharProperty() { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueName = "_Quantity", ValueTitle = "数量" });
        }



        private class TempClass
        {
            public string _ProductSName { get; set; }
            public string _CorSName { get; set; }
            public string _CorSTel { get; set; }
            public DateTime _OrderDateTime { get; set; }
            public string _OrderNO { get; set; }
            public string _OperatorerSName { get; set; }
            public string _UserSName { get; set; }
            public double _Quantity { get; set; }
            public double _TempPrice { get; set; }
            public double _BaoXiu { get; set; }
            public double _BaoXiuBe { get; set; }
            public double _BaoXiuOver { get; set; }
        }
    }
}
