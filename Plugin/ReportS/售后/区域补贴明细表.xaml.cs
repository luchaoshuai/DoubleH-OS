﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using System.Collections;

namespace ReportS
{
    /// <summary>
    /// 售后区域补贴统计表.xaml 的交互逻辑
    /// </summary>
    public partial class 售后区域补贴统计表 : Window
    {
        public 售后区域补贴统计表()
        {
            InitializeComponent();

            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            checkComboBoxArea.DisplayMemberPath = "Name";
            checkComboBoxArea.ValueMemberPath = "Id";
            checkComboBoxArea.ItemsSource = Table.UniqueS.GetList(Table.UniqueS.EnumFlag.区域);

            checkComboBoxCorS.DisplayMemberPath = "Name";
            checkComboBoxCorS.ValueMemberPath = "Id";
            checkComboBoxCorS.ItemsSource = MainWindow.allCorS;
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => FillData();
        }

        private void FillData()
        {
           var all= Table.AfterSaleServiceS.GetListHaveAreaMoney(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, NormalUtility.FormatStringToInt64(checkComboBoxArea.SelectedValue),NormalUtility.FormatStringToInt64(checkComboBoxCorS.SelectedValue));
           List<TempClass> result = new List<TempClass>();
           List<object> name = new List<object>();
            if (radioButtonArea.IsChecked.Value)
           {
               foreach (Table.AfterSaleServiceS ass in all)
                   if (!name.Contains(ass.CorSId))
                       name.Add(ass.CorSId);

               foreach (object obj in name)
               {
                   TempClass tc = new TempClass();
                   tc._Name = Table.CorS.GetObject((Int64)obj)._AreaName;
                   tc._Quantity = all.Where(f => f.CorSId == (Int64)obj).Count();
                   tc._Money = all.Sum(f => f.AreaMoney);
                   result.Add(tc);
               }
           }
            if (radioButtonUser.IsChecked.Value)
            {
                foreach (Table.AfterSaleServiceS ass in all)
                    if (!name.Contains(ass._ServiceUserSName))
                        name.Add(ass._ServiceUserSName);

                foreach (object obj in name)
                {
                    TempClass tc = new TempClass();
                    tc._Name = obj.ToString();
                    tc._Quantity = all.Where(f => f._ServiceUserSName == obj.ToString()).Count();
                    tc._Money = all.Sum(f => f.AreaMoney);
                    result.Add(tc);
                }
            }
            if (radioButtonCorS.IsChecked.Value)
            {
                foreach (Table.AfterSaleServiceS ass in all)
                    if (!name.Contains(ass._CorSName))
                        name.Add(ass._CorSName);

                foreach (object obj in name)
                {
                    TempClass tc = new TempClass();
                    tc._Name = obj.ToString();
                    tc._Quantity = all.Where(f => f._CorSName == obj.ToString()).Count();
                    tc._Money = all.Sum(f => f.AreaMoney);
                    result.Add(tc);
                }
            }

            uCdataGrid1.Init(DataTableText.售后区域补贴统计表, result, 
                new UC.CharProperty() { Series = new System.Windows.Controls.DataVisualization.Charting.LineSeries(), ValueName = "_Money", ValueTitle = "金额" });
        }

        public class TempClass
        {
            public string _Name { get; set; }

            int quantity = 0;
            public int _Quantity
            {
                get { return quantity; }
                set { quantity = value; }
            }

            double money = 0;
            public double _Money
            {
                get { return money; }
                set { money = value; }
            }
        }
    }
}
