﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using FCNS.Data;
using System.Data;
using System.Diagnostics;

namespace ReportS
{
    /// <summary>
    /// PfTongJi.xaml 的交互逻辑
    /// </summary>
    public partial class 销售统计表 : Window
    {
        public 销售统计表()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                return;

            InitVar();
            InitEvent();
        }

        DataGridColumn columnStore = null;
        DataGridColumn columnCorS = null;
        DataGridColumn columnFlag = null;
        public void InitVar()
        {
            checkComboBoxStoreS.DisplayMemberPath = "Name";
            checkComboBoxStoreS.ValueMemberPath = "Id";
            checkComboBoxStoreS.ItemsSource = MainWindow.allStoreS;

            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.销售订单);
            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.销售退货单);
            checkComboBoxFlag.Items.Add(Table.SalesOrderS.EnumFlag.赠品出库单);

            checkComboBoxEnable.ItemsSource = Enum.GetNames(typeof(Table.SalesOrderS.EnumEnable));
          
            uCdataGrid1.Init(DataTableText.销售统计表, null, new UC.CharProperty { Series = new System.Windows.Controls.DataVisualization.Charting.ColumnSeries(), ValueTitle = "金额", ValueName = "Money" });
            columnCorS = uCdataGrid1.dataGridExt1.GetColumn("CorSName");
            columnStore = uCdataGrid1.dataGridExt1.GetColumn("StoreSName");
            columnFlag = uCdataGrid1.dataGridExt1.GetColumn("Flag");
            Debug.Assert(columnFlag != null);
            Debug.Assert(columnStore != null);
            Debug.Assert(columnCorS != null);
        }

        private void InitEvent()
        {
            buttonSearch.Click += (ss, ee) => FillData(); 
        }

        private void FillData()
        {
            List<int> store = new List<int>();
            List<int> flag = new List<int>();
            List<int> enable = new List<int>();

            if (!string.IsNullOrEmpty(checkComboBoxStoreS.Text))
                foreach (string str in checkComboBoxStoreS.SelectedValue.Split(','))
                    store.Add(int.Parse(str));

            if (!string.IsNullOrEmpty(checkComboBoxFlag.Text))
                foreach (string str in checkComboBoxFlag.SelectedValue.Split(','))
                    flag.Add((int)Enum.Parse(typeof(Table.SalesOrderS.EnumFlag), str));

            if (!string.IsNullOrEmpty(checkComboBoxEnable.Text))
                foreach (string str in checkComboBoxEnable.SelectedValue.Split(','))
                    enable.Add((int)Enum.Parse(typeof(Table.SalesOrderS.EnumEnable), str));

            List<TempClass> obj = new List<TempClass>();
            foreach(Table.SalesOrderS sos in Table.SalesOrderS.GetList(uCDateTime1.StartDateTime, uCDateTime1.EndDateTime, flag.ToArray(), enable.ToArray(), store.ToArray()))
            {
                obj.Add(new TempClass()
                {
                    Flag = sos._FlagText,
                    CorSId = sos.CorSId,
                    CorSName = sos._CorSName,
                    StoreSId =sos.StoreSId,
                    StoreSName =sos._StoreSName,
                    Money = sos.Money
                });
            }

            if (radioButtonCorS.IsChecked.Value)
                FilterByCorS(obj);
            if (radioButtonStoreS.IsChecked.Value)
              FilterByStoreS(obj);

        }

        private void FilterByStoreS(List<TempClass> obj)
        {
            columnCorS.Visibility = Visibility.Hidden;
            columnStore.Visibility = Visibility.Visible;
            List<TempClass> ltf = new List<TempClass>();

            if (checkBoxFlag.IsChecked.Value)
            {
                columnFlag.Visibility = Visibility.Visible;
                foreach (string str in Enum.GetNames(typeof(Table.SalesOrderS.EnumFlag)))//订单类型
                {
                    var vr = from f in obj where f.Flag == str select f;
                    if (vr.Count() == 0)
                        continue;

                    foreach (IdAndName ian in MainWindow.allStoreS)
                    {
                        var v2 = from f in vr where f.StoreSId == ian.Id select f;
                        int count = v2.Count();
                        if (count == 0)
                            continue;

                        TempClass tf = new TempClass();
                        tf.StoreSName = ian.Name;
                        tf.Flag = str;
                        tf.Quantity = count;
                        tf.Money = v2.Sum(f => { return f.Money; });
                        ltf.Add(tf);
                    }
                }
            }
            else
            {
                columnFlag.Visibility = Visibility.Hidden;
                foreach (IdAndName ian in MainWindow.allStoreS)
                {
                    var v2 = from f in obj where f.StoreSId== ian.Id select f;
                    int count = v2.Count();
                    if (count == 0)
                        continue;

                    TempClass tf = new TempClass();
                    tf.StoreSName= ian.Name;
                    tf.Quantity = count;
                    tf.Money = v2.Sum(f => { return f.Money; });
                    ltf.Add(tf);
                }
            }

            uCdataGrid1.Init(DataTableText.销售统计表, ltf );
        }

        private void FilterByCorS(List<TempClass> obj)
        {
            columnStore.Visibility = Visibility.Hidden;
            columnCorS.Visibility = Visibility.Visible;
            List<TempClass> ltf=new List<TempClass>();

            if (checkBoxFlag.IsChecked.Value)
            {
                columnFlag.Visibility = Visibility.Visible;
                foreach(string str in Enum.GetNames(typeof(Table.SalesOrderS.EnumFlag)))//订单类型
                {
                    var vr = from f in obj where f.Flag ==str select f;
                    if(vr.Count()==0)
                        continue;

                    foreach (IdAndName ian in MainWindow.allCorS)
                    {
                        var v2 = from f in vr where f.CorSId == ian.Id select f;
                        int count = v2.Count();
                        if (count == 0)
                            continue;

                        TempClass tf = new TempClass();
                        tf.CorSName =ian.Name;
                        tf.Flag =str;
                        tf.Quantity = count;
                        tf.Money = v2.Sum(f => { return f.Money; });
                        ltf.Add(tf);
                    }
                }
            }
            else
            {
                columnFlag.Visibility = Visibility.Hidden;
                foreach (IdAndName ian in MainWindow.allCorS)
                {
                    var v2 = from f in obj where f.CorSId == ian.Id select f;
                    int count = v2.Count();
                    if (count == 0)
                        continue;

                    TempClass tf = new TempClass();
                    tf.CorSName = ian.Name;
                    tf.Quantity = count;
                    tf.Money = v2.Sum(f => { return f.Money; });
                    ltf.Add(tf);
                }
            }

            uCdataGrid1.Init(DataTableText.销售统计表,ltf);
        }


        private class TempClass
        {
            public string Flag { get; set; }
            public Int64 CorSId { get; set; }
            public string CorSName { get; set; }
            public Int64 StoreSId { get; set; }
            public string StoreSName { get; set; }
            public double Money { get; set; }
            public int Quantity { get; set; }
        }
    }
}
