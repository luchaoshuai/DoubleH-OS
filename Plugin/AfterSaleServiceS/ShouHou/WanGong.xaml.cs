﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH;

namespace AfterSaleServiceS
{
    /// <summary>
    /// WanGong.xaml 的交互逻辑
    /// </summary>
    public partial class WanGong : Window
    {
        Table.AfterSaleServiceS order;
        public WanGong(Table.AfterSaleServiceS obj)
        {
            InitializeComponent();

            order = obj;
            System.Diagnostics.Debug.Assert(order != null);
            InitVar();
            InitEvent();
        }

        private void InitVar()
        {
            doubleUpDownFuWuFei.FormatString = doubleUpDownJiaoTongFei.FormatString = "F" + Table.SysConfig.SysConfigParams.DecimalPlaces.ToString();

            dateTimePickerDaoDaDateTime.Value = order.ArrivalDateTime;
            dateTimePickerWanGongDateTime.Value = DateTime.Now;
            textBoxOtherBill.Text = order.RelatedBill;

            uCUniqueS1.Init(Table.UniqueS.EnumFlag.服务评价);

            corSinfo1.SelectedObjectId = order.CorSId;

            corSinfo1.Contact = order.CorSContactsName;
            corSinfo1.Phone = order.CorSContactsPhone;

            textBoxJieJueFangfa.Focus();
        }

        private void InitEvent()
        {
            buttonOK.Click += (ss, ee) => Save(); 
        }

        private void Save()
        {
            if (string.IsNullOrWhiteSpace(textBoxJieJueFangfa.Text))
            {
                MessageWindow.Show("解决方法不能为空");
                return;
            }
           
            if (uCUniqueS1.SelectedItem == null)
            {
                MessageWindow.Show("请选择客户评价");
                return;
            }

            order.DaoDaDateTime = (DateTime)dateTimePickerDaoDaDateTime.Value;
            order.WanChengDateTime = (DateTime)dateTimePickerWanGongDateTime.Value;
            order.JieJueFangFa = textBoxJieJueFangfa.Text;
            order.FuWuFei = (double)doubleUpDownFuWuFei.Value;
            order.JiaoTongFei = (double)doubleUpDownJiaoTongFei.Value;
            order.FuWuPingJia = uCUniqueS1.SelectedObjectId;
            order.RelatedBill = textBoxOtherBill.Text;

            Table.DataTableS.EnumDatabaseStatus result = order.Update((int)Table.AfterSaleServiceS.EnumEnable.维护完工);
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}