﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoubleH.Plugins;
using System.Diagnostics;
using Table = FCNS.Data.Table;
using DoubleH;
using DoubleH.Utility;

namespace PurchaseOrderS
{
    class PurchaseOrderSExt : Plugin
    {
        private IPluginHost ihost = null;
        MainWindow window = null;
        AutoPurchaseOrderS apo = null;

        public override bool Initialize(IPluginHost host)
        {
            Debug.Assert(host != null);
            if (host == null)
                return false;

            ihost = host;

            Table.PurchaseOrderS.EnumFlag flag = Table.PurchaseOrderS.EnumFlag.采购订单;
            if (TableText == DataTableText.自动补货)
            {
                apo = new AutoPurchaseOrderS();
                apo.Init(host);
                apo.ShowDialog();
            }
            else
            {
                window = new MainWindow();
                switch (TableText)
                {
                    case DataTableText.采购退货单:
                        flag = Table.PurchaseOrderS.EnumFlag.采购退货单;
                        break;

                    case DataTableText.赠品入库单:
                        flag = Table.PurchaseOrderS.EnumFlag.赠品入库单;
                        break;
                        
                    //default:
                    //    Terminate();
                    //    return false;
                }
                window.Init(flag, DataTable as Table.PurchaseOrderS);
                window.Owner = host.WorkAreaWindow;
                window.Show();
                //window.Closed += (ss, ee) => { ihost.WorkAreaWindow.RefreshDataGrid(); };

                //Terminate();
            }
            //Terminate();
            return true;
        }

        public override void Terminate()
        {
            if (window != null)
                window.Close();
            if (apo != null)
                apo.Close();

            //ihost.WorkAreaWindow.RefreshDataGrid();
        }
    }
}
