﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Table = FCNS.Data.Table;
using DoubleH.Utility;
using DoubleH;
using System.Diagnostics;

namespace StoreS
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Table.StoreS order = null;

        public void Init(Table.StoreS obj)
        {
            order = obj ?? new Table.StoreS();

            textBoxName.Text = order.Name;
            textBoxAddress.Text = order.Address;
            textBoxNote.Text = order.Note;
            checkBoxEnable.IsChecked = order.Enable == Table.StoreS.EnumEnable.停用 ? true : false;
            checkBoxIsDefault.IsChecked = order.IsDefault;
            textBoxName.Focus();
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            order.Note = textBoxNote.Text;
            order.Name = textBoxName.Text;
            order.Address = textBoxAddress.Text;
            order.Enable = checkBoxEnable.IsChecked.Value ? Table.StoreS.EnumEnable.停用 : Table.StoreS.EnumEnable.启用;
            order.IsDefault = checkBoxIsDefault.IsChecked.Value;

            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
                result = order.Insert();
            else
                result = order.Update();

            if (order.IsDefault)
                Table.StoreS.GetDefault();

            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                this.Close();
            else
                MessageWindow.Show(result.ToString());
        }
    }
}