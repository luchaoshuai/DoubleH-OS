﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Utility;
using Table = FCNS.Data.Table;
using DoubleH.Utility.Configuration;
using System.Diagnostics;

namespace StoreS
{
    /// <summary>
    /// DiaoBo.xaml 的交互逻辑
    /// </summary>
    public partial class DiaoBo : Window
    {
        public DiaoBo()
        {
            InitializeComponent();

            InitEvent();
        }

        Table.AllocationS order = null;
        public void Init(Table.AllocationS obj)
        {
            order = obj ?? new Table.AllocationS();
            productSListInfo1.Init(DoubleHConfig.UserConfig.GetUserUIparams(DataTableText.仓库调拨单商品编辑));
            productSListInfo1.ProductSType = ProductS.GetProductS.EnumProductS.调拨单商品;
            InitOrder();
        }

        private void InitOrder()
        {
            label1OrderNo.Content = order.OrderNO;
            dateTimeUpDownKaiDan.Value = order.OrderDateTime;
            uCStoreSIn.SelectedObjectId = order.StoreSInId;
            uCStoreSOut.SelectedObjectId = order.StoreSOutId;
            textBoxNote.Text = order.Note;
            productSListInfo1.ItemsSource = order.ProductSList;
            productSListInfo1.StoreS = uCStoreSOut.SelectedObject;

            if (order.Enable == Table.AllocationS.EnumEnable.评估)
            {
                if (order.Id == -1)
                {
                    buttonShenHe.IsEnabled = false;
                    buttonZuoFei.IsEnabled = false;
                }
            }
            else
            {
                buttonSave.IsEnabled = false;
                buttonShenHe.IsEnabled = false;
                buttonZuoFei.IsEnabled = false;
            }
            productSListInfo1.EnableText = order.Enable.ToString();
        }

        private void InitEvent()
        {
            uCStoreSOut.SelectedObjectEvent += (ee) =>   productSListInfo1.StoreS = ee; 
            buttonSave.Click += (ss, ee) => Save();
            buttonShenHe.Click += (ss, ee) => ShenHe();
            buttonZuoFei.Click += (ss, ee) => ZuoFei();
            productSListInfo1.DataGrid.CellEditEnding += (ss, ee) => CheckCellEditValue(ee);
            buttonPrint.Click += (ss, ee) => DoubleH.Utility.Print.PrintForm.ShowPrint(order, DataTableText.调拨单, "仓库\\仓库调拨单.frx");
        }

        private void CheckCellEditValue(DataGridCellEditEndingEventArgs ee)
        {
            DataGridColumn textColumn = ee.Column as DataGridColumn;
            double d = 0;
            switch (textColumn.Header.ToString())
            {
                case "调出数量":
                    TextBox textBox = (TextBox)ee.EditingElement;
                    if (!double.TryParse(textBox.Text, out d))
                        textBox.Undo();
                    else
                    {
                        Table.ProductS ps = productSListInfo1.GetSelectedObject();
                        if (Math.Abs(d) > ps._Quantity)
                            d = ps._Quantity;

                        textBox.Text = (-Math.Abs(d)).ToString();
                    }
                    break;
            }
        }

        private void Save()
        {
            order.Note = textBoxNote.Text;
            order.OrderDateTime = dateTimeUpDownKaiDan.Value.Value;
            order.StoreSOutId = uCStoreSOut.SelectedObjectId;
            order.StoreSInId = uCStoreSIn.SelectedObjectId;

            Table.DataTableS.EnumDatabaseStatus result;
            if (order.Id == -1)
            {
                result = order.Insert();
                if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                {
                    label1OrderNo.Content = order.OrderNO;
                    buttonShenHe.IsEnabled = true;
                    buttonZuoFei.IsEnabled = true;
                }
            }
            else
                result = order.Update();

            MessageWindow.Show(result.ToString());
            productSListInfo1.EnableText = order.Enable.ToString();
        }

        private void ShenHe()
        {
            Table.DataTableS.EnumDatabaseStatus result = order.ShenHe();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                InitOrder();

            MessageWindow.Show(result.ToString());
        }

        private void ZuoFei()
        {
            Table.DataTableS.EnumDatabaseStatus result = order.ZuoFei();
            if (result == Table.DataTableS.EnumDatabaseStatus.操作成功)
                InitOrder();

            MessageWindow.Show(result.ToString());
        }
    }
}