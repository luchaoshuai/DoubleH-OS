using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using DoubleH.Utility;

namespace DHserver.Plugins
{
	/// <summary>
	/// 插件基类,所有的插件必须继承此类
	/// </summary>
	public   class Plugin
	{
		/// <summary>
		/// 插件的初始化放这里
		/// </summary>
		/// <returns>true 初始化成功，false则卸载插件（不释放资源）</returns>
		public virtual bool Initialize(IPluginHost host)
		{
            return (host != null) ;
		}
		/// <summary>
		/// 插件清理函数，释放所有资源、关闭所有已打开文件等等操作时建议调用此函数。
		/// </summary>
		public virtual void Terminate()
		{
		}

        /// <summary>
        /// 获取插件图标
        /// </summary>
        public virtual System.Windows.Controls.Image SmallIcon
        {
            get { return null; }
        }

		/// <summary>
        /// 文件必须以.xml结尾,请查看样例:http://www.fcnsoft.com/download/DHserver.xml
		/// </summary>
		public virtual string UpdateUrl
		{
			get { return null; }
		}

        public virtual void ShowConfigWindow()
        {
        }
	}
}
