﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace DHserver
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            if (System.Diagnostics.Process.GetProcessesByName("DHserver").Length > 1)
            {
                MessageBox.Show("当前程序已运行");
                Environment.Exit(0);
            }
            base.OnStartup(e);
        }
    }
}
