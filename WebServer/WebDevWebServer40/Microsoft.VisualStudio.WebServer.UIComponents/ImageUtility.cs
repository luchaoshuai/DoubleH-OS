using System;
using System.Drawing;
using System.Drawing.Imaging;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	internal sealed class ImageUtility
	{
		private static ColorMatrix disabledImageColorMatrix;
		private static ColorMatrix DisabledImageColorMatrix
		{
			get
			{
				if (ImageUtility.disabledImageColorMatrix == null)
				{
					float[][] array = new float[5][];
					array[0] = new float[]
					{
						0.2125f, 
						0.2125f, 
						0.2125f, 
						0f, 
						0f
					};
					array[1] = new float[]
					{
						0.2577f, 
						0.2577f, 
						0.2577f, 
						0f, 
						0f
					};
					array[2] = new float[]
					{
						0.0361f, 
						0.0361f, 
						0.0361f, 
						0f, 
						0f
					};
					float[][] arg_5C_0 = array;
					int arg_5C_1 = 3;
					float[] array2 = new float[5];
					array2[3] = 1f;
					arg_5C_0[arg_5C_1] = array2;
					array[4] = new float[]
					{
						0.38f, 
						0.38f, 
						0.38f, 
						0f, 
						1f
					};
					ImageUtility.disabledImageColorMatrix = new ColorMatrix(array);
				}
				return ImageUtility.disabledImageColorMatrix;
			}
		}
		private ImageUtility()
		{
		}
		public static Image CreateDisabledImage(Image normalImage)
		{
			ImageAttributes imageAttributes = new ImageAttributes();
			imageAttributes.ClearColorKey();
			imageAttributes.SetColorMatrix(ImageUtility.DisabledImageColorMatrix);
			Size size = normalImage.Size;
			Bitmap bitmap = new Bitmap(size.Width, size.Height);
			Graphics graphics = Graphics.FromImage(bitmap);
			graphics.DrawImage(normalImage, new Rectangle(0, 0, size.Width, size.Height), 0, 0, size.Width, size.Height, GraphicsUnit.Pixel, imageAttributes);
			graphics.Dispose();
			return bitmap;
		}
	}
}
