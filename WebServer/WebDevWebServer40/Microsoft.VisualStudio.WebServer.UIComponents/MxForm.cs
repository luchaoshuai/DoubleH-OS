using System;
using System.Collections;
using System.Drawing;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Windows.Forms.Design;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public class MxForm : Form
	{
		private static readonly object InitialActivatedEvent = new object();
		private IServiceProvider _serviceProvider;
		private bool _initialActivatedRaised;
		public event EventHandler InitialActivated
		{
			add
			{
				base.Events.AddHandler(MxForm.InitialActivatedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(MxForm.InitialActivatedEvent, value);
			}
		}
		protected IServiceProvider ServiceProvider
		{
			get
			{
				return this._serviceProvider;
			}
		}
		public MxForm()
		{
		}
		public MxForm(IServiceProvider serviceProvider)
		{
			if (serviceProvider == null)
			{
				throw new ArgumentNullException("serviceProvider");
			}
			this._serviceProvider = serviceProvider;
			IUIService iUIService = (IUIService)this.GetService(typeof(IUIService));
			if (iUIService != null)
			{
				IDictionary styles = iUIService.Styles;
				Font font = (Font)styles["DialogFont"];
				this.Font = font;
			}
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._serviceProvider = null;
			}
			base.Dispose(disposing);
		}
		protected override object GetService(Type serviceType)
		{
			if (this._serviceProvider != null)
			{
				return this._serviceProvider.GetService(serviceType);
			}
			return null;
		}
		private bool HandleWmDrawItem(ref Message m)
		{
			return false;
		}
		private bool HandleWmMeasureItem(ref Message m)
		{
			return false;
		}
		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			if (!this._initialActivatedRaised)
			{
				this._initialActivatedRaised = true;
				this.OnInitialActivated(e);
			}
		}
		protected override void OnHandleCreated(EventArgs e)
		{
			base.OnHandleCreated(e);
		}
		protected virtual void OnInitialActivated(EventArgs e)
		{
			EventHandler eventHandler = (EventHandler)base.Events[MxForm.InitialActivatedEvent];
			if (eventHandler != null)
			{
				eventHandler(this, e);
			}
		}
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		protected override void WndProc(ref Message m)
		{
			int msg = m.Msg;
			switch (msg)
			{
				case 43:
				{
					if (!this.HandleWmDrawItem(ref m))
					{
						base.WndProc(ref m);
						return;
					}
					break;
				}
				case 44:
				{
					if (!this.HandleWmMeasureItem(ref m))
					{
						base.WndProc(ref m);
						return;
					}
					break;
				}
				default:
				{
					if (msg == 546)
					{
						if (m.LParam == base.Handle)
						{
							this.OnActivated(EventArgs.Empty);
						}
					}
					base.WndProc(ref m);
					break;
				}
			}
		}
	}
}
