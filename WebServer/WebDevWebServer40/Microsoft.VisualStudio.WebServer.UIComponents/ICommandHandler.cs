using System;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public interface ICommandHandler
	{
		bool HandleCommand(Command command);
		bool UpdateCommand(Command command);
	}
}
