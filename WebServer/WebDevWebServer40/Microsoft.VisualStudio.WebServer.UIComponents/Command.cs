using System;
using System.Drawing;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public abstract class Command
	{
		private const int StateChecked = 1;
		private const int StateEnabled = 2;
		private const int StateVisible = 4;
		private const int StateTextChanged = 256;
		private const int StateHelpTextChanged = 512;
		private const int StateGlyphChanged = 1024;
		private const int StateVisibleChanged = 2048;
		private const int StateInternalChange = 65536;
		private Type _commandGroup;
		private int _commandID;
		private object _commandUI;
		private int _state;
		private string _text;
		private string _helpText;
		private Image _glyph;
		private int _glyphIndex;
		private ICommandHandler _handler;
		private ICommandHandlerWithContext _contextHandler;
		private object _contextObject;
		public virtual bool Checked
		{
			get
			{
				return (this._state & 1) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 1;
					return;
				}
				this._state &= -2;
			}
		}
		public Type CommandGroup
		{
			get
			{
				return this._commandGroup;
			}
		}
		internal ICommandHandler Handler
		{
			get
			{
				return this._handler;
			}
		}
		public int CommandID
		{
			get
			{
				return this._commandID;
			}
		}
		public object CommandUI
		{
			get
			{
				return this._commandUI;
			}
		}
		public virtual bool Enabled
		{
			get
			{
				return (this._state & 2) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 2;
					return;
				}
				this._state &= -3;
			}
		}
		public virtual Image Glyph
		{
			get
			{
				return this._glyph;
			}
			set
			{
				if (this._glyph != value)
				{
					this._glyph = value;
					this.GlyphChanged = true;
				}
			}
		}
		public virtual int GlyphIndex
		{
			get
			{
				return this._glyphIndex;
			}
			set
			{
				if (this._glyphIndex != value)
				{
					this._glyphIndex = value;
					this.GlyphChanged = true;
				}
			}
		}
		protected bool GlyphChanged
		{
			get
			{
				return (this._state & 1024) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 1024;
					return;
				}
				this._state &= -1025;
			}
		}
		public virtual string HelpText
		{
			get
			{
				return this._helpText;
			}
			set
			{
				if ((this._helpText == null && value != null) || !this._helpText.Equals(value))
				{
					this._helpText = value;
					this.HelpTextChanged = true;
				}
			}
		}
		protected bool HelpTextChanged
		{
			get
			{
				return (this._state & 512) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 512;
					return;
				}
				this._state &= -513;
			}
		}
		protected bool InternalChange
		{
			get
			{
				return (this._state & 65536) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 65536;
					return;
				}
				this._state &= -65537;
			}
		}
		public virtual string Text
		{
			get
			{
				return this._text;
			}
			set
			{
				if ((this._text == null && value != null) || !this._text.Equals(value))
				{
					this._text = value;
					this.TextChanged = true;
				}
			}
		}
		protected bool TextChanged
		{
			get
			{
				return (this._state & 256) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 256;
					return;
				}
				this._state &= -257;
			}
		}
		public virtual bool Visible
		{
			get
			{
				return (this._state & 4) != 0;
			}
			set
			{
				if (value != this.Visible)
				{
					if (value)
					{
						this._state |= 4;
					}
					else
					{
						this._state &= -5;
					}
					this.VisibleChanged = true;
				}
			}
		}
		protected bool VisibleChanged
		{
			get
			{
				return (this._state & 2048) != 0;
			}
			set
			{
				if (value)
				{
					this._state |= 2048;
					return;
				}
				this._state &= -2049;
			}
		}
		public Command(Type commandGroup, int commandID, object commandUI)
		{
			if (commandGroup == null)
			{
				throw new ArgumentNullException("commandGroup", "Command Group can't be NULL");
			}
			if (commandUI == null)
			{
				throw new ArgumentNullException("commandUI", "Command UI can't be NULL");
			}
			this._commandGroup = commandGroup;
			this._commandID = commandID;
			this._commandUI = commandUI;
			this._glyphIndex = -1;
		}
		protected internal virtual bool InvokeCommand()
		{
			if ((this._handler == null && this._contextHandler == null) || this.InternalChange)
			{
				return false;
			}
			if (this._contextHandler != null)
			{
				return this._contextHandler.HandleCommand(this, this._contextObject);
			}
			return this._handler.HandleCommand(this);
		}
		protected internal virtual bool UpdateCommand(ICommandHandler[] handlers)
		{
			this._handler = null;
			this._contextHandler = null;
			this._contextObject = null;
			for (int i = 0; i < handlers.Length; i++)
			{
				if (handlers[i].UpdateCommand(this))
				{
					this._handler = handlers[i];
					return true;
				}
			}
			this.Enabled = false;
			return false;
		}
		protected internal bool UpdateCommand(ICommandHandler[] handlers, ICommandHandlerWithContext contextHandler, object context)
		{
			this._handler = null;
			this._contextHandler = null;
			this._contextObject = null;
			if (contextHandler.UpdateCommand(this, context))
			{
				this._contextHandler = contextHandler;
				this._contextObject = context;
				return true;
			}
			return this.UpdateCommand(handlers);
		}
		public abstract void UpdateCommandUI();
	}
}
