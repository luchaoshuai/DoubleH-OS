using System;
namespace Microsoft.VisualStudio.WebServer.UIComponents
{
	public interface IStatusBar
	{
		void SetProgress(int percentComplete);
		void SetText(string text);
	}
}
