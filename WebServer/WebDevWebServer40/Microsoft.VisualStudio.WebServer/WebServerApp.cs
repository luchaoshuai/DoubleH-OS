using Microsoft.VisualStudio.WebHost;
using Microsoft.VisualStudio.WebServer.UIComponents;
using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
namespace Microsoft.VisualStudio.WebServer
{
	public sealed class WebServerApp
	{
		[LoaderOptimization(LoaderOptimization.MultiDomainHost), STAThread]
		public static int Main(string[] args)
		{
			CommandLine commandLine = new CommandLine(args);
			bool flag = commandLine.Options["silent"] != null;
			if (!flag && commandLine.ShowHelp)
			{
				WebServerApp.ShowUsage();
				return 0;
			}
			string text = (string)commandLine.Options["vpath"];
			if (text != null)
			{
				text = text.Trim();
			}
			if (text == null || text.Length == 0)
			{
				text = "/";
			}
			else
			{
				if (!text.StartsWith("/", StringComparison.Ordinal))
				{
					if (!flag)
					{
						WebServerApp.ShowUsage();
					}
					return -1;
				}
			}
			string text2 = (string)commandLine.Options["path"];
			if (text2 != null)
			{
				text2 = text2.Trim();
			}
			if (text2 == null || text2.Length == 0)
			{
				if (!flag)
				{
					WebServerApp.ShowUsage();
				}
				return -1;
			}
			if (!Directory.Exists(text2))
			{
				if (!flag)
				{
					WebServerApp.ShowMessage(SR.GetString("WEBDEV_DirNotExist", new object[]
					{
						text2
					}));
				}
				return -2;
			}
			int num = 0;
			string text3 = (string)commandLine.Options["port"];
			if (text3 != null)
			{
				text3 = text3.Trim();
			}
			if (text3 != null && text3.Length != 0)
			{
				try
				{
					num = int.Parse(text3, CultureInfo.InvariantCulture);
					if (num < 1 || num > 65535)
					{
						if (!flag)
						{
							WebServerApp.ShowUsage();
						}
						int result = -1;
						return result;
					}
					goto IL_16E;
				}
				catch
				{
					if (!flag)
					{
						WebServerApp.ShowMessage(SR.GetString("WEBDEV_InvalidPort", new object[]
						{
							text3
						}));
					}
					int result = -3;
					return result;
				}
			}
			num = 80;
			IL_16E:
			bool requireAuthentication = commandLine.Options["ntlm"] != null;
			bool disableDirectoryListing = commandLine.Options["nodirlist"] != null;
			Server server = new Server(num, text, text2, requireAuthentication, disableDirectoryListing);
			try
			{
				server.Start();
			}
			catch (Exception ex)
			{
				if (!flag)
				{
					WebServerApp.ShowMessage(SR.GetString("WEBDEV_FailToStart", new object[]
					{
						num, 
						ex.Message, 
						ex.HelpLink
					}));
				}
				int result = -4;
				return result;
			}
			Application.Run(new WebServerForm_DAL(server));
			return 0;
		}
		private static void ShowUsage()
		{
			WebServerApp.ShowMessage(string.Concat(new string[]
			{
				SR.GetString("WEBDEV_Usagestr1"), 
				SR.GetString("WEBDEV_Usagestr2"), 
				SR.GetString("WEBDEV_Usagestr3"), 
				SR.GetString("WEBDEV_Usagestr4"), 
				SR.GetString("WEBDEV_Usagestr5"), 
				SR.GetString("WEBDEV_Usagestr6"), 
				SR.GetString("WEBDEV_Usagestr7")
			}), MessageBoxIcon.Asterisk);
		}
		private static void ShowMessage(string msg)
		{
			if (Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft)
			{
				MessageBox.Show(msg, SR.GetString("WEBDEV_Name"), MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
				return;
			}
			MessageBox.Show(msg, SR.GetString("WEBDEV_Name"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}
		private static void ShowMessage(string msg, MessageBoxIcon icon)
		{
			if (Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft)
			{
				MessageBox.Show(msg, SR.GetString("WEBDEV_Name"), MessageBoxButtons.OK, icon, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
				return;
			}
			MessageBox.Show(msg, SR.GetString("WEBDEV_Name"), MessageBoxButtons.OK, icon);
		}
	}
}
