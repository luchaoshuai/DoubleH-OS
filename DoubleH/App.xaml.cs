﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;
using DoubleH.Utility.Configuration;
using DoubleH.Plugins;
using System.IO;
using DoubleH.Utility.IO;
using Table = FCNS.Data.Table;

namespace DoubleH
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            if (System.Diagnostics.Process.GetProcessesByName("DoubleH").Length > 1)
            {
                MessageBox.Show("当前程序已运行");
                Environment.Exit(0);
            }

                WorkArea workArea = new WorkArea();
                workArea.Init();
                workArea.Show();
        }
    }
}