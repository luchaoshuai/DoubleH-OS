using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using DoubleH.Utility;
using System.Windows.Controls;

namespace DoubleH.Plugins
{
    /// <summary>
    /// 插件宿主(doubleH.exe)，包含可以调用的来自 DoubleH 的功能
    /// </summary>
    internal sealed class DefaultPluginHost : IPluginHost
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public void Initialize(WorkArea form)
        {
            Debug.Assert(form != null);

            this.window = form;
        }

        private WorkArea window = null;
        public WorkArea WorkAreaWindow
        {
            get { return window; }
        }

        public DoubleH.Utility.UC.UCPagePanel PagePanel
        {
            get { return window.uCPagePanel1; }
        }

        public PluginManager ExtentionManager { get { return window.PluginManager; } }

        /// <summary>
        /// 设置是否允许 导入数据
        /// </summary>
        public bool CanImport
        {
            set { window.button_import.IsEnabled = value; }
        }

        /// <summary>
        /// 设置是否允许删除数据
        /// </summary>
        public bool CanDelete
        {
            set { window.button_zuofei.IsEnabled = value; }
        }
    }
}
