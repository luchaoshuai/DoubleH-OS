﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DoubleH.Plugins;
using Table = FCNS.Data.Table;
using System.Data;
using System.ComponentModel;
using DoubleH.Utility;
using System.Windows.Controls.Primitives;
using System.Diagnostics;
using System.IO;
using DoubleH.Utility.Configuration;
using DoubleH.Utility.IO;
using FCNS.Data;

namespace DoubleH
{
    public partial class WorkArea
    {
        private void InitMenu()
        {
            foreach (Table.SysConfig.Model m in Table.SysConfig.SysConfigParams.AllModel())
            {
                if (!m.MenuShow || !m.Used)
                    continue;

                MenuItem mi = new MenuItem();
                mi.Header = m.ModelText;
                mi.Tag = m.ModelText;
                mi.Click += (ss, ee) => { TopMenuClick((Table.SysConfig.EnumModel)mi.Tag); };
                TopMenu.Items.Add(mi);
            }
        }

        internal void TopMenuClick(Table.SysConfig.EnumModel mode)
        {
            List<MenuItemBinding> bindingData = new List<MenuItemBinding>();
            Table.UserS authority = Table.UserS.LoginUser;
            switch (mode)
            {
                case Table.SysConfig.EnumModel.公寓租赁:
                    bindingData.Add(AddHouse());
                    bindingData.Add(AddHouseMoney());
                    bindingData.Add(AddCustomerSMenu());
                    bindingData.Add(AddHouseStatue());
                    break;

                case Table.SysConfig.EnumModel.IT设备管理:
                    bindingData.Add(AddITdbGroup());
                    bindingData.Add(AddITdbOther());
                    break;

                case Table.SysConfig.EnumModel.项目管理:
                    bindingData.Add(AddProjectSMenu());
                    bindingData.Add(AddCustomerSMenu());
                    bindingData.Add(AddSchedule());
                    break;

                case Table.SysConfig.EnumModel.消息中心: bindingData.Add(AddMessageS()); break;
                //case Table.SysConfig.EnumModel.项目管理: bindingData.Add(AddProjectSMenu()); break;

                case Table.SysConfig.EnumModel.采购进货:
                    bindingData.Add(AddPurchaseMenu());
                    bindingData.Add(AddSupplierSMenu());
                    bindingData.Add(AddProductSMenu(DataTableText.可采购商品));
                    bindingData.Add(AddWuLiu(DataTableText.采购物流跟踪));
                    break;

                case Table.SysConfig.EnumModel.仓库管理:
                    bindingData.Add(AddStoreSMenu1());
                    bindingData.Add(AddStoreSMenu2());
                    bindingData.Add(AddStore());
                    bindingData.Add(AddProductSMenu(DataTableText.仓库商品));
                    bindingData.Add(AddWuLiu(DataTableText.仓库物流跟踪));
                    break;

                case Table.SysConfig.EnumModel.批发销售:
                    bindingData.Add(AddSalesMenu());
                    bindingData.Add(AddCustomerSMenu());
                    bindingData.Add(AddProductSMenu(DataTableText.可销售商品));
                    bindingData.Add(AddVipMenu());
                    bindingData.Add(AddWuLiu(DataTableText.批发物流跟踪));
                    break;

                case Table.SysConfig.EnumModel.POS零售:
                    bindingData.Add(AddPosMenu());
                    bindingData.Add(AddCustomerSMenu());
                    bindingData.Add(AddProductSMenu(DataTableText.可销售商品));
                    bindingData.Add(AddVipMenu());
                    bindingData.Add(AddCouponsMenu());
                    break;

                case Table.SysConfig.EnumModel.售后服务:
                    bindingData.Add(AddAfterSaleServiceS());
                    bindingData.Add(AddIT());
                    bindingData.Add(AddRepairS());
                    bindingData.Add(AddServiceSandRepairS());
                    bindingData.Add(AddSupplierSMenu());
                    bindingData.Add(AddCustomerSMenu());
                    break;

                case Table.SysConfig.EnumModel.财务管理:
                    bindingData.Add(AddMoney1());
                    //bindingData.Add(AddMoney2());
                    bindingData.Add(AddMoney3());
                    bindingData.Add(AddInvoice());
                    break;

                case Table.SysConfig.EnumModel.报表中心: System.Diagnostics.Process.Start(DbDefine.baseDir + "ReportS.exe ", Table.UserS.LoginUser.UserNO + " " + Table.UserS.LoginUser.Password); return;

                case Table.SysConfig.EnumModel.车辆管理:
                    bindingData.Add(AddCar());
                    bindingData.Add(AddCar2());
                    bindingData.Add(AddCustomerSMenu());
                    break;
            }
            bindingData.RemoveAll(f => f == null);
            treeViewLeftMenu.ItemsSource = bindingData;
        }
        //公寓租赁
        private MenuItemBinding AddHouse()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "房屋管理",DllFile="HouseS.dll",MustRefreshSubItem=true  };
            foreach (Table.HouseS hs in Table.HouseS.GetList(Table.HouseS.EnumEnable.启用))
            {
                MenuItemBinding mib = AddMenuItem(items, "HouseS.dll", DataTableText.公寓, null, Table.UserS.EnumAuthority.作废);
                mib.Name = hs.Name;
                //mib.MustRefreshSubItem = true;
                mib.CanNew = false;
                mib.Tag = hs.Id;
                for (int i = 1; i <= hs.Floor; i++)
                {
                    MenuItemBinding m = AddMenuItem(mib, "HouseS.dll", DataTableText.楼层, null, Table.UserS.EnumAuthority.作废);
                    m.Name = "第" + i + "层";
                    //m.MustRefreshSubItem = true;
                    m.CanNew = false;
                    m.Tag = hs.Id+"."+i;
                }
            }

          return items;
        }
        private MenuItemBinding AddHouseMoney()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "费用管理" };
            AddMenuItem(items, "UniqueS.dll", DataTableText.房间类型, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "UniqueS.dll", DataTableText.公寓固定费用, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "UniqueS.dll", DataTableText.公寓仪表费用, null, Table.UserS.EnumAuthority.作废);
            return items;
        }
        private MenuItemBinding AddHouseStatue()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "房屋及费用状态" };

            AddMenuItem(items, "HouseS.dll", DataTableText.公寓状态, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "HouseS.dll", DataTableText.公寓缴费情况, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "HouseS.dll", DataTableText.公寓合同状态, null, Table.UserS.EnumAuthority.作废);
            return items;
        }

        //itdb
        private MenuItemBinding AddITdbGroup()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "设备资料" };
            MenuItemBinding mib = AddMenuItem(items, "ItDbS.dll", DataTableText.硬件, null, Table.UserS.EnumAuthority.作废);
            if (mib != null)
            {
                MenuItem obj1 = new MenuItem() { Header = "网络设备搜索" };
                obj1.Click += (ss, ee) =>
                {
                    pluginManager.LoadPlugin("ItDbS.dll", string.Empty, DataTableText.Null, null, null);
                };
                mib.ContextMenuItems = new List<MenuItem>();
                mib.ContextMenuItems .Add(obj1);
            }

            AddMenuItem(items, "ItDbS.dll", DataTableText.软件, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "GroupS.dll", DataTableText.设备分类, null, Table.UserS.EnumAuthority.作废);
            AddMenuItem(items, "UniqueS.dll", DataTableText.标记, null, Table.UserS.EnumAuthority.作废);
            return items;
        }
        private MenuItemBinding AddITdbOther()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "其它" };
            AddMenuItem(items, "ITdbS.dll", DataTableText.浏览数据, false);
            return items;
        }
        //消息中心
        private MenuItemBinding AddMessageS()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "消息中心" };
            AddMenuItem(items, "MessageS.dll", DataTableText.我的消息, null, Table.UserS.EnumAuthority.作废, false);
            AddMenuItem(items, "MessageS.dll", DataTableText.待办事项, "ScheduleS", Table.UserS.EnumAuthority.作废);
            return items;
        }
        //项目管理
        private MenuItemBinding AddProjectSMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "项目管理" };
            AddMenuItem(items, "ProjectS.dll", DataTableText.项目列表);
            AddMenuItem(items, "ProjectS.dll", DataTableText.任务跟踪, "ScheduleInProjectS");
            AddMenuItem(items, "ProjectS.dll", DataTableText.问题反馈, "FeedbackInProjectS");
            return items;
        }

        private MenuItemBinding AddSchedule()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "项目计划" };
            AddMenuItem(items, "UniqueS.dll", DataTableText.任务类型);
            return items;
        }

        //采购
        private MenuItemBinding AddPurchaseMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "采购单据" };
            //items.SubItems.Add(new TreeViewBinding() {  DllFile = "PurchaseOrderS.dll", DataTableText.采购询价单 });
            AddMenuItem(items, "PurchaseOrderS.dll", DataTableText.采购订单);
            AddMenuItem(items, "PurchaseOrderS.dll", DataTableText.采购退货单);
            AddMenuItem(items, "PurchaseOrderS.dll", DataTableText.赠品入库单);
            AddMenuItem(items, "PurchaseOrderS.dll", DataTableText.自动补货);
            return items;
        }
        //批发单
        private MenuItemBinding AddSalesMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "批发单据" };
            AddMenuItem(items, "SalesOrderS.dll", DataTableText.销售询价单);
            AddMenuItem(items, "SalesOrderS.dll", DataTableText.销售订单);
            AddMenuItem(items, "SalesOrderS.dll", DataTableText.销售退货单);
            AddMenuItem(items, "SalesOrderS.dll", DataTableText.赠品出库单);
            return items;
        }
        //pos
        private MenuItemBinding AddPosMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "POS零售" };
            MenuItemBinding mib = AddMenuItem(items, "POS.exe", DataTableText.Pos机器号);
            if (mib != null)
            {
                MenuItem obj2 = new MenuItem() { Header = "编辑收银员列表" };
                obj2.Click += (ss, ee) =>
                {
                    if (uCPagePanel1.SelectedItem == null)
                        return;

                    PosStopSale wss = new PosStopSale();
                    wss.Init(PosStopSale.EnumFlag.Pos操作员, uCPagePanel1.SelectedItem as Table.PosS, null);
                    wss.ShowDialog();
                };
                mib.ContextMenuItems = new List<MenuItem>();
                mib.ContextMenuItems .Add(obj2);
            }

            AddMenuItem(items, "POS.exe", DataTableText.Pos禁售商品);
            return items;
        }
        //private MenuItemBinding AddBarMenu()
        //{
        //    MenuItemBinding items = new MenuItemBinding() { Name = "酒吧管理" };
        //    AddMenuItem(items, "POS.exe", DataTableText.实时库存, "", Table.UserS.EnumAuthority.查看, false);
        //    AddMenuItem(items, "POS.exe", DataTableText.客存明细, "", Table.UserS.EnumAuthority.查看, false);
        //    return items;
        //}
        //供应商
        private MenuItemBinding AddSupplierSMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "供应商资料" };
            AddMenuItem(items, "CorS.dll", DataTableText.供应商);
            AddMenuItem(items, "GroupS.dll", DataTableText.客商分类);
            AddMenuItem(items, "UniqueS.dll", DataTableText.客户性质);
            AddMenuItem(items, "UniqueS.dll", DataTableText.区域);
            return items;
        }
        //客户
        private MenuItemBinding AddCustomerSMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "客户资料" };
            AddMenuItem(items, "CorS.dll", DataTableText.客户);
            AddMenuItem(items, "GroupS.dll", DataTableText.客商分类);
            AddMenuItem(items, "UniqueS.dll", DataTableText.客户性质);
            AddMenuItem(items, "UniqueS.dll", DataTableText.区域);
            return items;
        }
        //vip
        private MenuItemBinding AddVipMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "会员信息" };
            AddMenuItem(items, "CorS.dll", DataTableText.会员资料);
            AddMenuItem(items, "UniqueS.dll", DataTableText.会员分类);
            return items;
        }
        //促销
        private MenuItemBinding AddCouponsMenu()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "促销管理" };
            AddMenuItem(items, "PromotionS.dll", DataTableText.优惠券);
            AddMenuItem(items, "PromotionS.dll", DataTableText.促销方案);
            return items;
        }
        //商品资料
        private MenuItemBinding AddProductSMenu(DataTableText tableText)
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "商品资料" };
            switch (tableText)
            {
                case DataTableText.可采购商品:
                    AddMenuItem(items, "ProductS.dll", tableText);
                    AddMenuItem(items, "ProductS.dll", DataTableText.可采购物料清单);
                    break;
                case DataTableText.可销售商品:
                    MenuItemBinding mib = AddMenuItem(items, "ProductS.dll", tableText);
                    if (mib != null)
                    {
                        MenuItem obj1 = new MenuItem() { Header = "添加到POS禁售列表" };
                        obj1.Click += (ss, ee) =>
                        {
                            if (uCPagePanel1.SelectedItem == null || !Table.SysConfig.SysConfigParams.UsePos)
                                return;

                            PosStopSale wss = new PosStopSale();
                            wss.Init(PosStopSale.EnumFlag.Pos禁售商品, null, uCPagePanel1.SelectedItem as Table.ProductS);
                            wss.ShowDialog();
                        };
                        mib.ContextMenuItems = new List<MenuItem>();
                        mib.ContextMenuItems .Add(obj1);
                    }

                    AddMenuItem(items, "ProductS.dll", DataTableText.可销售物料清单);
                    break;

                default: AddMenuItem(items, "ProductS.dll", tableText);
                    AddMenuItem(items, "ProductS.dll", DataTableText.物料清单);
                    break;
            }
            AddMenuItem(items, "GroupS.dll", DataTableText.商品分类);
            AddMenuItem(items, "UniqueS.dll", DataTableText.计量单位);
            AddMenuItem(items, "UniqueS.dll", DataTableText.产地);
            AddMenuItem(items, "UniqueS.dll", DataTableText.品牌);
            //items.SubItems.Add(new TreeViewBinding() { DllFile = "UniqueS.dll", DataTableText.商品属性 });
            //items.SubItems.Add(new TreeViewBinding() { DllFile = "ProductS.dll", DataTableText.条码打印});
            AddMenuItem(items, "ProductS.dll", DataTableText.商品校验);

            return items;
        }
        //仓库
        private MenuItemBinding AddStoreSMenu1()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "单据出入库" };
            AddMenuItem(items, "StoreS.dll", DataTableText.仓库单据入库单, "StoreOrderS", false);
            AddMenuItem(items, "StoreS.dll", DataTableText.仓库单据出库单, "StoreOrderS", false);
            AddMenuItem(items, "StoreS.dll", DataTableText.非进货入库单, "StoreOrderS");
            AddMenuItem(items, "StoreS.dll", DataTableText.非销售出库单, "StoreOrderS");
            return items;
        }
        private MenuItemBinding AddStoreSMenu2()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "商品出入库" };
            AddMenuItem(items, "StoreS.dll", DataTableText.仓库商品入库单, "StoreOrderS", false);
            AddMenuItem(items, "StoreS.dll", DataTableText.仓库商品出库单, "StoreOrderS", false);
            return items;
        }
        private MenuItemBinding AddStore()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "仓库资料" };
            AddMenuItem(items, "StoreS.dll", DataTableText.调拨单);
            AddMenuItem(items, "StoreS.dll", DataTableText.库存调价单);
            AddMenuItem(items, "StoreS.dll", DataTableText.盘点单);
            AddMenuItem(items, "StoreS.dll", DataTableText.仓库);
            return items;
        }
        //售后服务
        private MenuItemBinding AddAfterSaleServiceS()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "客服中心" };
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.客户报修);
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.售后派单);
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.维护完工);
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.回访反馈);
            return items;
        }
        private MenuItemBinding AddIT()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "外包服务" };
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.外包合同, "WeiBaoS");
            AddMenuItem(items, "UniqueS.dll", DataTableText.服务级别定义);
            return items;
        }
        private MenuItemBinding AddRepairS()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "坏件保修" };
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.客户坏件送修);
            AddMenuItem(items, "AfterSaleServiceS.dll", DataTableText.供应商坏件送修);
            return items;
        }
        private MenuItemBinding AddServiceSandRepairS()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "维护资料" };
            AddMenuItem(items, "UniqueS.dll", DataTableText.维修对象);
            AddMenuItem(items, "UniqueS.dll", DataTableText.维修类型);
            AddMenuItem(items, "UniqueS.dll", DataTableText.客户提供资料);
            AddMenuItem(items, "UniqueS.dll", DataTableText.服务评价);
            return items;
        }
        //财务
        private MenuItemBinding AddMoney3()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "账务资料" };
            AddMenuItem(items, "UniqueS.dll", DataTableText.支付方式);
            AddMenuItem(items, "UniqueS.dll", DataTableText.发票定义);
            AddMenuItem(items, "UniqueS.dll", DataTableText.会计科目);
            return items;
        }
        private MenuItemBinding AddMoney2()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "凭证" };
            AddMenuItem(items, "PayS.dll", DataTableText.录入凭证);
            return items;
        }
        private MenuItemBinding AddMoney1()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "结算单" };
            AddMenuItem(items, "PayS.dll", DataTableText.收款单);
            AddMenuItem(items, "PayS.dll", DataTableText.付款单);
            AddMenuItem(items, "PayS.dll", DataTableText.其它费用支出单);
            AddMenuItem(items, "PayS.dll", DataTableText.其它费用收入单);
            if(Table.SysConfig.SysConfigParams.UsePos)
            AddMenuItem(items, "PayS.dll", DataTableText.POS记账明细表, "PosOrderS", false);
            //AddMenuItem(items, "PayS.dll", DataTableText.资金借出单);
            //AddMenuItem(items, "PayS.dll", DataTableText.资金借入单);
            AddMenuItem(items, "PayS.dll", DataTableText.银行存取款, "BankMoneyS");

            return items;
        }
        private MenuItemBinding AddInvoice()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "发票管理" };
            AddMenuItem(items, "PayS.dll", DataTableText.未开具发票, false);
            AddMenuItem(items, "PayS.dll", DataTableText.未收取发票, false);

            return items;
        }
        //车辆管理
        private MenuItemBinding AddCar()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "车辆管理" };
            AddMenuItem(items, "CarS.dll", DataTableText.车辆档案);
            AddMenuItem(items, "CarS.dll", DataTableText.预订车辆, "CarDriveS");
            AddMenuItem(items, "CarS.dll", DataTableText.行车记录, "CarDriveS");
            AddMenuItem(items, "CarS.dll", DataTableText.油票记录, "CarOilS");
            return items;
        }
        private MenuItemBinding AddCar2()
        {
            MenuItemBinding items = new MenuItemBinding() { Name = "车辆维护" };
            AddMenuItem(items, "CarS.dll", DataTableText.违章处罚, "CarHealthS");
            AddMenuItem(items, "CarS.dll", DataTableText.事故赔偿, "CarHealthS");
            AddMenuItem(items, "CarS.dll", DataTableText.维修保养, "CarHealthS");
            AddMenuItem(items, "CarS.dll", DataTableText.年审记录, "CarHealthS");
            AddMenuItem(items, "CarS.dll", DataTableText.投保记录, "CarHealthS");
            return items;
        }
        //物流
        private MenuItemBinding AddWuLiu(DataTableText tableText)
        {
            if (!sysConfig.UseWuLiuS)
                return null;

            MenuItemBinding items = new MenuItemBinding() { Name = "物流管理" };
            switch (tableText)
            {
                case DataTableText.采购物流跟踪: AddMenuItem(items, "WuLiuS.dll", DataTableText.采购物流跟踪); break;
                case DataTableText.仓库物流跟踪: AddMenuItem(items, "WuLiuS.dll", DataTableText.仓库物流跟踪); break;
                case DataTableText.批发物流跟踪: AddMenuItem(items, "WuLiuS.dll", DataTableText.批发物流跟踪); break;
            }
            AddMenuItem(items, "CorS.dll", DataTableText.物流公司);
            return items;
        }

        //private void AddMenuItem(MenuItemBinding items, string dllFile, DataTableText tableText)
        //{
        //    AddMenuItem(items, dllFile, tableText, null, Table.UserS.LoginUser.GetAuthority(tableText));
        //}
        private void AddMenuItem(MenuItemBinding items, string dllFile, DataTableText tableText, string tableName, bool canNew = true)
        {
            AddMenuItem(items, dllFile, tableText, tableName, Table.UserS.LoginUser.GetAuthority(tableText), canNew);
        }
        private MenuItemBinding AddMenuItem(MenuItemBinding items, string dllFile, DataTableText tableText, bool canNew = true)
        {
            return AddMenuItem(items, dllFile, tableText, null, Table.UserS.LoginUser.GetAuthority(tableText), canNew);
        }

        private MenuItemBinding AddMenuItem(MenuItemBinding items, string dllFile, DataTableText tableText, Utility.UC.PagePanel.PageListView.EnumModeTemplete tem, bool canNew = true)
        {
            MenuItemBinding item = AddMenuItem(items, dllFile, tableText, null, Table.UserS.LoginUser.GetAuthority(tableText), canNew);
            if (item != null)
                item.DataTempleteName = tem;

            return item;
        }
        private MenuItemBinding AddMenuItem(MenuItemBinding items, string dllFile, DataTableText tableText, string tableName, Table.UserS.EnumAuthority haveAuthority, bool canNew = true)
        {
            if (haveAuthority == Table.UserS.EnumAuthority.无权限)
                return null;

            MenuItemBinding item = new MenuItemBinding()
            {
                DllFile = dllFile,
                TableText = tableText,
                TableName = tableName,
                CanNew = haveAuthority > Table.UserS.EnumAuthority.查看
            };
            item.CanNew = canNew;
            items.SubItems.Add(item);
            return item;
        }
    }
}